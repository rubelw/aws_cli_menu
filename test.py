#! /usr/bin/env python
import io

thisismylist= [  
    {'Name': 'Albert' , 'Age': 16},
    {'Name': 'Suzy', 'Age': 17},
    {'Name': 'Johnny', 'Age': 13}
]

for d in thisismylist:
    print d['Name']
