import boto3
from botocore.exceptions import ClientError
from bson import json_util
import getpass
import logging
import sys
import os
import time
import json
import yaml
import traceback
import uuid
import jinja2
import subprocess
import re
import inspect
from os import listdir
from os.path import isfile, join


try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


try:
    POLL_INTERVAL = os.environ.get('CSU_POLL_INTERVAL', 30)
except:
    POLL_INTERVAL = 30

logging_level = logging.INFO

logging.basicConfig(
    level=logging_level,
    format='[%(levelname)s] %(asctime)s (%(module)s) %(message)s',
    datefmt='%Y/%m/%d-%H:%M:%S'
)

logging.getLogger().setLevel(logging_level)


class CloudStackUtility:
    """
    Cloud stack utility is yet another tool create AWS Cloudformation stacks.
    """
    ASK = '[ask]'
    SSM = '[ssm:'
    _verbose = False
    _template = None
    _template_file = None
    _b3Sess = None
    _cloudFormation = None
    _config = None
    _parameters = {}
    _stackParameters = []
    _s3 = None
    _ssm = None
    _tags = []
    _templateUrl = None
    _recipeUrl = None
    _lambdaBucket = None
    _lambdaKey = None
    _updateStack = False

    def __init__(self, config_block):
        """
        Cloud stack utility init method.

        Args:
            config_block - a dictionary creates from the CLI driver. See that
                           script for the things that are required and
                           optional.

        Returns:
           not a damn thing

        Raises:
            SystemError - if everything isn't just right
        """
        if config_block:
            self._config = config_block
        else:
            logging.error('config block was garbage')
            raise SystemError

    def upsert(self):
        """
        The main event of the utility. Create or update a Cloud Formation
        stack. Injecting properties where needed

        Args:
            None

        Returns:
            True if the stack create/update is started successfully else
            False if the start goes off in the weeds.

        Exits:
            If the user asked for a dryrun exit(with a code 0) the thing here. There is no
            point continuing after that point.

        """
        required_parameters = []
        self._stackParameters = []

        try:
            self._initialize_upsert()
        except Exception:
            return False

        # Try to update cloudformation stack
        try:
            available_parameters = self._parameters.keys()

            # Check if there are parameters in the template
            if 'Parameters' in self._template:

                # Add the parameters from the template to the required parameters list
                for parameter_name in self._template['Parameters']:
                    required_parameters.append(str(parameter_name))

            parameters = []

            # Iterate through each of the required prameters
            for required_parameter in required_parameters:

                parameter = {}
                parameter['ParameterKey'] = str(required_parameter)

                # Required parameters is equal to the prameters in the cloudformation template
                required_parameter = str(required_parameter)


                parameter['ParameterValue'] = self._parameters[required_parameter]

                parameters.append(parameter)

            # Exit if this is just a dryrun
            if self._config.get('dryrun', False):
                logging.info('This was a dryrun')
                sys.exit(0)



            # If not a dryrun, then update the cloudformation stack
            if self._updateStack:
                self._tags.append({"Key": "CODE_VERSION_SD", "Value": self._config.get('codeVersion')})
                self._tags.append({"Key": "ANSWER", "Value": str(42)})
                stack = self._cloudFormation.update_stack(
                    StackName=self._config.get('environment', {}).get('stack_name', None),
                    TemplateURL=self._templateUrl,
                    Parameters=parameters,
                    Capabilities=['CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'],
                    Tags=self._tags
                )
            else:
                self._tags.append({"Key": "CODE_VERSION_SD", "Value": self._config.get('codeVersion')})
                self._tags.append({"Key": "ANSWER", "Value": str(42)})
                stack = self._cloudFormation.create_stack(
                    StackName=self._config.get('environment', {}).get('stack_name', None),
                    TemplateURL=self._templateUrl,
                    Parameters=parameters,
                    Capabilities=['CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'],
                    Tags=self._tags
                )
                logging.info('stack: {}'.format(json.dumps(stack,
                                                           indent=4,
                                                           sort_keys=True)))
        except Exception as x:
            logging.error('Exception caught in upsert(): {}'.format(x))
            if self._verbose:
                traceback.print_exc(file=sys.stdout)

            return False

        return True

    def _lineno(self):
        """Returns the current line number in our program."""
        return inspect.currentframe().f_back.f_lineno


    def _is_template_yaml(self):
        """
        Determines if the template if yaml
        Args:
            None
        Returns:
            True or False
        """
        logging.info('def _is_template_yaml()')
        template_file = self._config.get('environment', {}).get('template', None)
        self._template = None

        # If calling stackility from outside the project directory
        if self._config.get('project_dir'):
            template_file = os.path.join(self._config.get('project_dir'), template_file)

        try:
            logging.info('Trying to read template: '+str(template_file))
            with open(template_file, 'r') as f:
                                self._template = yaml.load(f, Loader=Loader)
            logging.info('template file read successfully')
            if self._template and 'Resources' in self._template:
                template_decoded = True
                logging.info('template is YAML')
                return True
            else:
                logging.info('template is not a valid YAML template')
                return False

        except Exception:
            logging.warning('Exception caught in load_template(yaml): {}'.format(x))
            logging.info('template is not YAML')
            return False

    def _is_template_json(self):
        """
        Determines if the template if json
        Args:
            None
        Returns:
            True or False
        """
        logging.info('def _is_template_json()')
        template_file = self._config.get('environment', {}).get('template', None)
        self._template = None

        # If calling stackility from outside the project directory
        if self._config.get('project_dir'):
            template_file = os.path.join(self._config.get('project_dir'), template_file)

        try:
            json_stuff = open(template_file)
            self._template = json.load(json_stuff)


            if self._template and 'Resources' in self._template:
                    template_decoded = True
                    logging.info('template is JSON')
                    return True
            else:
                    logging.info('template is not a valid JSON template')
                    return False

        except Exception as x:
            logging.warning('Exception caught in load_template(json): {}'.format(x))
            logging.info('template is not JSON')
            return False


    def _is_jinja2_template(self):
        """
        Determines if the template is jinja2
        Args:
            None
        Returns:
            True or False
        """
        logging.info('def _is_jinja2_template()')
        template_file = self._config.get('environment', {}).get('template', None)
        self._template = None

        # If calling stackility from outside the project directory
        if self._config.get('project_dir'):
            template_file = os.path.join(self._config.get('project_dir'), template_file)

        try:
            logging.info('trying to load '+str(template_file))
            lines = open(template_file, 'r').read().replace('\n','')

            logging.info('Trying to find squirrelly brackets in template')

            file_stuff = ''
            for f in lines:
                file_stuff = file_stuff+str(f)

            matchObj = re.match(r'.*(\{\{[a-zA-Z\_\-]+\}\}).*', file_stuff, re.M | re.I)

            if matchObj.group(1):
                logging.info('found a match')
                return True
            else:
                logging.info("No match!!")
                return False

        except Exception as x:
            logging.warning('Exception caught is_jinja2_template: {}'.format(x))
            logging.info('template is not jinja2')
            return False

    def _determine_file_type(self):
        """
        Determines the type of template file.  Whether a jinja2 template, json or yaml
        Args:
            None
        Returns:
            file type
        """
        logging.info('def _determine_file_type()')
        self._set_template_file_location()

        logging.info('def _determine_file_type - checking if json')
        if self._is_template_json():
            # Is json
            logging.info('is a json template')
            if self._is_jinja2_template():
                logging.info('is a json jinja2 template')
                # Is a json jinja2 template
                logging.info('returning json.j2')
                return 'json.j2'
            else:
                # Just a plain json template
                logging.info('returning json')
                return 'json'

        elif self._is_template_yaml():
            logging.info('is a yaml template')
            # Is a yaml template
            if self._is_jinja2_template():
                logging.info('is a yaml jinja2 template')
                # Is a yaml jinja2 template
                logging.info('returning yaml.js')
                return 'yaml.j2'
            else:
                # Is just a plain yaml template
                logging.info('returning yaml')
                return 'yaml'
        else:
            logging.info('Could not determine template type')
            return None

    def _set_template_file_location(self):

        template_file = self._config.get('environment', {}).get('template', None)

        if self._config.get('project_dir'):
            self._template_file = os.path.join(self._config.get('project_dir'), template_file)
        else:
            cwd = os.getcwd()
            self._template_file = os.path.join(cwd, template_file)


    def _load_template(self):
        template_decoded = self._determine_file_type()


        logging.info('template is: '+str(template_decoded))
        # if the template is json, then test if jinja2 template
        if template_decoded == 'json':
            logging.info('template is json')
        elif template_decoded == 'json.j2':
            template_file = self._render_template('json')
        elif template_decoded == 'yaml':
            logging.info('template is yaml')
        elif template_decoded == 'yaml.j2':
            template_file = self._render_template('yaml')
        else:
            logging.info('def _load_template-Could not determine the yaml file type')
            return False


        template_file = self._config.get('environment', {}).get('template', None)
        self._template = None


        logging.info('def _load_template- template is: '+str(template_decoded)+' - line: '+str(self._lineno()))

        try:

            # If a jinja template, then convert to a normal template
            if template_decoded.endswith('.j2'):
                logging.info('def _load_template- template file ends with j2'+' - line: '+str(self._lineno()))

                if template_decoded.endswith('json.j2'):
                    if self._is_valid_json(self._template_file):
                        # Will convert the jinja2 template to a file named .stackility_generated.json
                        self._template_file = self._render_template('json')

                    # Sets the template file to the new rendered template i.e. filename.json
                    #template_file = template_file[:-3]

                    logging.info('def _load_template- new template file is: '+str(template_file)+' - line: '+str(self._lineno()))

                elif template_decoded.endwith('yaml.j2'):
                    # Will convert the jinja2 template to a file named ..stackility_generated.yml
                    self._template_file = self._render_template('yaml')


            else:
                logging.info('def _load_template- template does not end with j2 in ini file or the actual file does not exist'+' - line: '+str(self._lineno()))

            #
            if template_file.endswith('yaml'):
                logging.info('def _load_template- loading yaml file'+' - line: '+str(self._lineno()))
                with open(self._template_file, 'r') as f:
                    self._template = yaml.load(f, Loader=Loader)

            # If a json file os json jinja2 template
            else:

                logging.info('def _load_template- template file does not end with yaml - '+str(template_file)+' - line: '+str(self._lineno()))
                # We are not calling stackility from within the project directory
                if self._config.get('project_dir'):

                    # User wants to output the json template as yaml
                    if self._config.get('output_yaml'):

                        if template_decoded.endswith('json.j2'):
                            # Creates a yaml template from json file
                            logging.info('def _load_template - template ends with json.j2 - convert json template to yaml'+' - line: '+str(self._lineno()))
                            new_template_file = os.path.join(self._config.get('project_dir'),template_file+'.stackility_generated.json')


                            self._template_file = self._convert_json_template_to_yaml(new_template_file)
                            with open(self._template_file, 'r') as f:
                                self._template = yaml.load(f, Loader=Loader)


                        elif template_decoded.endswith('yaml.j2'):
                            logging.info('def _load_template - template ends with yaml.j2 - convert json template to yaml'+' - line: '+str(self._lineno()))
                            # Creates a yaml template from json file
                            new_template_file = os.path.join(self._config.get('project_dir'),template_file+'.stackility_generated.yaml')
                            self._template_file = new_template_file
                            with open(self._template_file, 'r') as f:
                                self._template = yaml.load(f, Loader=Loader)

                        elif template_decoded.endswith('json'):
                            logging.info('def _load_template - template ends with json - convert json template to yaml'+' - line: '+str(self._lineno()))

                            # Creates a yaml template from json file
                            new_template_file = os.path.join(self._config.get('project_dir'),template_file)
                            self._template_file = self._convert_json_template_to_yaml(new_template_file)

                            with open(self._template_file, 'r') as f:
                                self._template = yaml.load(f, Loader=Loader)


                        elif template_decoded.endswith('yaml'):
                            logging.info('def _load_template - template ends with yaml - convert json template to yaml'+' - line: '+str(self._lineno()))

                            # Creates a yaml template from json file
                            new_template_file = os.path.join(self._config.get('project_dir'),template_file)
                            self._template_file = new_template_file

                            with open(self._template_file, 'r') as f:
                                self._template = yaml.load(f, Loader=Loader)


                        else:
                            logging.info('Could not determine type of files'+' - line: '+str(self._lineno()))



                        # Load the yaml template
                        #if self._config.get('project_dir'):
                        #    new_template_file=os.path.splitext(template_file)[0]+'.yaml'
                        #    new_template_file=os.path.join(self._config.get('project_dir'),new_template_file)
                        #    self._template_file = new_template_file
                        #else:
                        #    new_template_file=os.path.splitext(template_file)[0]+'.yaml'
                        #    self._template_file = new_template_file

                        #with open(new_template_file, 'r') as f:
                        #    self._template = yaml.load(f, Loader=Loader)

                    # User does not want to output the json template as yaml
                    # Just load the json template
                    else:
                        json_stuff = open(os.path.join(self._config.get('project_dir'),template_file))
                        self._template = json.load(json_stuff)


                # Calling stackility from within the project directory
                else:
                    # If we want to convert the json template to a yaml template
                    if self._config.get('output_yaml'):
                        # Create a yaml template
                        self._convert_json_template_to_yaml(template_file)
                        # Load the yaml template
                        new_template_file = os.path.splitext(template_file)[0] + '.yaml'
                        self._template_file = new_template_file

                        with open(new_template_file, 'r') as f:
                            self._template = yaml.load(f, Loader=Loader)
                    # We are going to upload the standard template
                    else:
                        json_stuff = open(template_file)
                        self._template = json.load(json_stuff)

        except Exception as x:
            logging.error('Exception caught in load_template(): {}'.format(x))
            traceback.print_exc(file=sys.stdout)
            return False

        return True

    def list(self):
        """
        List the existing stacks in the indicated region

        Args:
            None

        Returns:
            True if True

        Todo:
            Figure out what could go wrong and take steps
            to handle problems.
        """
        self._initialize_list()
        interested = True

        response = self._cloudFormation.list_stacks()
        print('Stack(s):')
        while interested:
            if 'StackSummaries' in response:
                for stack in response['StackSummaries']:
                    stack_status = stack['StackStatus']
                    if stack_status != 'DELETE_COMPLETE':
                        print('    [{}] - {}'.format(stack['StackStatus'], stack['StackName']))

            next_token = response.get('NextToken', None)
            if next_token:
                response = self._cloudFormation.list_stacks(NextToken=next_token)
            else:
                interested = False

        return True

    def smash(self):
        """
        Smash the given stack

        Args:
            None

        Returns:
            True if True

        Todo:
            Figure out what could go wrong and take steps
            to handle problems.
        """
        self._initialize_smash()
        try:
            stack_name = self._config.get('environment', {}).get('stack_name', None)
            response = self._cloudFormation.describe_stacks(StackName=stack_name)
            logging.debug('smash pre-flight returned: {}'.format(
                json.dumps(response,
                           indent=4,
                           default=json_util.default
                           )))
        except ClientError as wtf:
            logging.warning('your stack is in another castle [0].')
            return False
        except Exception as wtf:
            logging.error('failed to find intial status of smash candidate: {}'.format(wtf))
            return False

        response = self._cloudFormation.delete_stack(StackName=stack_name)
        logging.info('delete started for stack: {}'.format(stack_name))
        logging.debug('delete_stack returned: {}'.format(json.dumps(response, indent=4)))
        return self.poll_stack()

    def _init_boto3_clients(self):
        """
        The utililty requires boto3 clients to Cloud Formation and S3. Here is
        where we make them.

        Args:
            None

        Returns:
            Good or Bad; True or False
        """
        try:
            profile = self._config.get('environment', {}).get('profile')
            region = self._config.get('environment', {}).get('region')
            if profile:
                self._b3Sess = boto3.session.Session(profile_name=profile)
            else:
                self._b3Sess = boto3.session.Session()

            self._s3 = self._b3Sess.client('s3')
            self._cloudFormation = self._b3Sess.client('cloudformation', region_name=region)
            self._ssm = self._b3Sess.client('ssm', region_name=region)

            return True
        except Exception as wtf:
            logging.error('Exception caught in intialize_session(): {}'.format(wtf))
            traceback.print_exc(file=sys.stdout)
            return False

    def _fill_defaults(self):

        logging.info('def _fill_defaults')

        try:

            # If there are parameters in the template
            if 'Parameters' in self._template:
                parms = self._template['Parameters']

                print('parms: '+str(parms))
                for key in parms:
                    print('key: '+str(key))
                    key = str(key)
                    if 'Default' in parms[key] and key not in self._parameters:
                        self._parameters[key] = parms[key]['Default']

            if self._config.get('recipe_dir'):

                if self._config.get('recipe_url_parameter'):
                    self._parameters[self._config.get('recipe_url_parameter')] = str(self._recipeUrl)
                else:
                    logging.info('Need to specify the recipe url parameter')

            elif self._config.get('lambda_dir'):

                if self._config.get('lambda_bucket_parameter'):
                    self._parameters[self._config.get('lambda_bucket_parameter')] = str(self._lambdaBucket)
                else:
                    logging.info('Need to specify the lambda bucket parameter')

                if self._config.get('lambda_key_parameter'):
                    self._parameters[self._config.get('lambda_key_parameter')] = str(self._lambdaKey)
                else:
                    logging.info('Need to specify the lambda key parameter')

            logging.info('parameters are now: '+str(self._parameters))

        except Exception as wtf:
            logging.error('Exception caught in fill_defaults(): {}'.format(wtf))
            traceback.print_exc(file=sys.stdout)
            return False

        return True

    def _get_ssm_parameter(self, p):
        """
        Get parameters from Simple Systems Manager

        Args:
            p - a parameter name

        Returns:
            a value, decrypted if needed, if successful or None if things go
            sideways.
        """
        val = None
        secure_string = False
        try:
            response = self._ssm.describe_parameters(
                Filters=[{'Key': 'Name', 'Values': [p]}]
            )

            if 'Parameters' in response:
                t = response['Parameters'][0].get('Type', None)
                if t == 'String':
                    secure_string = False
                elif t == 'SecureString':
                    secure_string = True

                response = self._ssm.get_parameter(Name=p, WithDecryption=secure_string)
                val = response.get('Parameter', {}).get('Value', None)
        except Exception:
            pass

        return val

    def _fill_parameters(self):
        """
        Fill in the _parameters dict from the properties file.

        Args:
            None

        Returns:
            True

        Todo:
            Figure out what could go wrong and at least acknowledge the the
            fact that Murphy was an optimist.
        """

        logging.info('def _fill_parameters')

        self._parameters = self._config.get('parameters', {})
        self._fill_defaults()

        for k in self._parameters.keys():
            if self._parameters[k].startswith(self.SSM) and self._parameters[k].endswith(']'):
                parts = self._parameters[k].split(':')
                tmp = parts[1].replace(']', '')
                val = self._get_ssm_parameter(tmp)
                if val:
                    self._parameters[k] = val
                else:
                    logging.error('SSM parameter {} not found'.format(tmp))
                    return False
            elif self._parameters[k] == self.ASK:
                val = None
                a1 = '__x___'
                a2 = '__y___'
                prompt1 = "Enter value for '{}': ".format(k)
                prompt2 = "Confirm value for '{}': ".format(k)
                while a1 != a2:
                    a1 = getpass.getpass(prompt=prompt1)
                    a2 = getpass.getpass(prompt=prompt2)
                    if a1 == a2:
                        val = a1
                    else:
                        print('values do not match, try again')
                self._parameters[k] = val

        logging.info('parameters are now: '+str(self._parameters))


        return True

    def _read_tags(self):
        """
        Fill in the _tags dict from the tags file.

        Args:
            None

        Returns:
            True

        Todo:
            Figure what could go wrong and at least acknowledge the
            the fact that Murphy was an optimist.
        """
        tags = self._config.get('tags', {})
        for tag_name in tags.keys():
            tag = {}
            tag['Key'] = tag_name
            tag['Value'] = tags[tag_name]
            self._tags.append(tag)

        logging.info('Tags: {}'.format(json.dumps(
            self._tags,
            indent=4,
            sort_keys=True
        )))
        return True

    def _set_update(self):
        """
        Determine if we are creating a new stack or updating and existing one.
        The update member is set as you would expect at the end of this query.

        Args:
            None

        Returns:
            True
        """
        try:
            self._updateStack = False
            stack_name = self._config.get('environment', {}).get('stack_name', None)
            response = self._cloudFormation.describe_stacks(StackName=stack_name)
            stack = response['Stacks'][0]
            if stack['StackStatus'] == 'ROLLBACK_COMPLETE':
                logging.info('stack is in ROLLBACK_COMPLETE status and should be deleted')
                del_stack_resp = self._cloudFormation.delete_stack(StackName=stack_name)
                logging.info('delete started for stack: {}'.format(stack_name))
                logging.debug('delete_stack returned: {}'.format(json.dumps(del_stack_resp, indent=4)))
                stack_delete = self.poll_stack()
                if not stack_delete:
                    return False

            if stack['StackStatus'] in ['CREATE_COMPLETE', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_COMPLETE']:
                self._updateStack = True
        except:
            self._updateStack = False

        logging.info('update_stack: ' + str(self._updateStack))
        return True

    def _archive_elements(self):
        """
        Cloud Formation likes to take the template from S3 so here we put the
        template into S3. We also store the parameters file that was used in
        this run. Note: you can pass anything as the version string but you
        should at least consider a version control tag or git commit hash as
        the version.

        Args:
            None

        Returns:
            True if the stuff lands in S3 or False if the file doesn't
            really exist or the upload goes sideways.
        """

        logging.info('def _archive_elements')

        try:

            if self._config.get('recipe_dir'):

                now = time.gmtime()
                stub = "templates/{stack_name}/{version}".format(
                    stack_name=self._config.get('environment', {}).get('stack_name', None),
                    version=self._config.get('codeVersion')
                )

                stub = stub + "/" + str(now.tm_year)
                stub = stub + "/" + str('%02d' % now.tm_mon)
                stub = stub + "/" + str('%02d' % now.tm_mday)
                stub = stub + "/" + str('%02d' % now.tm_hour)
                stub = stub + ":" + str('%02d' % now.tm_min)
                stub = stub + ":" + str('%02d' % now.tm_sec)

                recipefile_key = stub + "/recipe.zip"


                logging.info('recipefile_key: '+str(recipefile_key))

                CMD = "cd " + str(self._config.get('recipe_dir')) + " && /usr/bin/zip -r recipe.zip *"

                logging.info(CMD)

                PROCESS = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

                while True:
                    out = PROCESS.stdout.read(1)
                    if out == '' and PROCESS.poll() != None:
                        break
                    if out != '':
                        sys.stdout.write(out)
                        sys.stdout.flush()



                recipe_file = os.path.join(self._config.get('recipe_dir'), 'recipe.zip')

                logging.info('recipe file: '+str(recipe_file))


                bucket = self._config.get('environment', {}).get('bucket', None)
                if not os.path.isfile(recipe_file):
                    logging.info("{} is not actually a file".format(recipe_file))
                    return False

                logging.info('Copying {} to s3://{}/{}'.format(recipe_file, bucket, recipefile_key))
                self._s3.upload_file(recipe_file, bucket, recipefile_key)

                self._recipeUrl = 'https://s3.amazonaws.com/{}/{}'.format(bucket, recipefile_key)

                # Remove zip file
                if os.path.exists(recipe_file):
                    os.remove(recipe_file)


                logging.info("recipe_url: " + self._recipeUrl)

                logging.info('parameters are: '+str(self._parameters))

                if self._config.get('recipe_url_parameter'):
                    self._parameters[self._config.get('recipe_url_parameter')]=self._recipeUrl

                logging.info('parameters are now: '+str(self._parameters))

                # Delete the recipe

            elif self._config.get('lambda_dir'):

                now = time.gmtime()
                stub = "templates/{stack_name}/{version}".format(
                    stack_name=self._config.get('environment', {}).get('stack_name', None),
                    version=self._config.get('codeVersion')
                )

                # Create a datetime stamp
                stub = stub + "/" + str(now.tm_year)
                stub = stub + "/" + str('%02d' % now.tm_mon)
                stub = stub + "/" + str('%02d' % now.tm_mday)
                stub = stub + "/" + str('%02d' % now.tm_hour)
                stub = stub + ":" + str('%02d' % now.tm_min)
                stub = stub + ":" + str('%02d' % now.tm_sec)


                lambdafile_key = stub + "/lambda.zip"


                logging.info('lambdafile_key: '+str(lambdafile_key))

                # Zip the lambda directory
                CMD = "cd " + str(self._config.get('lambda_dir')) + " && /usr/bin/zip -r lambda.zip *"

                logging.info(CMD)

                PROCESS = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

                while True:
                    out = PROCESS.stdout.read(1)
                    if out == '' and PROCESS.poll() != None:
                        break
                    if out != '':
                        sys.stdout.write(out)
                        sys.stdout.flush()


                lambda_file = os.path.join(self._config.get('lambda_dir'), 'lambda.zip')

                logging.info('lambda file: '+str(lambda_file))


                bucket = self._config.get('environment', {}).get('bucket', None)
                if not os.path.isfile(lambda_file):
                    logging.info("{} is not actually a file".format(lambda_file))
                    return False

                logging.info('Copying {} to s3://{}/{}'.format(lambda_file, bucket, lambdafile_key))
                self._s3.upload_file(lambda_file, bucket, lambdafile_key)

                self._lambdaBucket = bucket
                self._lambdaKey = lambdafile_key

                # Remove zip file
                if os.path.exists(lambda_file):
                    os.remove(lambda_file)


                logging.info("lambda_bucket: " + self._lambdaBucket)
                logging.info("lambda_key: " + self._lambdaKey)


                logging.info('parameters are: '+str(self._parameters))

                if self._config.get('lambda_bucket_parameter'):
                    self._parameters[self._config.get('lambda_bucket_parameter')]=self._lambdaBucket

                if self._config.get('lambda_key_parameter'):
                    self._parameters[self._config.get('lambda_key_parameter')]=self._lambdaKey

                logging.info('parameters are now: '+str(self._parameters))

                # Delete the lambda



            stackfile_key, propertyfile_key = self._craft_s3_keys()

            template_file = self._template_file

            bucket = self._config.get('environment', {}).get('bucket', None)
            if not os.path.isfile(template_file):
                logging.info("{} is not actually a file".format(template_file))
                return False

            logging.info('Copying parameters to s3://{}/{}'.format(bucket, propertyfile_key))
            temp_file_name = '/tmp/{}'.format((str(uuid.uuid4()))[:8])
            with open(temp_file_name, 'w') as dump_file:
                json.dump(self._parameters, dump_file, indent=4)


            self._s3.upload_file(temp_file_name, bucket, propertyfile_key)

            logging.info('Copying {} to s3://{}/{}'.format(template_file, bucket, stackfile_key))
            self._s3.upload_file(template_file, bucket, stackfile_key)

            self._templateUrl = 'https://s3.amazonaws.com/{}/{}'.format(bucket, stackfile_key)
            logging.info("template_url: " + self._templateUrl)

            self._cleanup_stackility_generated_templates()

            return True
        except Exception as x:
            logging.error('Exception caught in copy_stuff_to_S3(): {}'.format(x))
            traceback.print_exc(file=sys.stdout)
            return False






    def _craft_s3_keys(self):
        """
        We are putting stuff into S3, were supplied the bucket. Here we
        craft the key of the elements we are putting up there in the
        internet clouds.

        Args:
            None

        Returns:
            a tuple of teplate file key and property file key
        """
        now = time.gmtime()
        stub = "templates/{stack_name}/{version}".format(
            stack_name=self._config.get('environment', {}).get('stack_name', None),
            version=self._config.get('codeVersion')
        )

        stub = stub + "/" + str(now.tm_year)
        stub = stub + "/" + str('%02d' % now.tm_mon)
        stub = stub + "/" + str('%02d' % now.tm_mday)
        stub = stub + "/" + str('%02d' % now.tm_hour)
        stub = stub + ":" + str('%02d' % now.tm_min)
        stub = stub + ":" + str('%02d' % now.tm_sec)

        if self._config.get('yaml'):
            template_key = stub + "/stack.yaml"
        else:
            template_key = stub + "/stack.json"

        property_key = stub + "/stack.properties"
        return template_key, property_key

    def poll_stack(self):
        """
        Spin in a loop while the Cloud Formation process either fails or succeeds

        Args:
            None

        Returns:
            Good or bad; True or False
        """
        logging.info('polling stack status, POLL_INTERVAL={}'.format(POLL_INTERVAL))
        time.sleep(POLL_INTERVAL)
        completed_states = [
            'CREATE_COMPLETE',
            'UPDATE_COMPLETE',
            'DELETE_COMPLETE'
        ]
        stack_name = self._config.get('environment', {}).get('stack_name', None)
        while True:
            try:
                response = self._cloudFormation.describe_stacks(StackName=stack_name)
                stack = response['Stacks'][0]
                current_status = stack['StackStatus']
                logging.info('Current status of {}: {}'.format(stack_name, current_status))
                if current_status.endswith('COMPLETE') or current_status.endswith('FAILED'):
                    if current_status in completed_states:
                        return True
                    else:
                        return False

                time.sleep(POLL_INTERVAL)
            except ClientError as wtf:
                if str(wtf).find('does not exist') == -1:
                    logging.error('Exception caught in wait_for_stack(): {}'.format(wtf))
                    traceback.print_exc(file=sys.stdout)
                    return False
                else:
                    logging.info('{} is gone'.format(stack_name))
                    return True
            except Exception as wtf:
                logging.error('Exception caught in wait_for_stack(): {}'.format(wtf))
                traceback.print_exc(file=sys.stdout)
                return False

    def _initialize_list(self):
        if not self._init_boto3_clients():
            logging.error('session initialization was not good')
            raise SystemError

    def _initialize_smash(self):
        if not self._init_boto3_clients():
            logging.error('session initialization was not good')
            raise SystemError

    def _validate_ini_data(self):
        if 'stack_name' not in self._config.get('environment', {}):
            return False
        elif 'bucket' not in self._config.get('environment', {}):
            return False
        elif 'template' not in self._config.get('environment', {}):
            return False
        else:
            return True

    def _initialize_upsert(self):
        if not self._validate_ini_data():
            logging.error('INI file missing required bits; bucket and/or template and/or stack_name')
            raise SystemError
        elif not self._load_template():
            logging.error('template initialization was not good')
            raise SystemError
        elif not self._init_boto3_clients():
            logging.error('session initialization was not good')
            raise SystemError
        elif not self._fill_parameters():
            logging.error('parameter setup was not good')
            raise SystemError
        elif not self._read_tags():
            logging.error('tags initialization was not good')
            raise SystemError
        elif not self._archive_elements():
            logging.error('saving stuff to S3 did not go well')
            raise SystemError
        elif not self._set_update():
            logging.error('there was a problem determining update or create')
            raise SystemError

    def _render(self,tpl_path):

        tags_parms = self._config.get('tags')
        env_parms = self._config.get('environment')

        all_parms = tags_parms.copy()
        all_parms.update(env_parms)

        logging.info('All parameters: '+str(all_parms))
        path, filename = os.path.split(tpl_path)
        return jinja2.Environment(
            loader=jinja2.FileSystemLoader(path or './')
        ).get_template(filename).render(all_parms)


    def _render_template(self,type):

        logging.info('def _render_tempalte')
        template_file = self._config.get('environment', {}).get('template', None)

        logging.info('template file: '+str(template_file))
        if type == 'yaml':
            if self._config.get('project_dir'):
                new_template_file = os.path.join(self._config.get('project_dir'),template_file)
                newest_template_file = new_template_file+'.stackility_generated.yml'
                output_from_parsed_template = self.render(new_template_file)
                with open(newest_template_file, "wb") as f:
                    f.write(output_from_parsed_template)
                return new_template_file
            else:
                template_file = self._config.get('environment', {}).get('template', None)
                new_template_file = template_file+'.stackility_generated.yml'
                output_from_parsed_template = self.render(template_file)
                with open(new_template_file, "wb") as f:
                    f.write(output_from_parsed_template)
                return new_template_file

        elif type == 'json':
            if self._config.get('project_dir'):
                new_template_file = os.path.join(self._config.get('project_dir'), template_file)

                logging.info('new template file: '+str(new_template_file))
                newest_template_file = new_template_file+'.stackility_generated.json'
                output_from_parsed_template = self._render(new_template_file)
                with open(newest_template_file, "wb") as f:
                    f.write(output_from_parsed_template)
                return new_template_file
            else:
                template_file = self._config.get('environment', {}).get('template', None)
                new_template_file = template_file+'.stackility_generated.json'
                output_from_parsed_template = self.render(template_file)
                with open(new_template_file, "wb") as f:
                    f.write(output_from_parsed_template)
                return new_template_file

        else:
            logging.error('Error trying to render jinja2 template')
            raise SystemError

    def _cleanup_stackility_generated_templates(self):

        logging.info('def _cleanup_stackility_generated_templates -line: '+str(self._lineno()))
        if self._config.get('project_dir'):
            logging.info('def _cleanup_stackility_generated_templates - has project directory -line: ' + str(self._lineno()))

            onlyfiles = [f for f in listdir(self._config.get('project_dir')) if isfile(join(self._config.get('project_dir'), f))]

            for f in onlyfiles:
                logging.info(f)
                #if f.endswith('stackility_generated.json'):
                #    os.remove(f)
                #elif f.endswith('stackility_generated.yaml'):
                #    os.remove(f)
        else:
            logging.info('def _cleanup_stackility_generated_templates - has project directory -line: ' + str(self._lineno()))

            onlyfiles = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f))]
            for f in onlyfiles:
                logging.info(f)
                #if f.endswith('stackility_generated.json'):
                #    os.remove(f)
                #elif f.endswith('stackility_generated.yaml'):
                #    os.remove(f)

    def _is_valid_json(self,path):
        try:
            json_stuff = open(path)
            json_object = json.load(json_stuff)
        except ValueError, e:
            logging.info('Invalid json in '+str(path)+' - '+str(e))
            raise SystemError
        return True


    def _convert_json_template_to_yaml(self,file):


        new_template_file = os.path.splitext(file)[0] + '.yaml'

        # If executing outside of the project directory, then
        # add the project directory
        logging.info('def _convert_json_template_to_yaml - file: '+str(file)+' - line: '+str(self._lineno()))
        logging.info('template to flip: '+str(file)+' - line: '+str(self._lineno()))
        logging.info('new template file: '+str(new_template_file)+' - line: '+str(self._lineno()))

        try:


            CMD = "cfn-flip -y -l " + str(file) + " " + str(new_template_file)

            print(CMD)
            ## run it ##

            STACKS = []
            PROCESS = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            while True:
                out = PROCESS.stdout.read(1)
                if out == '' and PROCESS.poll() != None:
                    break
                if out != '':
                    sys.stdout.write(out)
                    sys.stdout.flush()

        except OSError as e:
            logging.error("OSError > ", e.errno)
            logging.error("OSError > ", e.strerror)
            logging.error("OSError > ", e.filename)

        except:
            logging.error("Error > ", sys.exc_info()[0])

        new_template_file = os.path.splitext(file)[0] + '.yaml'
        logging.info('def _convert_json_template_to_yaml - new template files is: '+str(new_template_file)+' - line: '+str(self._lineno()))

        return new_template_file