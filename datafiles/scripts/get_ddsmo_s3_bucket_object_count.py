#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
from datetime import datetime, time, timedelta
import pytz # $ pip install pytz
import re
from botocore.exceptions import ClientError

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#############################'
        print '## Select S3 Buckets       ##'
        print '#############################'

        bucket_list=[]

        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

            matchObj = re.match(r'ddsmo-.*',str(menu[key][0]),re.M|re.I)

            if matchObj:
                print('match: '+str(matchObj.group()))
                bucket_list.append(matchObj.group())



        tz = pytz.timezone("America/Chicago")
        yesterday = datetime.now(tz).date() - timedelta(days=1)
        today = datetime.now(tz).date()
        print('today: '+str(today))


        (yyear,ymonth,yday)=str(yesterday).split('-')
        (tyear,tmonth,tday)=str(today).split('-')

        client = session.client('cloudwatch')

        sum = 0
        for bucket_name in bucket_list:

            #print(str(bucket_name))

            try:
                response = client.get_metric_statistics(
                    Namespace='AWS/S3',
                    MetricName='NumberOfObjects',
                    Dimensions=[
                        {
                            'Name': 'BucketName',
                            'Value': str(bucket_name)
                        },
                        {
                            'Name': 'StorageType',
                            'Value': 'AllStorageTypes'
                        }
                    ],


                    StartTime=datetime(int(yyear),int(ymonth),int(yday)),
                    EndTime=datetime(int(tyear),int(tmonth),int(tday)),
                    Period=60,
                    Statistics=[
                        'Average'
                    ],
                    Unit='Count'
                )

                #print(response)
                if response['Datapoints']:
                    if len(response['Datapoints'])>0:
                        print(str(bucket_name)+' - Object Count: '+str(response['Datapoints'][0]['Average']))
                        sum = sum + int(response['Datapoints'][0]['Average'])
                    else:
                        print(str(bucket_name)+' - Object Count: 0')

            except ClientError as e:
                print("Error: %s" %e)

        print('Total object:'+str(sum))

    else:
        print("\n")
        print("#######################")
        print('No S3 buckets found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
