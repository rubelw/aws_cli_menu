#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import subprocess
import os

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy From   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break


        from_bucket_name = info['Name']

        response = client.list_objects(
            Bucket=from_bucket_name,
        )

        objects = response.get('Contents')

        if len(objects) > 0:

            menu = {}
            counter = 0
            for s in objects:
                counter += 1
                my_list = []
                my_list.append(s['Key'])
                my_list.append(s['ETag'])
                my_list.append(s)
                menu[counter] = my_list

            print "\n\n"
            print '#####################################'
            print '## Select file to copy   ##'
            print '#####################################'
            for key in sorted(menu):
                print str(key) + ": " + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        file_info = menu[int(ans)][2]
                        break



            key = file_info['ETag']
            filename = file_info['Key']

            print('key: '+str(key))
            print('file name: '+str(filename))
            print('bucket name: '+str(from_bucket_name))



    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:

            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy To   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        to_bucket_name = info['Name']


        print('to bucket: '+str(to_bucket_name))


        response = client.copy_object(
            Bucket=to_bucket_name,
            CopySource= {
                'Bucket': from_bucket_name,
                'Key': filename
            },
            Key='test.jar'
        )

        print(pretty(response))

    else:
        print("\n")
        print("#######################")
        print('No S3 buckets found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
