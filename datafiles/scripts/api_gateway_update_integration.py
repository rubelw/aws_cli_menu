#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('apigateway')
    response = client.get_rest_apis()

    #print(pretty(response))
    stacks = response.get('items')


    if len(stacks)>0:

        menu = {}
        counter=0

        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['name'])
            if 'description' in s:
                my_list.append(s['description'])
            else:
                my_list.append('No description')
            my_list.append(s)
            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select API'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][2]
                    break


        api_id = info['id']


        response = client.get_resources(
            restApiId=str(api_id),
        )


        print(pretty(response))

        items=response['items']

        if len(items)>0:

            menu = {}
            counter=0

            for s in items:

                if 'resourceMethods' in s:
                    counter+=1
                    my_list = []
                    my_list.append(s['path'])
                    my_list.append(s['id'])
                    my_list.append(s)
                    menu[counter]=my_list

            print "\n"
            print '#############################'
            print '## Select Method'
            print '#############################'
            for key in sorted(menu):
                print str(key)+":" + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][2]
                        print(pretty(menu[int(ans)][2]))
                        methods = menu[int(ans)][2]['resourceMethods']
                        break




        parent_id = info['id']
        parent_path = info['path']

        lambda_uri = raw_input("Enter new lambda uri: [ENTER](Cntrol-C to exit)")
        print('restApiId:'+str(api_id))
        print('resourceId: '+str(parent_id))

        print('lambda_uri:'+str(lambda_uri))


        for m in methods:

            if m != 'OPTIONS':


                if 1:
                    response = client.get_integration(
                        restApiId=str(api_id),
                        resourceId=str(parent_id),
                        httpMethod=str(m)
                    )

                    print(pretty(response))

                    integrationResponses = response['integrationResponses']
                    passthroughBehavior = response['passthroughBehavior']
                    cacheKeyParameters = response['cacheKeyParameters']
                    uri= response['uri']
                    httpMethod= response['httpMethod']
                    requestTemplates= response['requestTemplates']
                    cacheNamespace=response['cacheNamespace']
                    type=response['type']

                    response = client.delete_integration(
                        restApiId=str(api_id),
                        resourceId=str(parent_id),
                        httpMethod=str(m)
                    )
                    print(pretty(response))

                    response = client.put_integration(
                        restApiId=str(api_id),
                        resourceId=str(parent_id),
                        httpMethod=str(m),
                        type=str(type),
                        integrationHttpMethod=str(httpMethod),
                        uri=str(uri),
                        credentials='Do not add caller credentials to cache key',
                        requestTemplates=requestTemplates,
                        passthroughBehavior=str(passthroughBehavior),
                        cacheKeyParameters=cacheKeyParameters

                    )

                else:

                    print('method: '+str(m))

                    response = client.update_integration(
                        restApiId=str(api_id),
                        resourceId=str(parent_id),
                        httpMethod=str(m),
                        patchOperations=[
                          {
                            'op': 'replace',
                            'path': '/uri',
                            'value': str(lambda_uri)

                           }
                        ]
                    )

                    print(pretty(response))

    else:
        print("\n")
        print("##########################")
        print('No APIs Found')
        print("##########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
