#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


import sys

try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('logs')

    log_group = raw_input("Enter log group name: [ENTER](Cntrl-C to exit)")
    filter_name= raw_input("Enter filter name: [ENTER](Cntrl-C to exit)")
    destination_arn =raw_input("Enter destination arn: [ENTER](Cntrl-C to exit)")

    response = client.put_subscription_filter(
        logGroupName=str(log_group),
        filterName=str(filter_name),
        filterPattern='',
        destinationArn=str(destination_arn)
    )

    print(response)



except (KeyboardInterrupt, SystemExit):
    sys.exit()
