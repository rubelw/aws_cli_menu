#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('lambda')

    function_name = raw_input("Enter function name: [ENTER](Cntrl-C to exit)")
    sid = raw_input("Enter unique id: [ENTER](Cntrl-C to exit)")
    action = raw_input("Enter action: [ENTER](Cntrl-C to exit)")
    principal = raw_input("Enter principal: [ENTER](Cntrl-C to exit)")
    source_arn= raw_input("Enter source_arn: [ENTER](Cntrl-C to exit)")

    response = client.add_permission(
        FunctionName=str(function_name),
        StatementId=str(sid),
        Action=action,
        Principal=str(principal),
        SourceArn=source_arn
    #    SourceAccount='string',
    #    EventSourceToken='string',
    #    Qualifier='string'
    )

    print(response)

except (KeyboardInterrupt, SystemExit):
    sys.exit()
