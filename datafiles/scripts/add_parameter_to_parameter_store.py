#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    project_name = raw_input("Enter project name for parameter store: [ENTER](Cntrl-C to exit)")
    environment = raw_input("Enter enviornment name for parameter store: [ENTER](Cntrl-C to exit)")
    key_name = raw_input("Enter key_name name for parameter store: [ENTER](Cntrl-C to exit)")


    parameter = str(project_name)+'_'+str(environment)+'_'+str(key_name)

    if yes_or_no('The proposed key name will be '+str(parameter)+'.  Does this look correct?'):

        key_description = raw_input("Enter key description for parameter store: [ENTER](Cntrl-C to exit)")
        key_value = raw_input("Enter key value for parameter store: [ENTER](Cntrl-C to exit)")

        key_type = multiple_choice('Select key type', ['String','SecureString'])


        print('parameter: '+str(parameter))
        print('key_description: '+str(key_description))
        print('key_value: '+str(key_value))
        raw_input("Press any key to continue: [ENTER](Cntrl-C to exit)")


        profile_name = get_profile_name()

        session = boto3.session.Session(profile_name=profile_name)

        client = session.client('ssm')

        if key_type == 'SecureString':

            kms_client = session.client('kms')

            response = kms_client.list_aliases(
                Limit=99
            )

            print(pretty(response))
            new_aliases = {}
            if 'Aliases' in response:
                aliases = response.get('Aliases')

                for a in aliases:
                    #print(pretty(a))
                    if 'AliasName' in a:

                        if 'TargetKeyId' in a:
                            # print('There is an alias name in dict')
                            new_aliases[str(a['TargetKeyId'])] = []
                            new_aliases[str(a['TargetKeyId'])].append(a['AliasName'])
                            new_aliases[str(a['TargetKeyId'])].append(a['AliasArn'])

            #print(pretty(new_aliases))
            response = kms_client.list_keys(
                Limit=99,
            )

            # print(pretty(response))

            if 'Keys' in response:
                keys = response.get('Keys')
                menu = {}
                counter = 0

                for k in keys:
                    counter += 1
                    my_list = []
                    my_list.append(k['KeyArn'])
                    my_list.append(k['KeyId'])
                    if str(k['KeyId']) in new_aliases:
                        my_list.append(new_aliases[str(k['KeyId'])][0])
                        my_list.append(new_aliases[str(k['KeyId'])][1])
                    else:
                        my_list.append('None')
                        my_list.append('None')
                    my_list.append(k)
                    menu[counter] = my_list

                if len(menu) > 0:
                    print "\n"
                    print '#########################################'
                    print '## Select KeyArn                       ##'
                    print '#########################################'
                    for key in sorted(menu):
                        print str(key) + ":" + menu[key][2] + " " + menu[key][1]

                    pattern = r'^[0-9]+$'
                    while True:
                        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in menu:
                                info = menu[int(ans)][4]
                                break

                #print(pretty(info))

                print("\n##################################")
                print('Alias Name: ' + str(menu[int(ans)][2]))
                print('Alias ARN: ' + str(menu[int(ans)][3]))
                print("##################################")
                response = kms_client.describe_key(
                    KeyId=info['KeyId'],
                )

                #print(pretty(response))


                response = client.put_parameter(
                    Name=str(parameter),
                    Description=str(key_description),
                    Value=str(key_value),
                    Type=str(key_type),
                    KeyId=str(info['KeyId']),
                    Overwrite=False
                    #AllowedPattern='string'
                )
        else:

            response = client.put_parameter(
                Name=str(parameter),
                Description=str(key_description),
                Value=str(key_value),
                Type=str(key_type),
                Overwrite=False
                #AllowedPattern='string'
            )


        print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
