#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)


    account_id = session.client('sts').get_caller_identity().get('Account')
    print('account id: '+str(account_id))


    client = session.client('ec2')

    response = client.describe_images(
            Filters=[
        {
            'Name': 'owner-id',
            'Values': [
                str(account_id)
            ]
        },
    ]
    )
    #print(response)

    if 'Images' in response:
        stacks = response.get('Images')
        menu = {}
        counter=0

        for i in stacks:
            print('###########################')
            print(pretty(i))
            counter+=1
            my_list = []
            if 'Name' in i:
                my_list.append(i['Name'])
            else:
                my_list.append('no_name')

            my_list.append(i['ImageId'])
            my_list.append(i)
            menu[counter]=my_list

        if len(menu)>0:
            print "\n"
            print '#########################################'
            print '## Select Image                        ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key)+":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern,ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][2]
                        break

            print("\n")
            print("###############################")
            print("Detailed Image Information")
            print("###############################")
            print(pretty(info))

        else:
            print("\n")
            print("##############")
            print('No Images')
            print("##############")
    else:
        print("\n")
        print("###############")
        print('No Images')
        print("###############")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
