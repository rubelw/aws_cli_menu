#!/usr/bin/env python

import sys
import subprocess
import boto3.session
from aws_cli_menu_helper import *
import re
import stackility

ACCOUNNTS = {
    "dsi-nonprod" : "np",
    "dsi-prod":"pd",
    "dsi-sandbox":"sb",
    "dsi-utility":"ut",
    "phigalileo":"ga"
}


def remove_non_jinja2_cf_template(ini_data):
    """
    Removes the non .j2 template file so there is only a .j2 template

    Args:
        ini_data

    Returns:
        Success or error

    Todo:
        Figure out what could go wrong and take steps
        to hanlde problems.
    """
    template_file= ini_data['environment']['template']
    template_file = template_file[:-3]

    if 'project_dir' in ini_data:
        template_file = os.path.join(ini_data['project_dir'], template_file)

    try:
        os.remove(template_file)
    except OSError, e:  ## if failed ##
        print("Exception caught in deleting template file:  %s - %s." % (e.filename, e.strerror))


def read_config_info(ini_file):

    print('def read_config_info: ' + str(ini_file))
    try:
        config = ConfigParser.ConfigParser()
        config.optionxform = str
        config.read(ini_file)
        the_stuff = {}
        for section in config.sections():
            the_stuff[section] = {}
            for option in config.options(section):
                the_stuff[section][option] = config.get(section, option)

        return the_stuff
    except Exception as wtf:
        print('Exception caught in read_config_info(): {}'.format(wtf))
        return sys.exit(1)



def validate_config_info():
    return True



try:
    STACKILITY_DIR = get_stackility_project_directory()
    #print STACKILITY_DIR

    DIRS = []
    for child in os.listdir(STACKILITY_DIR):
        path = os.path.join(STACKILITY_DIR, child)
        if os.path.isdir(path):
            DIRS.append(path)

    #print DIRS

    MENU = {}
    UNSORTED_MENU = {}

    COUNTER = 0

    for d in DIRS:
        list = []


        list.append(d)

        matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

        if matchObj:
            list.append(matchObj.group(1))
            UNSORTED_MENU[matchObj.group(1)]= list


    for key in sorted(UNSORTED_MENU):
        COUNTER+=1

        MENU[COUNTER] = UNSORTED_MENU[key]

    print "\n"
    print "####################################"
    print "Select Project"
    print "####################################"

    for key in sorted(MENU):
        print str(key) + ": " + MENU[key][1]

    while True:

        USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
        if int(USER_RESPONSE) in MENU:
            PROJECT = MENU[int(USER_RESPONSE)][0]
            break





    PROJECT_DIRS = []
    PROJECT_DIRECTORY = os.path.join(STACKILITY_DIR,PROJECT)
    for child in os.listdir(PROJECT_DIRECTORY):
        path = os.path.join(PROJECT_DIRECTORY, child)
        if os.path.isdir(path):
            PROJECT_DIRS.append(path)


    for d in PROJECT_DIRS:

        ini_files = []
        for child in os.listdir(d):
            path = os.path.join(d, child)

            if path.endswith('config'):
                print('config directory')



                for child in os.listdir(path):
                    ini_files.append(d+'/config/'+child)


        print(ini_files)

        for i in ini_files:

            ini_data = read_config_info(INI_FILE)

            ini_data['output_yaml'] = True

            if d:
                ini_data['project_dir'] = d

            ini_data['dryrun'] = False
            stack_driver = stackility.CloudStackUtility(ini_data)

            if stack_driver.upsert():
                print('stack create/update was started successfully.')
                if stack_driver.poll_stack():
                    print('stack create/update was finished successfully.')
                    print(ini_data)
                    if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(0)
                else:
                    logging.error('stack create/update was did not go well.')
                    if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(1)


except (KeyboardInterrupt, SystemExit):
    sys.exit()
