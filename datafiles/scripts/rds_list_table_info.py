#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re

try:
    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("select datname from pg_database where datistemplate = false;")

        rows = cur.fetchall()
    except psycopg2.OperationalError as e:
        print('Error: \n{0}').format(e)
        sys.exit(1)

    schemas = {}


    for row in rows:
        print(row)
        schemas[row[0]]=row

    cur.close()
    conn.close()

    menu = {}

    counter = 0

    for i in sorted(schemas):
        counter +=1
        menu[counter]=schemas[i]


    print(pretty(menu))
    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select Database")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    database_name = menu[int(ans)][0]
                    break

        print('database name: '+str(database_name))

        conn_string = 'dbname=\'' + str(database_name) + '\' user=\'' + str(
            creds[profile][0]['user']) + '\' host=\'localhost\' port=\'' + str(
            creds[profile][0]['port']) + '\' password=\'' + str(creds[profile][0]['password']) + '\''



        try:
            conn=psycopg2.connect(conn_string)

            cur = conn.cursor()
            cur.execute("select schema_name from information_schema.schemata;")

            rows = cur.fetchall()
        except psycopg2.OperationalError as e:
            print('Error: \n{0}').format(e)
            sys.exit(1)

        schemas = {}


        for row in rows:
            schemas[row[0]]=row

        cur.close()
        conn.close()

        menu = {}

        counter = 0

        for i in sorted(schemas):
            counter +=1
            menu[counter]=schemas[i]


        #print(pretty(menu))
        if len(menu) > 0:

            print("\n")
            print("#################################")
            print("Select Schema")
            print("#################################")

            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        schema_name = menu[int(ans)][0]
                        break

            #print('schema name: '+str(schema_name))


            try:
                conn=psycopg2.connect(conn_string)

                cur = conn.cursor()
                cur.execute("SELECT * from information_schema.tables WHERE table_schema = \'"+str(schema_name)+ "\'")

                rows = cur.fetchall()

                tables = {}
                counter = 0
                for row in rows:
                    print(pretty(row))
                    counter+=1
                    tables[counter]= row[2]


                print(pretty(tables))

                if len(tables) > 0:

                    print("\n")
                    print("#################################")
                    print("Select Table")
                    print("#################################")

                    for key in sorted(tables):
                        print str(key) + ":" + tables[key]

                    pattern = r'^[0-9]+$'
                    while True:
                        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in tables:
                                table_name = tables[int(ans)]
                                break


                    print('table name: '+str(table_name))


                    menu = {1:'Permissions', 2:'Record Count'}

                    print("\n")
                    print("#################################")
                    print("Select Option")
                    print("#################################")

                    for key in sorted(menu):
                        print str(key) + ":" + menu[key]


                    pattern = r'^[0-9]+$'
                    while True:
                        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in menu:
                                option_value = int(ans)
                                break



                    if option_value == 1:
                        print('Table Permissions')

                        permissions = {}

                        try:
                            conn=psycopg2.connect(conn_string)

                            cur = conn.cursor()
                            cur.execute("SELECT grantee, privilege_type FROM information_schema.role_table_grants WHERE table_name= '"+str(table_name)+ "\'")

                            rows = cur.fetchall()

                            for row in rows:
                                #print(pretty(row))

                                if not row[0] in permissions.keys():

                                    permissions[row[0]] = []
                                    permissions[row[0]].append(row[1])
                                else:
                                    permissions[row[0]].append(row[1])

                        except psycopg2.OperationalError as e:
                            print('Error: \n{0}').format(e)
                            sys.exit(1)

                        print("###############################")
                        print("Permissions")
                        print("###############################")
                        print(pretty(permissions))

                    elif option_value ==2:
                        print('Record Counts')

                        try:
                            conn=psycopg2.connect(conn_string)

                            cur = conn.cursor()
                            cur.execute("SELECT count(*) from "+str(schema_name)+'.'+str(table_name))

                            rows = cur.fetchall()

                            count = int(rows[0][0])

                            print('Record count: '+str(count))
                            #for row in rows:
                            #    print(pretty(row))


                        except psycopg2.OperationalError as e:
                            print('Error: \n{0}').format(e)
                            sys.exit(1)

                    else:
                        print('Unknown option')
                        sys.exit(-1)

                else:
                    print('There are no tables')
                    sys.exit(-1)

            except psycopg2.OperationalError as e:
                print('Error: \n{0}').format(e)
                sys.exit(1)

            cur.close()
            conn.close()

        else:
            print("\n")
            print("####################")
            print('No are no schemas')
            print("####################")








except (KeyboardInterrupt, SystemExit):
    sys.exit()