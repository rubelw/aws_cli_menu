#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    response = list_hosted_zones(session)

    stacks = response.get('HostedZones')

    if len(stacks)>0:

        menu = {}
        counter = 0
        for item in stacks:
            counter +=1
            my_list=[]
            my_list.append(item['Name'])
            my_list.append(item['Id'])
            menu[counter]=my_list


        print("\n")
        print("#################################")
        print("Select Hosted Zones")
        print("#################################")
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    id = menu[int(ans)][1]
                    domain = menu[int(ans)][0]
                    break



        client = session.client('route53')
        response = client.list_resource_record_sets(
            HostedZoneId=id
        )



        matchObj = re.match( r'^(/.*/)(.*?)$', id, re.M|re.I)

        if matchObj:
           hosted_zone_id = matchObj.group(2)
        else:
           print "No match!!"
           sys.exit(-1)

        if 'ResourceRecordSets' in response:

            print("\n")


            stacks = response['ResourceRecordSets']


            if len(stacks)>0:

                menu = {}
                counter = 0
                for item in stacks:
                    counter +=1
                    my_list=[]
                    my_list.append(item['Name'])
                    my_list.append(item['Type'])
                    my_list.append(item)
                    menu[counter]=my_list


                print("\n")
                print("#################################")
                print("Select Record Set")
                print("#################################")
                for key in sorted(menu):
                    print str(key) + ":" + menu[key][0]

                pattern = r'^[0-9]+$'
                while True:
                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            name = menu[int(ans)][0]
                            type = menu[int(ans)][1]
                            info = menu [int(ans)][2]
                            break

                print("\n")
                print("name: "+str(name))
                print('type: '+str(type))
                print("hosted zone id: "+str(hosted_zone_id))
                print("domain: "+str(domain))
                print(pretty(info))

                if str(type) == "A":
                    ans = raw_input("Enter new record set value: [ENTER](Cntrl-C to exit)")


                    result = client.change_resource_record_sets(
                        HostedZoneId=str(hosted_zone_id),
                        ChangeBatch={
                            'Comment': 'Update record set',
                            'Changes': [{
                                'Action': 'UPSERT',
                                'ResourceRecordSet': {
                                    'Name': str(name),
                                    'Type': str(type),
                                    'AliasTarget': {
                                        'HostedZoneId': str(hosted_zone_id),
                                        'DNSName': str(ans),
                                        'EvaluateTargetHealth': False
                                    },
                                },
                            }]
                        }
                    )

                elif str(type) == "CNAME":

                    ans = raw_input("Enter new record set value: [ENTER](Cntrl-C to exit)")

                    result = client.change_resource_record_sets(
                        HostedZoneId=str(hosted_zone_id),
                        ChangeBatch={
                            'Comment': 'Update record set',
                            'Changes': [{
                                'Action': 'UPSERT',
                                'ResourceRecordSet':{
                                    'Name': str(name),
                                    'Type': str(type),
                                    "TTL" : 300,
                                    'ResourceRecords': [
                                        {
                                            "Value": str(ans)
                                        }
                                    ]
                                }

                            }]
                        }
                    )


            else:
                print("\n")
                print("##############################################")
                print('There are no records sets for this hosted zone')
                print("##############################################")

        else:
            print("\n")
            print("##################")
            print("No Record Sets")
            print("##################")
    else:
        print("\n")
        print("######################")
        print("No Hosted Zones")
        print("######################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()
