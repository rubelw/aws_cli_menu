#!/usr/bin/env python

import sys
import subprocess
import boto3.session
from aws_cli_menu_helper import *
import re
import os
import stackility
import ConfigParser
from inspect import currentframe


ACCOUNNTS = {
    "dsi-nonprod" : "np",
    "dsi-prod":"pd",
    "dsi-sandbox":"sb",
    "dsi-utility":"ut",
    "phigalileo":"ga"
}

def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno


def remove_non_jinja2_cf_template(ini_data):
    """
    Removes the non .j2 template file so there is only a .j2 template

    Args:
        ini_data

    Returns:
        Success or error

    Todo:
        Figure out what could go wrong and take steps
        to hanlde problems.
    """
    template_file= ini_data['environment']['template']
    template_file = template_file[:-3]

    if 'project_dir' in ini_data:
        template_file = os.path.join(ini_data['project_dir'], template_file)

    os.remove(template_file)


def read_config_info(ini_file):

    print('def read_config_info: ' + str(ini_file))
    try:
        config = ConfigParser.ConfigParser()
        config.optionxform = str
        config.read(ini_file)
        the_stuff = {}
        for section in config.sections():
            the_stuff[section] = {}
            for option in config.options(section):
                the_stuff[section][option] = config.get(section, option)

        return the_stuff
    except Exception as wtf:
        print('Exception caught in read_config_info(): {}'.format(wtf))
        return sys.exit(1)



def validate_config_info():
    return True


try:
    STACKILITY_DIR = get_stackility_project_directory()
    print ('Stackility directory: '+str(STACKILITY_DIR))


    MENU = {}
    COUNTER = 0


    for child in STACKILITY_DIR:
        COUNTER += 1
        print('child: '+str(child))
        path = os.path.dirname(child)
        print('path: '+str(path))
        MENU[COUNTER] = [path,child]

    print "\n"
    print "####################################"
    print "Select Project Directory"
    print "####################################"

    for key in sorted(MENU):
        print str(key) + ": " + MENU[key][1]

    while True:

        USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
        if int(USER_RESPONSE) in MENU:
            PROJECT_DIR = MENU[int(USER_RESPONSE)][1]
        break

    DIRS = []

    if int(USER_RESPONSE) == 1:

        MENU = {}
        COUNTER = 0

        print('project dir: '+str(PROJECT_DIR))
        for child in os.listdir(PROJECT_DIR):
            COUNTER += 1
            print('child: ' + str(child))
            #path = os.path.dirname(child)
            path = os.path.join(PROJECT_DIR, child)
            print('path: ' + str(path))
            MENU[COUNTER] = [path, child]

        print "\n"
        print "####################################"
        print "Select Project Directory"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][0]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                PROJECT_DIR = MENU[int(USER_RESPONSE)][0]
            break

        if int(USER_RESPONSE) == 1:

            print('Trying to iterate: '+str(PROJECT_DIR))
            for child in os.listdir(PROJECT_DIR):
                path = os.path.join(PROJECT_DIR, child)
                if os.path.isdir(path):
                    DIRS.append(path)
        elif int(USER_RESPONSE) ==2:
            print('Trying to iterate: '+str(PROJECT_DIR))

            for child in os.listdir(PROJECT_DIR):
                path = os.path.join(PROJECT_DIR, child)
                if os.path.isdir(path):
                    DIRS.append(path)
        else:
            print('invalid answer')


    elif int(USER_RESPONSE) ==2:
        print('Trying to iterate: ' + str(STACKILITY_DIR[1]))

        for child in os.listdir(STACKILITY_DIR[1]):
            path = os.path.join(STACKILITY_DIR[1], child)
            if os.path.isdir(path):
                DIRS.append(path)

    else:
        print('invalid answer')
    #print DIRS

    MENU = {}
    UNSORTED_MENU = {}

    COUNTER = 0

    for d in DIRS:
        list = []


        list.append(d)

        matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

        if matchObj:
            list.append(matchObj.group(1))
            UNSORTED_MENU[matchObj.group(1)]= list


    for key in sorted(UNSORTED_MENU):
        COUNTER+=1

        MENU[COUNTER] = UNSORTED_MENU[key]

    print "\n"
    print "####################################"
    print "Select Project"
    print "####################################"

    for key in sorted(MENU):
        print str(key) + ": " + MENU[key][1]

    while True:

        USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
        if int(USER_RESPONSE) in MENU:
            PROJECT = MENU[int(USER_RESPONSE)][0]
            break


    PROJECT_DIRS = []
    PROJECT_DIRECTORY = os.path.join(STACKILITY_DIR,PROJECT)
    for child in os.listdir(PROJECT_DIRECTORY):
        path = os.path.join(PROJECT_DIRECTORY, child)
        if os.path.isdir(path):
            PROJECT_DIRS.append(path)

    MENU = {}
    UNSORTED_MENU = {}

    COUNTER = 0

    steps_flag = -1

    for d in PROJECT_DIRS:
        list = []

        list.append(d)

        matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

        if matchObj:
            list.append(matchObj.group(1))

            if 'step' in matchObj.group(1):
                print('flag turned on')
                steps_flag = 1

            UNSORTED_MENU[matchObj.group(1)] = list



    if steps_flag <0:
        for key in sorted(UNSORTED_MENU):
            COUNTER += 1

            MENU[COUNTER]= UNSORTED_MENU[key]
    else:
        COUNTER=40
        for key in sorted(UNSORTED_MENU):
            print(key)
            matchObj = re.match(r'.*step([0-9]+).*', key, re.M | re.I)

            if matchObj:
                print('match')
                print(matchObj.group(1))
                MENU[int(matchObj.group(1))] = UNSORTED_MENU[key]
            else:
                MENU[COUNTER] = UNSORTED_MENU[key]
                COUNTER += 1

    subdir = None
    for key in sorted(MENU):

        matchObj = re.match(r'.*/config$', MENU[key][0], re.M | re.I)

        if matchObj:

            subdir = MENU[key][0]

    print('subdir: '+str(subdir))

    if subdir:
        MENU.clear()
        COUNTER = 0

        STACK = os.path.dirname(subdir)

        print('stack: '+str(STACK))

        for child in os.listdir(subdir):
            COUNTER += 1
            print('child: '+str(child))
            path = os.path.join(subdir, child)
            print('path: '+str(path))
            MENU[COUNTER] = [path,child]

        print "\n"
        print "####################################"
        print "Select Ini File"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                INI_FILE = MENU[int(USER_RESPONSE)][0]
                break

    else:

        print "\n"
        print "####################################"
        print "Select Stack"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]


        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                STACK = MENU[int(USER_RESPONSE)][0]
                break



        if os.path.exists(STACK+'/config'):

            INI_FILES = []
            for file in os.listdir(STACK+'/config'):
                if file.endswith(".ini"):
                    ifile = os.path.join(STACK, 'config/'+file)
                    #print ifile
                    INI_FILES.append(os.path.join(ifile))

            MENU = {}

            COUNTER = 0

            for f in INI_FILES:
                #print(f)
                list = []
                COUNTER += 1
                list.append(f)

                matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_\.]+)$', f, re.M | re.I)

                if matchObj:
                    #print('found match')
                    list.append(matchObj.group(1))

                MENU[COUNTER] = list

            print "\n"
            print "####################################"
            print "Select Ini File"
            print "####################################"

            for key in sorted(MENU):
                print str(key) + ": " + MENU[key][1]

            while True:

                USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
                if int(USER_RESPONSE) in MENU:
                    INI_FILE = MENU[int(USER_RESPONSE)][0]
                    break

        # Has subprojects
        else:

            SUBPROJECT_DIRS = []

            for child in os.listdir(STACK):
                path = os.path.join(STACK, child)
                if os.path.isdir(path):
                    SUBPROJECT_DIRS.append(path)

            MENU = {}
            UNSORTED_MENU = {}

            COUNTER = 0

            for d in SUBPROJECT_DIRS:
                list = []

                list.append(d)

                matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

                if matchObj:
                    list.append(matchObj.group(1))

                    UNSORTED_MENU[matchObj.group(1)] = list

            for key in sorted(UNSORTED_MENU):
                COUNTER += 1

                MENU[COUNTER] = UNSORTED_MENU[key]

            print(MENU)


            print "\n"
            print "####################################"
            print "Select Stack"
            print "####################################"

            for key in sorted(MENU):
                print str(key) + ": " + MENU[key][1]

            while True:

                USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
                if int(USER_RESPONSE) in MENU:
                    STACK = MENU[int(USER_RESPONSE)][0]
                    break

            INI_FILES = []
            for file in os.listdir(STACK + '/config'):
                if file.endswith(".ini"):
                    ifile = os.path.join(STACK, 'config/' + file)
                    # print ifile
                    INI_FILES.append(os.path.join(ifile))

            MENU = {}

            COUNTER = 0

            for f in INI_FILES:
                # print(f)
                list = []
                COUNTER += 1
                list.append(f)

                matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_\.]+)$', f, re.M | re.I)

                if matchObj:
                    # print('found match')
                    list.append(matchObj.group(1))

                MENU[COUNTER] = list

            print "\n"
            print "####################################"
            print "Select Ini File"
            print "####################################"

            for key in sorted(MENU):
                print str(key) + ": " + MENU[key][1]

            while True:

                USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
                if int(USER_RESPONSE) in MENU:
                    INI_FILE = MENU[int(USER_RESPONSE)][0]
                    break




    #print 'ini file: ' + str(INI_FILE)

    #PROFILE_NAME = get_profile_name()
    #print "\n\n"

    #REGION = select_region()

    #cmd = "stackility upsert --ini "+str(ini_file)+" --region "+str(region)+" --profile "+str(profile_name)

    if determine_if_opsworks_stack(INI_FILE, STACK):

        print("\n\n")
        print("####################")
        print("This is an opsworks stack: "+str(get_linenumber()))
        print("#####################")
        print("\n\n")


        DIRS = []
        for child in os.listdir(STACK):
            path = os.path.join(STACK, child)
            if os.path.isdir(path):
                DIRS.append(path)

        # print DIRS

        MENU = {}
        UNSORTED_MENU = {}

        COUNTER = 0

        for d in DIRS:
            list = []

            list.append(d)

            matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

            if matchObj:
                list.append(matchObj.group(1))
                UNSORTED_MENU[matchObj.group(1)] = list

        for key in sorted(UNSORTED_MENU):
            COUNTER += 1

            MENU[COUNTER] = UNSORTED_MENU[key]

        print "\n"
        print "####################################"
        print "Select Recipe Directory"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                recipe_dir = MENU[int(USER_RESPONSE)][0]
                break

        recipe_param = get_package_url_from_ini(INI_FILE)

        print('recipe_param: '+str(recipe_param))
        ini_data = read_config_info(INI_FILE)
        print('ini_data: '+str(ini_data))
        ini_data['output_yaml'] = True

        if STACK:
            ini_data['project_dir'] = STACK

        if recipe_dir:
            ini_data['recipe_dir'] = recipe_dir
        if recipe_param:
            ini_data['recipe_url_parameter'] = recipe_param



        if yes_or_no('Is this a dryrun?'):
            ini_data['dryrun'] = True

            print('trying to get stackdriver'+str( get_linenumber()))
            stack_driver = stackility.CloudStackUtility(ini_data)

            print('trying to do upsert: '+str( get_linenumber()))
            if stack_driver.upsert():
                print('stack create/update was started successfully.')
                if stack_driver.poll_stack():
                    print('stack create/update was finished successfully.')
                    print(ini_data)
                    if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                            '.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(0)
                else:
                    print('stack create/update was did not go well.')
                    if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(1)
            else:
                print('start of stack create/update did not go well.')
                sys.exit(1)
        else:

            ini_data['dryrun'] = False
            print('trying to get stackdriver: '+str( get_linenumber()))
            print('ini_date is: '+str(ini_data))
            stack_driver = stackility.CloudStackUtility(ini_data)

            print('have a stack driver: '+str(get_linenumber()))

            if stack_driver.upsert():
                print('stack create/update was started successfully.')
                if stack_driver.poll_stack():
                    print('stack create/update was finished successfully.')
                    print(ini_data)
                    if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(0)
                else:
                    print('stack create/update was did not go well.')
                    if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(1)
            else:
                print('start of stack create/update did not go well.')
                sys.exit(1)
    elif determine_if_lambda_stack(INI_FILE, STACK):

        print("\n\n")
        print("####################")
        print("This is a lambda stack: "+str(get_linenumber()))
        print("#####################")
        print("\n\n")

        if not os.path.isdir(STACK+'/templates/lambda.j2'):

            print('it is a lambda stack')
            print('need to render template')

            if not os.path.isdir(STACK + '/templates'):

                template_directory  = select_path('Select template directory', STACK)
            else:
                template_directory = STACK+'/templates'


            if not os.path.isdir(STACK + '/files'):
                lambda_directory = select_path('Select lambda directory', STACK)
            else:
                lambda_directory= STACK+'/files'

            rendered_files = render_jinja_template(INI_FILE,template_directory,lambda_directory)

            print(rendered_files)

            lambda_bucket = get_lambda_bucket_from_ini(INI_FILE)

            lambda_key = get_lambda_bucket_key_from_ini(INI_FILE)

            print('lambda bucket: '+str(lambda_bucket))
            print('lambda key: '+str(lambda_key))
            print('lambda directory: '+str(lambda_directory))


            ini_data = read_config_info(INI_FILE)

            if 'environment' not in ini_data:
                print('[environment] section is required in the INI file')

            ini_data['output_yaml'] = True

            if STACK:
                ini_data['project_dir'] = STACK
            if lambda_directory:
                ini_data['lambda_dir'] = lambda_directory
            if lambda_bucket:
                ini_data['lambda_bucket_parameter'] = lambda_bucket
            if lambda_key:
                ini_data['lambda_key_parameter'] = lambda_key


            if yes_or_no('Is this a dryrun?'):

                ini_data['dryrun'] = True
                print('trying to get stackdriver' + str(get_linenumber()))
                stack_driver = stackility.CloudStackUtility(ini_data)

                if stack_driver.upsert():
                    print('stack create/update was started successfully.')
                    if stack_driver.poll_stack():
                        print('stack create/update was finished successfully.')
                        print(ini_data)
                        if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith('.j2'):
                            remove_non_jinja2_cf_template(ini_data)
                        sys.exit(0)
                    else:
                        print('stack create/update was did not go well.')
                        if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                            remove_non_jinja2_cf_template(ini_data)
                        sys.exit(1)
                else:
                    print('start of stack create/update did not go well.')
                    sys.exit(1)

            else:
                ini_data['dryrun'] = False
                print('trying to get stackdriver' + str(get_linenumber()))
                stack_driver = stackility.CloudStackUtility(ini_data)


                print('ini_data: '+str(ini_data))

                if stack_driver.upsert():
                    print('stack create/update was started successfully.')
                    if stack_driver.poll_stack():
                        print('stack create/update was finished successfully.')
                        print(ini_data)
                        if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith('.j2'):
                            remove_non_jinja2_cf_template(ini_data)
                        sys.exit(0)
                    else:
                        print('stack create/update was did not go well.')
                        if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                            remove_non_jinja2_cf_template(ini_data)
                        sys.exit(1)
                else:
                    print('start of stack create/update did not go well.')
                    sys.exit(1)


            # Remove rendered files
            #for f in rendered_files:
            #    os.remove(f)


        else:

            if yes_or_no('Is a template which needs to be rendered to the lambda package before uploading lambda zip file to s3?'):

                print('it is a lambda stack')
                print('need to render template')

                if not os.path.isdir(STACK + '/templates'):

                    template_directory = select_path('Select template directory', STACK)
                else:
                    template_directory = STACK + '/templates'

                if not os.path.isdir(STACK + '/files'):
                    lambda_directory = select_path('Select lambda directory', STACK)
                else:
                    lambda_directory = STACK + '/files'

                rendered_files = render_jinja_template(INI_FILE, template_directory, lambda_directory)

                print(rendered_files)

                lambda_bucket = get_lambda_bucket_from_ini(INI_FILE)
                lambda_key = get_lambda_bucket_key_from_ini(INI_FILE)

                print('lambda bucket: ' + str(lambda_bucket))
                print('lambda key: ' + str(lambda_key))
                print('lambda directory: ' + str(lambda_directory))

                ini_data = read_config_info(INI_FILE)

                if 'environment' not in ini_data:
                    print('[environment] section is required in the INI file')

                ini_data['output_yaml'] = True

                if STACK:
                    ini_data['project_dir'] = STACK
                if lambda_directory:
                    ini_data['lambda_dir'] = lambda_directory
                if lambda_bucket:
                    ini_data['lambda_bucket_parameter'] = lambda_bucket
                if lambda_key:
                    ini_data['lambda_key_parameter'] = lambda_key

                if yes_or_no('Is this a dryrun?'):

                    ini_data['dryrun'] = True

                    print('trying to get stackdriver')
                    stack_driver = stackility.CloudStackUtility(ini_data)

                    print('have stackdriver - tyring to do upsert')
                    if stack_driver.upsert():
                        print('stack create/update was started successfully.')
                        if stack_driver.poll_stack():
                            print('stack create/update was finished successfully.')
                            print(ini_data)
                            if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                                    '.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(0)
                        else:
                            print('stack create/update was did not go well.')
                            if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(1)
                    else:
                        print('start of stack create/update did not go well.')
                        sys.exit(1)

                else:
                    ini_data['dryrun'] = False

                    print('trying to get stack driver')
                    stack_driver = stackility.CloudStackUtility(ini_data)

                    print('have stack driver')
                    print('ini_data: ' + str(ini_data))
                    print('trying to do upsert')
                    if stack_driver.upsert():
                        print('stack create/update was started successfully.')
                        if stack_driver.poll_stack():
                            print('stack create/update was finished successfully.')
                            print(ini_data)
                            if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                                    '.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(0)
                        else:
                            print('stack create/update was did not go well.')
                            if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(1)
                    else:
                        print('start of stack create/update did not go well.')
                        sys.exit(1)

                # Remove rendered files
                # for f in rendered_files:
                #    os.remove(f)

            else:

                lambda_directory = select_path('Select lambda directory', STACK)

                lambda_bucket = get_lambda_bucket_from_ini(INI_FILE)
                lambda_key = get_lambda_bucket_key_from_ini(INI_FILE)

                print('lambda bucket: ' + str(lambda_bucket))
                print('lambda key: ' + str(lambda_key))

                ini_data = read_config_info(INI_FILE)

                if 'environment' not in ini_data:
                    print('[environment] section is required in the INI file')

                ini_data['output_yaml'] = True

                if STACK:
                    ini_data['project_dir'] = STACK
                if lambda_directory:
                    ini_data['lambda_dir'] = lambda_directory
                if lambda_bucket:
                    ini_data['lambda_bucket_parameter'] = lambda_bucket
                if lambda_key:
                    ini_data['lambda_key_parameter'] = lambda_key

                if yes_or_no('Is this a dryrun?'):

                    ini_data['dryrun'] = True
                    print('trying to get stackdriver' + str(get_linenumber()))
                    stack_driver = stackility.CloudStackUtility(ini_data)

                    if stack_driver.upsert():
                        print('stack create/update was started successfully.')
                        if stack_driver.poll_stack():
                            print('stack create/update was finished successfully.')
                            print(ini_data)
                            if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                                    '.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(0)
                        else:
                            print('stack create/update was did not go well.')
                            if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(1)
                    else:
                        print('start of stack create/update did not go well.')
                        sys.exit(1)
                else:

                    ini_data['dryrun'] = False
                    print('trying to get stackdriver' + str(get_linenumber()))
                    stack_driver = stackility.CloudStackUtility(ini_data)

                    if stack_driver.upsert():
                        print('stack create/update was started successfully.')
                        if stack_driver.poll_stack():
                            print('stack create/update was finished successfully.')
                            print(ini_data)
                            if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                                    '.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(0)
                        else:
                            print('stack create/update was did not go well.')
                            if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                                remove_non_jinja2_cf_template(ini_data)
                            sys.exit(1)
                    else:
                        print('start of stack create/update did not go well.')
                        sys.exit(1)

    else:


        print("\n\n")
        print("####################")
        print("This is a normal stack: "+str(get_linenumber()))
        print("#####################")
        print("\n\n")


        ini_data = read_config_info(INI_FILE)

        if 'environment' not in ini_data:
            print('[environment] section is required in the INI file')

        ini_data['output_yaml'] = True

        if STACK:
            ini_data['project_dir'] = STACK


        if yes_or_no('Is this a dryrun?'):

            ini_data['dryrun'] = True
            print('trying to get stackdriver'+str( get_linenumber()))
            stack_driver = stackility.CloudStackUtility(ini_data)

            if stack_driver.upsert():
                print('stack create/update was started successfully.')
                if stack_driver.poll_stack():
                    print('stack create/update was finished successfully.')
                    print(ini_data)
                    if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                            '.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(0)
                else:
                    print('stack create/update was did not go well.')
                    if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(1)
            else:
                print('start of stack create/update did not go well.')
                sys.exit(1)

        else:
            ini_data['dryrun'] = False
            print('trying to get stackdriver'+str( get_linenumber()))
            stack_driver = stackility.CloudStackUtility(ini_data)
            print('have stack driver: '+str(get_linenumber()))
            if stack_driver.upsert():
                print('stack create/update was started successfully.')
                if stack_driver.poll_stack():
                    print('stack create/update was finished successfully.')
                    print(ini_data)
                    if 'template' in ini_data['environment'] and ini_data['environment']['template'].endswith(
                            '.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(0)
                else:
                    print('stack create/update was did not go well.')
                    if 'template' in ini_data and ini_data['template'].endswith('.j2'):
                        remove_non_jinja2_cf_template(ini_data)
                    sys.exit(1)
            else:
                print('start of stack create/update did not go well.')
                sys.exit(1)



except (KeyboardInterrupt, SystemExit):
    sys.exit()
