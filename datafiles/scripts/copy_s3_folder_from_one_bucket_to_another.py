#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import subprocess
import os
import collections


def get_folder_list(session,bucket):

    client = session.client('s3')

    response = client.list_objects(
        Bucket=from_bucket_name,
    )

    objects = response.get('Contents')

    if len(objects) > 0:

        folders= collections.OrderedDict()



        for s in objects:

            segments = s['Key'].split('/')

            if len(segments)>0:
                file_name = segments[-1]
                segments.pop()
                path = '/'.join(segments)

                print('path: '+str(path))
                print('file name:'+str(file_name))

                if str(path) in folders:
                    print('found path in folders')
                    folders[str(path)].append(file_name)
                else:
                    print('did not find path in folders')
                    folders[str(path)]=[]
                    print(folders[str(path)])
                    folders[str(path)].append(file_name)
                    print(folders[str(path)])

        counter = 0
        menu = {}
        for k,v in folders.items():
            counter += 1
            my_list = []
            my_list.append(k)
            my_list.append(v)
            menu[counter] = my_list


        if len(menu)>0:
            print "\n\n"
            print '#####################################'
            print '## Select folder to copy   ##'
            print '#####################################'
            for key in menu:
                print str(key)+": "+str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        files = []
                        files.append(menu[int(ans)][0])
                        files.append(menu[int(ans)][1])
                        break

            return files


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    map = {}

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy From   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break


        from_bucket_name = info['Name']


        files = get_folder_list(session,from_bucket_name)
        path = files[0]
        files = files[1]

    profile_name = get_profile_name()
    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')


    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:

            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy To   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        to_bucket_name = info['Name']


        print('to bucket: '+str(to_bucket_name))

        for f in files:
            response = client.copy_object(
                Bucket=to_bucket_name,
                CopySource= {
                    'Bucket': from_bucket_name,
                    'Key': str(path)+'/'+str(f)
                },
                Key=str(path)+'/'+str(f)
            )

            print(pretty(response))

    else:
        print("\n")
        print("#######################")
        print('No S3 buckets found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
