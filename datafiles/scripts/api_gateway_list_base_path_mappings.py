#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('apigateway')



    response = client.get_domain_names()
    print(pretty(response))
    stacks = response.get('items')

    if len(stacks) > 0:
        menu = {}
        counter = 0
        for s in stacks:

            my_list = []

            my_list.append(s['domainName'])
            my_list.append(s)
            counter += 1
            menu[counter] = my_list

        print "\n\n"
        print '#########################################'
        print '## Select Domain                         ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    domain_name = menu[int(ans)][0]
                    break


        response = client.get_base_path_mappings(
            domainName=str(domain_name)
            #position='string',
            #limit=123
        )
        print(pretty(response))



except (KeyboardInterrupt, SystemExit):
    sys.exit()
