#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import time
from collections import OrderedDict
from datetime import timedelta, date
import datetime


groups= {}
groups['AWS::EC2::Instance']=1
groups['AWS::OpsWorks::Instance']=1
groups['AWS::EC2::SecurityGroup']=1
groups['AWS::Lambda::Function']=1
groups['AWS::SNS::Topic']=1
groups['AWS::RDS::DBInstance']=1
groups['AWS::IAM::Role']=1
groups['AWS::Logs::LogGroup']=1


groups['AWS::OpsWorks::ElasticLoadBalancerAttachment']=1
groups['AWS::EC2::NatGateway']=1
groups['AWS::CodeBuild::Project']=1
groups['AWS::OpsWorks::Layer']=1
groups['AWS::Route53::HostedZone']=1
groups['AWS::CloudFront::CloudFrontOriginAccessIdentity']=1
groups['AWS::IAM::AccessKey']=1
groups['AWS::CodePipeline::Pipeline']=1
groups['AWS::IAM::ManagedPolicy']=1
groups['AWS::Route53::RecordSetGroup']=1

groups['AWS::EC2::VPNGatewayRoutePropagation']=1
groups['AWS::Route53::RecordSet']=1
groups['AWS::EC2::VPCEndpoint']=1
groups['AWS::EC2::VPCDHCPOptionsAssociation']=1
groups['AWS::Events::Rule']=1
groups['AWS::EC2::RouteTable']=1
groups['AWS::Elasticsearch::Domain']=1
groups['AWS::IAM::Group']=1
groups['AWS::IAM::User']=1
groups['AWS::EC2::InternetGateway']=1
groups['AWS::EC2::VPCGatewayAttachment']=1
groups['AWS::IAM::UserToGroupAddition']=1

groups['AWS::EC2::EIP']=1

groups['AWS::EC2::VPNGateway']=1
groups['AWS::EC2::Route']=1
groups['AWS::EC2::NetworkAclEntry']=1
groups['AWS::EC2::VPNConnection']=1
groups['AWS::EC2::VPC']=1

groups['AWS::ElasticLoadBalancing::LoadBalancer']=1
groups['AWS::EC2::SubnetNetworkAclAssociation']=1
groups['AWS::EC2::SubnetRouteTableAssociation']=1
groups['AWS::Lambda::Permission']=1
groups['AWS::OpsWorks::Stack']=1
groups['AWS::SSM::Parameter']=1
groups['AWS::EC2::NetworkAcl']=1
groups['AWS::CodeCommit::Repository']=1
groups['AWS::SNS::Subscription']=1
groups['AWS::EC2::Subnet']=1
groups['AWS::EC2::DHCPOptions']=1
groups['AWS::IAM::InstanceProfile']=1
groups['AWS::RDS::DBSubnetGroup']=1
groups['AWS::CloudFront::Distribution']=1
groups['AWS::ApiGateway::Authorizer']=1
groups['AWS::ApiGateway::RestApi']=1
groups['AWS::ECS::Service']=1
groups['AWS::SNS::TopicPolicy']=1
groups['AWS::ApplicationAutoScaling::ScalingPolicy']=1
groups['AWS::ElasticLoadBalancingV2::Listener']=1
groups['AWS::Batch::ComputeEnvironment']=1
groups['AWS::ECS::TaskDefinition']=1
groups['AWS::CloudWatch::Alarm']=1
groups['AWS::AutoScaling::AutoScalingGroup']=1
groups['AWS::ApiGateway::Deployment']=1
groups['AWS::S3::BucketPolicy']=1
groups['AWS::ApiGateway::UsagePlanKey']=1
groups['AWS::ElastiCache::SubnetGroup']=1
groups['AWS::ApiGateway::DomainName']=1
groups['AWS::Batch::JobDefinition']=1
groups['AWS::ApiGateway::Resource']=1
groups['AWS::EC2::SecurityGroupIngress']=1
groups['AWS::ElastiCache::CacheCluster']=1
groups['AWS::ElasticLoadBalancingV2::ListenerRule']=1
groups['Custom::AddElbListener']=1
groups['AWS::EC2::SecurityGroupEgress']=1
groups['AWS::S3::Bucket']=1
groups['AWS::ApiGateway::UsagePlan']=1
groups['AWS::ECR::Repository']=1
groups['AWS::SQS::Queue']=1
groups['AWS::StepFunctions::StateMachine']=1
groups['AWS::ApiGateway::BasePathMapping']=1
groups['AWS::ECS::Cluster']=1
groups['AWS::ApiGateway::ApiKey']=1
groups['AWS::ElasticLoadBalancingV2::TargetGroup']=1
groups['AWS::AutoScaling::LaunchConfiguration']=1
groups['AWS::ApplicationAutoScaling::ScalableTarget']=1
groups['AWS::Batch::JobQueue']=1
groups['AWS::ApiGateway::Method']=1
groups['AWS::ElasticLoadBalancingV2::LoadBalancer']=1
groups['AWS::CloudTrail::Trail']=1
groups['AWS::KMS::Alias']=1
groups['AWS::RDS::DBParameterGroup']=1
groups['AWS::Logs::SubscriptionFilter']=1
groups['AWS::KMS::Key']=1
groups['AWS::Logs::LogStream']=1
groups['AWS::Glue::Job']=1
groups['AWS::EC2::FlowLog']=1
groups['AWS::IAM::Policy']=1
groups['AWS::Config::ConfigRule']=1
groups['AWS::SQS::QueuePolicy']=1
groups['AWS::CloudFormation::WaitConditionHandle']=1
groups['AWS::EC2::SpotFleet']=1
groups['AWS::CloudFormation::WaitCondition']=1
groups['AWS::AutoScaling::ScalingPolicy']=1


no_groups={}
non_cloud_resources = {}
cloud_resources = {}
cloud_ec2_instances = {}
resource_groups={}


def print_resources_with_cloudformation():

    print("\n\n")
    print('##########################################')
    print('# Resources built with cloudformation #')
    print('##########################################')
    print("\n")
    resource_list = {}
    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c])
            print(c)
            resource_list[str(c)] = cloud_resources[c]['info'][c][0]

    resource_list_sorted_by_value = OrderedDict(sorted(resource_list.items(), key=lambda x: x[1]))
    #print(pretty(resource_list_sorted_by_value))

    groupings = {}


    for item in resource_list_sorted_by_value:
        # print(pretty(item))
        if resource_list_sorted_by_value[item] in groupings:
            groupings[resource_list_sorted_by_value[item]].append(cloud_resources[item]['info'])
        else:
            groupings[resource_list_sorted_by_value[item]]=[]
            groupings[resource_list_sorted_by_value[item]].append(cloud_resources[item]['info'])

    for item in groupings:
        print("\n"+str(item))

        for group in groupings[item]:
            print("\t"+str(group))

        #print(cloud_resources[item]['info'])


def print_resources_without_cloudformation():
    print("\n\n")
    print('##########################################')
    print('# Resources built without cloudformation #')
    print('##########################################')
    print("\n")
    resource_list = {}
    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == False:
            #print(cloud_resources[c]['info'])
            resource_list[str(c)] = cloud_resources[c]['info'][c][0]

    resource_list_sorted_by_value = OrderedDict(sorted(resource_list.items(), key=lambda x: x[1]))
    #print(pretty(resource_list_sorted_by_value))

    groupings = {}

    for item in resource_list_sorted_by_value:
        # print(pretty(item))
        if resource_list_sorted_by_value[item] in groupings:
            groupings[resource_list_sorted_by_value[item]].append(cloud_resources[item]['info'])
        else:
            groupings[resource_list_sorted_by_value[item]] = []
            groupings[resource_list_sorted_by_value[item]].append(cloud_resources[item]['info'])

    for item in groupings:
        print("\n"+str(item))

        for group in groupings[item]:
            print("\t" + str(group))


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('cloudformation')

    resource = session.resource('cloudformation')


    stack_iterator = resource.stacks.all()

    my_stacks = []
    for i in stack_iterator:
        #print(i.name)
        my_stacks.append(i.name)


    if len(my_stacks) > 0:
        dict = {}

        for s in my_stacks:
            list = []
            list.append(s)

            #print(pretty(s))
            #print('stack name: '+str(s))
            #raw_input("Press Enter to continue...")

            #list.append(s.get('StackStatus'))
            dict[s] = list

        print("\n\n")
        print('##################################')
        print('## Undeleted Stacks             ##')
        print('##################################')

        stack_counter =0

        for key, value in sorted(dict.iteritems()):
            #print("%s\t\t%s" % (value[0], value[1]))

            stack_counter=+1

            response = client.describe_stack_resources(
                StackName=str(value[0])
            )
            time.sleep(1)

            if stack_counter>=25:
                time.sleep(5)
                stack_counter=0
            #print(response)

            resources = response['StackResources']

            if len(resources)>0:
                for r in resources:

                    if r['ResourceStatus']== 'DELETE_COMPLETE':
                        continue

                    my_dict={}
                    my_list= []
                    print(r['ResourceType'])
                    if 'PhysicalResourceId' in r:
                        print(r['PhysicalResourceId'])
                        my_list.append(str(r['PhysicalResourceId']))
                        cloud_resources[str(r['PhysicalResourceId'])]={}
                        cloud_resources[str(r['PhysicalResourceId'])]['info']=my_dict
                        cloud_resources[str(r['PhysicalResourceId'])]['cloud']=True

                    print(r['LogicalResourceId'])

                    if str(r['ResourceType']) not in groups:
                        no_groups[str(r['ResourceType'])]=1

                    my_list.append(str(r['ResourceType']))
                    resource_groups[str(r['ResourceType'])]=1
                    my_list.append(str(r['LogicalResourceId']))

                    if 'PhysicalResourceId' in r:
                        my_dict[str(r['PhysicalResourceId'])]=my_list


                    if str(r['ResourceType'])=='AWS::Lambda::Function':
                        if 'PhysicalResourceId' in r:
                            print(r['PhysicalResourceId'])

                        print(r['LogicalResourceId'])
                        #raw_input("Press Enter to continue...")

                    if str(r['ResourceType'])== 'AWS::EC2::Instance':

                        if 'PhysicalResourceId' in r:
                            cloud_ec2_instances[str(r['PhysicalResourceId'])]=str(r['LogicalResourceId'])


        print(pretty(resource_groups))
    else:
        print("\n")
        print("#############################")
        print('No undeleted stacks found')
        print("#############################")

    print(pretty(no_groups))


    ############################
    # Get Ec2 instances
    ############################

    client = session.client('ec2')
    response = client.describe_instances()

    if (DEBUG):
        print(response)

    stacks = response.get('Reservations')

    menu = {}
    fields = {}

    for s in stacks:
        #print(pretty(s))
        #raw_input("Press Enter to continue...")

        list = []
        id = str(s.get('Instances')[0]['InstanceId'])

        opsworks_id = ''
        if 'ClientToken' in s['Instances'][0]:
            opsworks_id= s['Instances'][0]['ClientToken']

        name = ''

        if 'Tags' in s['Instances'][0]:

            for tag in s['Instances'][0]['Tags']:
                #print(pretty(tag))
                if 'Key' in tag and tag['Key']== 'Name':
                    name = tag['Value']



        else:
            name = 'no tag - ' + str(id)

        if len(name)<1:
            name = 'no tag - '+str(id)

        list.append(name)
        list.append(id)

        if 'State' in s['Instances'][0]:
            state = s['Instances'][0]['State']['Name']
        else:
            state = 'none'
        list.append(state)

        fields[name] = list

        # if a spot instance
        # We are adding spot instance request here so that when we iterate
        # through spot fleet requests, we can identify spot instances
        if 'SpotInstanceRequestId' in s['Instances'][0]:
            my_dict = {}
            my_list = []
            my_list.append('AWS::EC2::SpotInstance')
            my_list.append(str(s['Instances'][0]['SpotInstanceRequestId']))
            my_list.append(name)
            my_dict[str(s['Instances'][0]['SpotInstanceRequestId'])] = my_list
            cloud_resources[str(s['Instances'][0]['SpotInstanceRequestId'])] = {}
            cloud_resources[str(s['Instances'][0]['SpotInstanceRequestId'])]['info'] = my_dict
            cloud_resources[str(s['Instances'][0]['SpotInstanceRequestId'])]['cloud'] = False

        else:

            # Fix opsworks
            if len(opsworks_id)>0:
                if str(opsworks_id) in cloud_resources:
                    print('instance id: ' + str(opsworks_id) + ' created with cloud formation')
                    cloud_resources[str(opsworks_id)]['info'][cloud_resources[str(opsworks_id)]['info'].keys()[0]].append(name)
                    # print(pretty(cloud_resources[str(id)]['info'].keys()[0]))
                    # raw_input("Press Enter to continue...")

                    cloud_resources[str(opsworks_id)]['cloud'] = True
                else:
                    my_dict = {}
                    my_list = []
                    my_list.append('AWS::EC2::Instance')
                    my_list.append(str(opsworks_id))
                    my_list.append(name)
                    my_dict[str(opsworks_id)] = my_list
                    cloud_resources[str(opsworks_id)] = {}
                    cloud_resources[str(opsworks_id)]['info'] = my_dict
                    cloud_resources[str(opsworks_id)]['cloud'] = False
            else:
                if str(id) in cloud_resources:
                    print('instance id: ' + str(id) + ' created with cloud formation')
                    cloud_resources[str(id)]['info'][cloud_resources[str(id)]['info'].keys()[0]].append(name)
                    # print(pretty(cloud_resources[str(id)]['info'].keys()[0]))
                    # raw_input("Press Enter to continue...")

                    cloud_resources[str(id)]['cloud'] = True
                else:
                    my_dict = {}
                    my_list = []
                    my_list.append('AWS::EC2::Instance')
                    my_list.append(str(id))
                    my_list.append(name)
                    my_dict[str(id)] = my_list
                    cloud_resources[str(id)] = {}
                    cloud_resources[str(id)]['info'] = my_dict
                    cloud_resources[str(id)]['cloud'] = False

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])


    ##########################
    # Get opsworks instances
    ###########################
    client = session.client('opsworks')
    response = client.describe_stacks()
    stacks = response.get('Stacks')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            response = client.describe_instances(
                StackId=str(s['StackId'])
            )

            print(pretty(response))
            #raw_input("Press Enter to continue...")

            ow_instance_id = response['Instances'][0]['InstanceId']

            #print('opsworks instance id: '+str(ow_instance_id))
            #raw_input("Press Enter to continue...")

            if str(ow_instance_id) in cloud_resources:
                print('instance id: '+str(ow_instance_id)+' created with cloud formation')
                cloud_resources[str(ow_instance_id)]['info'][cloud_resources[str(ow_instance_id)]['info'].keys()[0]].append(response['Instances'][0]['Hostname'])

                cloud_resources[str(ow_instance_id)]['cloud']=True
            else:
                my_dict={}
                my_list= []
                my_list.append('AWS::OpsWorks::Instance')
                my_list.append(str(ow_instance_id))
                my_list.append(response['Instances'][0]['Hostname'])
                my_dict[str(ow_instance_id)]=my_list
                cloud_resources[str(ow_instance_id)]={}
                cloud_resources[str(ow_instance_id)]['info']=my_dict
                cloud_resources[str(ow_instance_id)]['cloud']=False

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])

    ##########################
    # Get Spot Requests
    #########################
    client = session.client('ec2')

    spot_fleet_requests = []

    response = client.describe_spot_fleet_requests(
        DryRun=False
    )


    if 'SpotFleetRequestConfigs' in response:
        stacks = response.get('SpotFleetRequestConfigs')

        if len(stacks) > 0:

            for s in stacks:

                #print(pretty(s))
                #raw_input("Press Enter to continue...")

                if s['SpotFleetRequestState'] <> 'Cancelled':
                    spot_fleet_requests.append(str(s['SpotFleetRequestId']))
                    if str(str(s['SpotFleetRequestId'])) in cloud_resources:
                        print('spot fleet request id: ' + str(s['SpotFleetRequestId']) + ' created with cloud formation')
                        cloud_resources[str(s['SpotFleetRequestId'])]['cloud'] = True
                    else:
                        my_dict = {}
                        my_list = []
                        my_list.append('AWS::EC2::SpotFleet')
                        my_list.append(str(s['SpotFleetRequestId']))
                        my_dict[str(s['SpotFleetRequestId'])] = my_list
                        cloud_resources[str(s['SpotFleetRequestId'])] = {}
                        cloud_resources[str(s['SpotFleetRequestId'])]['info'] = my_dict
                        cloud_resources[str(s['SpotFleetRequestId'])]['cloud'] = False

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])



    ##########################
    #  Get Spot Instances
    ###########################
    # u'SpotInstanceRequestId': 'sir-vb9r54ej',
    # u'SpotInstanceRequestId': 'sir-9xk85igg', u'State': 'disabl


    client = session.client('ec2')

    if len(spot_fleet_requests)>0:

        for spot_request in spot_fleet_requests:

            response = client.describe_spot_fleet_instances(
                SpotFleetRequestId=str(spot_request)
            )


            if 'ActiveInstances' in response:
                stacks = response.get('ActiveInstances')

                if len(stacks) > 0:

                    for s in stacks:
                        #print(pretty(s))
                        #raw_input("Press Enter to continue...")

                        if str(str(s['SpotInstanceRequestId'])) in cloud_resources:
                            print('spot instance id: '+str(s['SpotInstanceRequestId'])+' created with cloud formation')
                            cloud_resources[str(s['SpotInstanceRequestId'])]['cloud']=True


                        else:
                            my_dict={}
                            my_list= []
                            my_list.append('AWS::EC2::SpotInstance')
                            my_list.append(str(s['SpotInstanceRequestId']))
                            my_dict[str(s['InstanceId'])]=my_list
                            cloud_resources[str(s['SpotInstanceRequestId'])]={}
                            cloud_resources[str(s['SpotInstanceRequestId'])]['info']=my_dict
                            cloud_resources[str(s['SpotInstanceRequestId'])]['cloud']=False
                            print('spot instance id: '+str(s['SpotInstanceRequestId'])+' not created with cloud formation')


    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])



    ##########################
    # Get security groups
    ##########################
    client = session.client('ec2')
    response = client.describe_security_groups()
    stacks = response.get('SecurityGroups')

    if len(stacks) > 0:
        for s in stacks:

            #print(pretty(s))
            #raw_input("Press Enter to continue...")

            if str(str(s['GroupId'])) in cloud_resources:
                print('security group id: '+str(s['GroupId'])+' created with cloud formation')

                try:
                    cloud_resources[str(id)]['info'][cloud_resources[str(id)]['info'].keys()[0]].append(s['GroupName'])
                except e:
                    print(e)
                    print(pretty(cloud_resources[str(id)]))
                    sys.exit(1)

                cloud_resources[str(s['GroupId'])]['cloud']=True
            else:
                my_dict={}
                my_list= []
                my_list.append('AWS::EC2::SecurityGroup')
                my_list.append(str(s['GroupId']))
                my_list.append(str(s['GroupName']))
                my_dict[str(s['GroupId'])]=my_list
                cloud_resources[str(s['GroupId'])]={}
                cloud_resources[str(s['GroupId'])]['info']=my_dict
                cloud_resources[str(s['GroupId'])]['cloud']=False

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])

    ##########################
    # Get lambda functions
    ##########################
    client = session.client('lambda')


    response = client.list_functions()
    stacks = response.get('Functions')

    if len(stacks) > 0:
        for s in stacks:

            #print(pretty(s))
            #raw_input("Press Enter to continue...")
            #ut - io - sl - daily - billing - report - Lambda - 1
            #VUB1HRWC6418


            if str(str(s['FunctionName'])) in cloud_resources:
                print('lambda id: '+str(s['FunctionName'])+' created with cloud formation')
                cloud_resources[str(id)]['info'][cloud_resources[str(id)]['info'].keys()[0]].append(s['FunctionName'])
                cloud_resources[str(s['FunctionName'])]['cloud']=True

                #raw_input("Press Enter to continue...")

            else:
                my_dict={}
                my_list= []
                my_list.append('AWS::Lambda::Function')
                my_list.append(str(s['FunctionName']))
                my_dict[str(s['FunctionName'])]=my_list
                cloud_resources[str(s['FunctionName'])]={}
                cloud_resources[str(s['FunctionName'])]['info']=my_dict
                cloud_resources[str(s['FunctionName'])]['cloud']=False

                #print('function not in cloud resources')
                #raw_input("Press Enter to continue...")

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])

    ##########################
    # Get sns topics
    ###########################

    client = session.client('sns')

    response = client.list_topics()

    if 'Topics' in response:
        stacks = response.get('Topics')

        if len(stacks) > 0:

            for s in stacks:

                #print('#### SNS Topics ####')
                #print(pretty(s))
                #raw_input("Press Enter to continue...")

                if str(str(s['TopicArn'])) in cloud_resources:
                    print('sns id: '+str(s['TopicArn'])+' created with cloud formation')
                    cloud_resources[str(s['TopicArn'])]['cloud']=True
                else:
                    my_dict={}
                    my_list= []
                    my_list.append('AWS::SNS::Topic')
                    my_list.append(str(s['TopicArn']))
                    my_dict[str(s['TopicArn'])]=my_list
                    cloud_resources[str(s['TopicArn'])]={}
                    cloud_resources[str(s['TopicArn'])]['info']=my_dict
                    cloud_resources[str(s['TopicArn'])]['cloud']=False

    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == True:
            print(cloud_resources[c]['info'])

    ###########################
    # Get RDS Instances
    ###########################
    client = session.client('rds')

    response = client.describe_db_instances()

    if 'DBInstances' in response:
        stacks = response.get('DBInstances')

        if len(stacks) > 0:
            menu = {}
            counter=0
            for s in stacks:
                #print(pretty(s))
                #raw_input("Press Enter to continue...")

                #my_list.append(s['DBName'])
                #my_list.append(s['DBInstanceIdentifier'])

                if str(str(s['DbiResourceId'])) in cloud_resources:
                    print('rds id: '+str(s['DbiResourceId'])+' created with cloud formation')
                    cloud_resources[str(id)]['info'][cloud_resources[str(id)]['info'].keys()[0]].append(s['DBInstanceIdentifier'])
                    cloud_resources[str(s['DbiResourceId'])]['cloud']=True
                else:
                    my_dict={}
                    my_list= []
                    my_list.append('AWS::RDS::DBInstance')
                    my_list.append(str(s['DbiResourceId']))
                    my_list.append(str(s['DBInstanceIdentifier']))
                    my_dict[str(s['DbiResourceId'])]=my_list
                    cloud_resources[str(s['DbiResourceId'])]={}
                    cloud_resources[str(s['DbiResourceId'])]['info']=my_dict
                    cloud_resources[str(s['DbiResourceId'])]['cloud']=False



    for c in cloud_resources:
        if cloud_resources[c]['cloud'] == False:
            print(cloud_resources[c]['info'])

    #######################
    # Get IAM Roles
    #######################

    client = session.client('iam')
    response = client.list_roles()
    stacks = response.get('Roles')



    for s in stacks:

        #print(pretty(s))
        #raw_input("Press Enter to continue...")

        list = []
        name = str(s.get('RoleName'))
        id = str(s.get('RoleId'))

        if str(str(s['RoleId'])) in cloud_resources:
            print('rds id: '+str(s['RoleId'])+' created with cloud formation')
            cloud_resources[str(id)]['info'][cloud_resources[str(id)]['info'].keys()[0]].append(
                s['RoleName'])

            cloud_resources[str(s['RoleId'])]['cloud']=True
        else:
            my_dict={}
            my_list= []
            my_list.append('AWS::IAM::Role')
            my_list.append(str(s['RoleId']))
            my_list.append(str(s['RoleName']))
            my_dict[str(s['RoleId'])]=my_list
            cloud_resources[str(s['RoleId'])]={}
            cloud_resources[str(s['RoleId'])]['info']=my_dict
            cloud_resources[str(s['RoleId'])]['cloud']=False


    ####################
    # Get Log Groups
    #####################
    client = session.client('logs')

    log_groups = []

    marker = None
    while True:
        if marker:
            response_iterator = client.describe_log_groups(
                nextToken=marker,
                limit=50
            )
        else:
            response_iterator = client.describe_log_groups(
                limit=10
            )

        for log_group in response_iterator['logGroups']:
            #print(pretty(log_group))
            #raw_input("Press Enter to continue...")

            print(log_group['logGroupName'])
            log_groups.append(log_group['logGroupName'])

        try:
            marker = response_iterator['nextToken']
            print(marker)
        except KeyError:
            break

    #print(pretty(log_groups))


    for lg in log_groups:
        print('lg: '+str(lg))

        if str(lg) in cloud_resources:
            print('loggroup name: '+str(lg)+' created with cloud formation')

            cloud_resources[str(lg)]['cloud']=True
        else:
            my_dict={}
            my_list= []
            my_list.append('AWS::Logs::LogGroup')
            my_list.append(str(lg))
            my_list.append(str(lg))
            my_dict[str(lg)]=my_list
            cloud_resources[str(lg)]={}
            cloud_resources[str(lg)]['info']=my_dict
            cloud_resources[str(lg)]['cloud']=False




    print_resources_with_cloudformation()
    print_resources_without_cloudformation()


except (KeyboardInterrupt, SystemExit):
    sys.exit()
