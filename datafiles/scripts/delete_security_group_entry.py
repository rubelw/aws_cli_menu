#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('ec2')
    response = client.describe_security_groups()
    stacks = response.get('SecurityGroups')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            my_list = []
            my_list.append(s['GroupName'])
            my_list.append(s['Description'])
            my_list.append(s)
            menu[counter] = my_list

        print "\n"
        print '#########################################'
        print '## Select Security Group               ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0] + '-' + str(menu[key][1])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    answer = menu[int(ans)][2]
                    break

        print("\n")
        print("######################################")
        print("Detailed Security Group Information")
        print("######################################")

        print(pretty(answer))

        if 'IpPermissions' in answer:

            menu = {}
            counter = 0

            for perm in answer['IpPermissions']:

                groupid = answer['GroupId']
                groupname = answer['GroupName']

                print('perm: '+str(perm))


                counter += 1
                my_list = []

                if 'IpRanges' in perm:
                    for i in perm['IpRanges']:
                        print('i : '+str(i))
                        if 'Description' in i:
                            my_list.append(i['Description'])
                        else:
                            my_list.append('None')
                        my_list.append(i['CidrIp'])
                        my_list.append(groupid)
                        my_list.append(groupname)
                        my_list.append(perm)
                        menu[counter] = my_list


            print "\n"
            print '#########################################'
            print '## Select Ingress Group to Delete      ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][0] + '-' + str(menu[key][1])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        cidr = menu[int(ans)][1]
                        group = menu[int(ans)][4]
                        break

            for index, item in enumerate(group['IpRanges']):
                if item['CidrIp'] == str(cidr):
                    del group['IpRanges'][index]

            print(group)


    else:
        print("\n")
        print("##############################")
        print('No security groups found')
        print("##############################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
