#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
from aws_requests_auth.aws_auth import AWSRequestsAuth
from elasticsearch import Elasticsearch, RequestsHttpConnection
import sys


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('es')
    response = client.list_domain_names()
    domains = response['DomainNames']

    print(pretty(response))


    if len(domains) >0:

        menu = {}
        counter = 0
        for s in domains:
            counter += 1
            menu[counter] = s['DomainName']

        print "\n\n"
        print '#########################################'
        print '## Select Domain Name                  ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    domain_name = menu[int(ans)]
                    break

        print('domain name: '+str(domain_name))
        response = client.delete_elasticsearch_domain(
            DomainName=domain_name
        )

        print(pretty(response))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
