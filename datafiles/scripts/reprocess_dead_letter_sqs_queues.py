#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import Queue
import threading


work_queue = Queue.Queue()

def process_queue():
    while True:
        messages = work_queue.get()

        bodies = list()
        for i in range(0, len(messages)):
            bodies.append({'Id': str(i+1), 'MessageBody': messages[i].body})

        to_q.send_messages(Entries=bodies)

        for message in messages:
            print("Coppied " + str(message.body))
            message.delete()

try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('sqs')

    response = client.list_queues()
    print(pretty(response))

    if 'QueueUrls' in response:
        stacks = response.get('QueueUrls')

        if len(stacks) > 0:

            menu = {}
            counter=0
            for s in stacks:
                print(pretty(s))
                counter+=1
                menu[counter]=s

            print("\n")
            print("##########################")
            print("Select 'From' SQS Queue")
            print("##########################")
            for key in sorted(menu):
                print str(key)+":" + str(menu[key])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        from_queue_name = menu[int(ans)]
                        break


            matchObj = re.match( r'.*/([^/]*)$', from_queue_name, re.M|re.I)

            if matchObj:
               from_q_name= matchObj.group(1)
            else:
               print "No match!!"


            print("\n")
            print("##########################")
            print("Select 'To' SQS Queue")
            print("##########################")
            for key in sorted(menu):
                print str(key)+":" + str(menu[key])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        to_queue_name = menu[int(ans)]
                        break


            matchObj = re.match( r'.*/([^/]*)$',to_queue_name, re.M|re.I)

            if matchObj:
               to_q_name= matchObj.group(1)
            else:
               print "No match!!"



            client = session.resource('sqs')
            from_q = client.get_queue_by_name(QueueName=from_q_name)
            to_q = client.get_queue_by_name(QueueName=to_q_name)

            print("From: " + from_q_name + " To: " + to_q_name)

            for i in range(10):
                 t = threading.Thread(target=process_queue)
                 t.daemon = True
                 t.start()

            while True:
                messages = list()
                for message in from_q.receive_messages(
                        MaxNumberOfMessages=10,
                        VisibilityTimeout=123,
                        WaitTimeSeconds=20):
                    messages.append(message)
                work_queue.put(messages)

            work_queue.join()



        else:
            print("\n")
            print("##################")
            print('No SQS Queues')
            print("##################")
    else:
        print("\n")
        print("##################")
        print('No SQS Queues')
        print("##################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()
