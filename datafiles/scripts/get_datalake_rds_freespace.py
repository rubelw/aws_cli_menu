#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
from datetime import datetime, time, timedelta
import pytz # $ pip install pytz
import re
from botocore.exceptions import ClientError
import math

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])


try:
    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('rds')

    response = client.describe_db_instances()

    if 'DBInstances' in response:
        stacks = response.get('DBInstances')

        if len(stacks) > 0:
            menu = {}
            counter=0

            db_names = []

            for s in stacks:

                #print(s)
                #print(s['DBInstanceIdentifier'])
                counter+=1
                my_list = []
                my_list.append(s['DBName'])
                my_list.append(s['DBInstanceIdentifier'])
                my_list.append(s)
                menu[counter]=my_list
                db_names.append(s['DBInstanceIdentifier'])



            tz = pytz.timezone("America/Chicago")
            yesterday = datetime.now(tz).date() - timedelta(days=1)
            today = datetime.now(tz).date()
            print('today: '+str(today))


            (yyear,ymonth,yday)=str(yesterday).split('-')
            (tyear,tmonth,tday)=str(today).split('-')

            client = session.client('cloudwatch')

            sum = 0
            for db in db_names:

                try:
                    response = client.get_metric_statistics(
                        Namespace='AWS/RDS',
                        MetricName='FreeStorageSpace',
                        Dimensions=[
                            {
                                'Name': 'DBInstanceIdentifier',
                                'Value': str(db)
                            }
                        ],
                        StartTime=datetime(int(yyear),int(ymonth),int(yday)),
                        EndTime=datetime(int(tyear),int(tmonth),int(tday)),
                        Period=3600,
                        Statistics=[
                            'Maximum'
                        ]
                    )

                    #print(response)
                    if response['Datapoints']:
                        if len(response['Datapoints'])>0:
                            print(str(db)+' - Size: '+str(convert_size(int(response['Datapoints'][0]['Maximum']))))
                            sum = sum + int(response['Datapoints'][0]['Maximum'])
                        else:
                            print(str(db)+' - Size: 0')


                except ClientError as e:
                    print("Error: %s" % e)

            print('Total Size:'+str(convert_size(sum)))

    else:
        print("\n")
        print("#######################")
        print('No RDS Instance Found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
