#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys



MAPPINGS = {
        '845565000680':'phiaws',
        '744827442853':'ami',
        '136702420730':'phisamlsandbox',
        '093924704261':'phiawsprod',
        '786215072930':'phiawstest',
        '740903730880':'phinsim',
        '058812869862':'phigalileo',
        '434834166239':'phicompass',
        '774160669924':'phicompasstest',
        '574544438066':'phicompassprod',
        '627980510655':'phidatatest',
        '552526261755':'phidataprod',
        '826397687123':'phimapshotsdev',
        '254454801483':'phimapshotsprod',
        '461788686395':'ftiosandbox'
}



try:

    profile_name = get_profile_name()
    print("\n")


    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('ec2')
    response = client.describe_vpcs()


    if 'Vpcs' in response:

        stacks = response.get('Vpcs')

        if len(stacks) > 0:


            menu = {}
            counter = 0
            for s in stacks:
                counter+=1
                my_list = []

                if 'Tags' in s:
                    name = 'Tag: '+str(s['Tags'][0]['Value'])
                else:
                    name = 'Tag: None - ' + str(s.get('VpcId'))
                vpcid = s.get('VpcId')
                cidr_block = s.get('CidrBlock')

                my_list.append(name)
                my_list.append(vpcid)
                my_list.append(cidr_block)
                my_list.append(s)
                menu[counter] = my_list


            if len(menu) > 0:

                print "\n"
                print '#########################################'
                print '## Select VPC                          ##'
                print '#########################################'
                for key in sorted(menu):
                    print str(key) + ":" + menu[key][0] + ' - ' + menu[key][1] + ' - ' + menu[key][2]

                pattern = r'^[0-9]+$'
                while True:

                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            vpc_id = menu[int(ans)][1]
                            info = menu[int(ans)][3]
                            break
                        elif int(ans) ==0:
                            sys.exit(1)

                print "\n"
                print '#########################################'
                print '## VPC Details                         ##'
                print '#########################################'
                print(pretty(info))


                peering_connections = client.describe_vpc_peering_connections(
                    DryRun=False,
                    Filters=[
                        {
                            'Name': 'accepter-vpc-info.vpc-id',
                            'Values':[vpc_id]
                        }
                    ]
                )
                print('###################################')
                print('Peering connections to '+vpc_id)
                list = peering_connections['VpcPeeringConnections']

                if len(list)>0:

                    for i in list:
                        print(str(MAPPINGS[i['AccepterVpcInfo']['OwnerId']])+' - '+str(i['RequesterVpcInfo']['OwnerId'])+' - '+str(i['RequesterVpcInfo']['VpcId'])+' - '+str(i['RequesterVpcInfo']['CidrBlock']))
                else:
                    print('No peering connections to '+vpc_id)

                peering_connections = client.describe_vpc_peering_connections(
                    DryRun=False,
                    Filters=[
                        {
                            'Name': 'requester-vpc-info.vpc-id',
                            'Values':[vpc_id]
                        }
                    ]
                )
                print('###################################')
                print('Peering connections from '+vpc_id)
                list = peering_connections['VpcPeeringConnections']

                if len(list)>0:

                    for i in list:
                        print(str(MAPPINGS[i['AccepterVpcInfo']['OwnerId']])+' - '+str(i['AccepterVpcInfo']['OwnerId'])+' - '+str(i['AccepterVpcInfo']['VpcId'])+' - '+str(i['AccepterVpcInfo']['CidrBlock']))
                else:
                    print('No peering connections from '+vpc_id)




            else:
                print("\n")
                print("##############################")
                print('No vpc information found')
                print("##############################")

        else:
            print("\n")
            print("##############################")
            print('No vpc information found')
            print("##############################")

    else:
        print("\n")
        print("##############################")
        print('No vpc information found')
        print("##############################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()




