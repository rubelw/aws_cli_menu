#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import subprocess
import re




try:


    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("select schema_name from information_schema.schemata")

        rows = cur.fetchall()

        cur.close()
        conn.close()

        schemas_menu = {}

        counter =0
        for row in rows:
            counter+=1
            #print(pretty(row))
            schemas_menu[counter] = row[0]

        #print(pretty(schemas_menu))

        pattern = r'^[0-9]+$'

        type_menu = {1:['Read Only Role'], 2:['Read-Write Role']}

        print("\n")
        print("#################################")
        print("Select Role Type")
        print("#################################")

        for key in sorted(type_menu):
            print str(key) + ":" + type_menu[key][0]

        ans = raw_input("Make a selection: [ENTER](Cntrl-C to exit)")

        while True:

            pattern = r'^[0-9]+$'

            if re.match(pattern, ans) is not None:
                if int(ans) in type_menu:
                    selection  = int(ans)
                    break



        if int(selection) == 1:
            # Read only role

            role_name = raw_input("Enter role name: [ENTER](Cntrl-C to exit)")

            pattern = r'.*_ro$'

            if re.match(pattern,role_name) is None:
                role_name = role_name+str('_ro')


            print("\n")
            print("#################################")
            print("Select Schema")
            print("#################################")

            for key in sorted(schemas_menu):
                print str(key) + ":" + schemas_menu[key]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in schemas_menu:
                        schema = schemas_menu[int(ans)]
                        break


            print('schema is: '+str(schema))
            print('new role name will be: '+str(role_name))

            conn=psycopg2.connect(conn_string)
            cur = conn.cursor()

            # Need to check for a user
            cur.execute("GRANT SELECT ON ALL TABLES IN SCHEMA "+str(schema)+' TO '+str(role_name))
            conn.commit()

            print("\n")
            print("##############################")
            print("New Role has been created")
            print("##############################")


        elif int(selection) ==2:
            # Read write role

            role_name = raw_input("Enter role name: [ENTER](Cntrl-C to exit)")

            pattern = r'.*_rw$'

            if re.match(pattern,role_name) is None:
                role_name = role_name+str('_rw')

            print("\n")
            print("#################################")
            print("Select Schema")
            print("#################################")

            for key in sorted(schemas_menu):
                print str(key) + ":" + schemas_menu[key]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in schemas_menu:
                        schema = schemas_menu[int(ans)]
                        break


            print('schema is: '+str(schema))
            print('new role name will be: '+str(role_name))

            conn=psycopg2.connect(conn_string)
            cur = conn.cursor()

            # Need to check for a user
            cur.execute("GRANT SELECT, INSERT, UPDATE, DELETE, TRUNACTE, REFERENCES, TRIGGER ON ALL TABLES IN SCHEMA "+str(schema)+' TO '+str(role_name))
            conn.commit()

            print("\n")
            print("##############################")
            print("New Role has been created")
            print("##############################")



        else:
            print('Invalid selection')
            sys.exit(-1)


    except:
        print('Could not connect to database')



except (KeyboardInterrupt, SystemExit):
    sys.exit()