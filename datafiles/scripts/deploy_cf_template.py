#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import collections
import json
import time

try:

    profile_name = get_profile_name()
    print("\n")

    stack_name = raw_input("Enter stack name: [ENTER](Cntrl-C to exit)")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            menu[counter] = s['Name']

        print "\n\n"
        print '#########################################'
        print '## Select Object                       ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    object_name = menu[int(ans)]
                    break

        response = client.list_objects(
            Bucket=object_name,
        )

        if 'Contents' not in response:
            print("\n")
            print("#####################################")
            print("There are no objects in the bucket.")
            print("#####################################")
        else:

            stacks = response['Contents']
            if len(stacks) > 0:

                menu = {}
                counter = 0

                for i in stacks:
                    print(pretty(i))
                    counter += 1
                    my_list = []


                    if 'ETag' in i:
                        my_list.append(i['ETag'])
                        my_list.append(i['StorageClass'])
                        my_list.append(i['Key'])

                        menu[counter] = my_list

                if len(menu) > 0:
                    print "\n\n"
                    print '#########################################'
                    print '## Select Object                       ##'
                    print '#########################################'

                    for key in sorted(menu):
                        print str(key) + ":" + menu[key][2]

                    pattern = r'^[0-9]+$'
                    while True:
                        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in menu:
                                etag = menu[int(ans)][0]
                                name = menu[int(ans)][2]

                                break

                    print('object_name: '+str(object_name))
                    print('name: '+str(name))
                    url = 'https://s3.amazonaws.com/'+str(object_name)+'/'+str(name)

                else:
                    print("\n")
                    print("##################################################")
                    print("No files in bucket.  However, there may be other")
                    print("sub-folders but this program does not yet have the")
                    print("functionality to iterate more than one layer deep.")
                    print("##################################################")
    else:
        print("\n")
        print("#########################")
        print('No S3 buckets found')
        print("#########################")



    client = session.client('cloudformation')

    #json_string = json.dumps(template_body)
    #print(json_string)

    response = client.validate_template(
        #TemplateBody=str(json_string)
        TemplateURL=str(url)
    )

    print(pretty(response))

    response = client.create_stack(
        StackName=str(stack_name),
        #TemplateBody=str(json_string),
        TemplateURL=str(url),
        #Parameters=[
        #    {
        #        'ParameterKey': 'KeyName',
        #        'ParameterValue': str(key_name),
        #        'UsePreviousValue': False
        #    },
        #    {
        #        'ParameterKey': 'EcsClusterName',
        #        'ParameterValue': str(cluster_name),
        #        'UsePreviousValue': False
        #    },
        #],
        DisableRollback=True,
        #DisableRollback=True|False,
        TimeoutInMinutes=10,
        #NotificationARNs=[
        #    'string',
        #],
        #Capabilities=[
        #    'CAPABILITY_IAM',
        #],
        #ResourceTypes=[
        #    'string',
        #],
        #OnFailure='DELETE',
        #OnFailure='DO_NOTHING'|'ROLLBACK'|'DELETE',
        #StackPolicyBody='string',
        #StackPolicyURL='string',
        Tags=[
            {
                'Key': 'Name',
                'Value': 'test'
            },
        ]
    )

    print(pretty(response))

    complete = -1

    while complete < 0:


        response = client.list_stacks(
            StackStatusFilter=[
                'CREATE_IN_PROGRESS',
                'CREATE_FAILED',
                'CREATE_COMPLETE',
                'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
                'UPDATE_COMPLETE',
                'UPDATE_IN_PROGRESS'])


        stacks = response.get('StackSummaries')


        if len(stacks) > 0:
            dict = {}

            for s in stacks:
                list = []

                if s.get('StackName') == str(stack_name):
                    print(s.get('StackStatus'))

                    if s.get('StackStatus') == 'CREATE_COMPLETE':
                        complete = 1


            time.sleep(5)

except (KeyboardInterrupt, SystemExit):
    sys.exit()
