#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64
import subprocess
from Crypto.Cipher import AES


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('sts')

    print('Is it text or a file?')
    print('1. text')
    print('2. file')
    type = raw_input("Enter 1 or 2 [ENTER]")

    if int(type) == int(1):


        text = raw_input("Enter text to decrypt: [ENTER](Cntrl-C to exit)")

        response = client.decode_authorization_message(
            EncodedMessage=str(text)
        )
        #crypter = AES.new(response.get('Plaintext'))
        #print(crypter.decrypt(base64.b64decode(data)).rstrip())



        print(pretty(response))
        print(response['Plaintext'])
    else:
        path = raw_input("Enter file path [ENTER]")

        print('Is it text or binary file?')
        print('1. text')
        print('2. binary')
        btype = raw_input("Enter 1 or 2 [ENTER]")



        if int(btype) == int(1):

            with open(str(path), 'r') as myfile:
                text = myfile.read().replace('\n', '')

            response = client.decode_authorization_message(
                EncodedMessage=str(text)
            )

            print(response)
            file_name = raw_input("Enter the name of the file to save the encrypted key as?")

            print('file_name is: ' + str(file_name))
            f = open('/tmp/' + str(file_name), 'w')
            f.write(response['Plaintext'])
            f.close()

            print('file is: /tmp/' + str(file_name))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
