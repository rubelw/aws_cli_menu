#!/usr/bin/env python


import boto3.session
import sys
import time
from aws_cli_menu_helper import *
from inspect import currentframe



DEBUG =1


STATUS = [
    'TERMINATED_WITH_ERRORS',
    'TERMINATED'
]

PROJCODES = {
    "mfld":'mfld',
    "gsap":'gsap',
    "fp":'fp',
    "dl":'dl',
    "DL":'dl',
    "DataLake":"dl",
    'sluice':'dl',
    'Landsat':'dl',
    "thes":'thes',
    "THES":'thes',
    'parp':'parp',
    "parp-wx":'parp-wx',
    "parp-s":'parp-s',
    "parp-fm":'parp-fm',
    "io":'io',
    "uap":'uap',
    "lrm":'lrm',
    "psr":'psr',
    "uas":'uas',
    "cron":'cron',
    "arcd":'arcd',
    'ARCD':'arcd',
    'dwh':'dwh',
    "pol":'pol',
    'polaris':'pol',
    'POLARIS':'pol',
    "jump":'io',
    "PSR-Mantis":'psr',
    'mantis':'psr',
    'MANTIS':'psr',
    "kube":'kube',
    'KUBE':'kube',
    'parp-split':'parp',
    'ABTG':'abtg',
    'PSR':'psr',
    'skyf':'skyf',
    'SKYF':'skyf',
    'parp-wx-transforms':'parp-wx',
    'ngsa':'psr',
    'DSI-DevOps':'io',
    'ngsa': 'psr',
    'GNA': 'gna',
    'gna':'gna',
    'fsip':'fsip',
    'cgbs':'cgbs',
    'CGBS':'cgbs',
    'NGSA':'psr',
    'ngsa':'psr',
    'poc':'poc',
    'POC':'poc',
    'cicd':'io',
    'CICD':'io',
    'seda':'seda',
    'SEDA':'seda',
    'tdata':'tdata',
    'TDATA':'tdata',
    'peat':'peat',
    'PEAT':'peat',
    'isaia':'isaia',
    'ISAIA':'isaia'
}


PROJECTOWNERS = {
    "mfld":'kevin.hayes@pioneer.com',
    "fsip":'neil.hausmann@pioneer.com',
    "FSIP":'neil.hausmann@pioneer.com',
    "dl":'gabe.conrad1@pioneer.com',
    "DL":'gabe.conrad1@pioneer.com',
    "DataLake":'gabe.conrad1@pioneer.com',
    "sluice":'gabe.conrad1@pioneer.com',
    'Landsat':'gabe.conrad1@pioneer.com',
    "thes":'robin.emig@pioneer.com',
    "THES":'robin.emig@pioneer.com',
    'parp-fm':'mark.p.johnson@pioneer.com',
    'parp-s':'brent.myers@pioneer.com',
    'parp-wx':'tim.hart@pioneer.com',
    'parp': 'brent.myers@pioneer.com',
    'PARP': 'brent.myers@pioneer.com',
    "io":'randy.black@pioneer.com',
    "IO":'randy.black@pioneer.com',
    "lrm":'matt.king@pioneer.com',
    "psr":'kevin.hayes@pioneer.com',
    "PSR":'kevin.hayes@pioneer.com',
    'mantis':'kevin.hayes@pioneer.com',
    'MANTIS':'kevin.hayes@pioneer.com',
    'ngsa':'kevin.hayes@pioneer.com',
    'NGSA':'kevin.hayes@pioneer.com',
    "cgbs":'matt.king@pioneer.com',
    "CGBS": 'matt.king@pioneer.com',
    "arcd":'andy.beatty@pioneer.com',
    "ARCD":'andy.beatty@pioneer.com',
    "pol":'andy.beatty@pioneer.com',
    "POL":'andy.beatty@pioneer.com',
    "kube":'manny.ruidiaz@pioneer.com',
    "KUBE":'manny.ruidiaz@pioneer.com',
    'abtg':'justin.gerke@pioneer.com',
    'ABTG':'justin.gerke@pioneer.com',
    'dwh':'stefanmeier@pioneer.com',
    'DWH':'stefanmeier@pioneer.com',
    'DSI-DevOps': 'alex.eifler@pioneer.com',
    'skyf':'matt.king@pioneer.com',
    'SKYF':'matt.king@pioneer.com',
    'cicd':'randy.black@pioneer.com',
    'CICD':'randy.black@pioneer.com',
    'seda':'jeremy@comer@pioneer.com',
    'SEDA':'jeremy@comer@pioneer.com',
    'tdata':'michael.wang@pioneer.com',
    'TDATA':'michael.wang@pioneer.com',
    'peat':'nathan.lippert@pioneer.com',
    'PEAT':'nathan.lippert@pioneer.com',
    'isaia':'ryan.smith@pioneer.com',
    'ISAIA':'ryan.smith@pioneer.com'
}


DEPLOYERS = {
   "will.rubel@pioneer.com":"will.rubel@pioneer.com",
   "nathan.mikkelsen":"nathan.mikkelsen@pioneer.com",
   "dave.powers":"U549383@bsnconnect.mail.onmicrosoft.com",
   "nakhil.anand": "nakhil.anand@pioneer.com",
   "praveen.pankajakshan":"praveen.pankajakshan@pioneer.com",
   "ryand.smith":"ryand.smith@pioneer.com",
   "chaitanyam.potnuru":"chaitanyam.potnuru@pioneer.com",
   "venkatesh.innanji":"venkatesh.innanji@pioneer.com",
   "margaret.huang":"margaret.huang@pioneer.com",
   "scott.warren":"scott.warren@pioneer.com",
   "marc.caron":"marc.caron@pioneer.com",
   "duke.takle":"duke.takle@pioneer.com",
   "eifleral":"alex.eifler@pioneer.com",
   "srinivas.paladugu":"srinivas.paladugu@pioneer.com",
   "renju.krishna":"renju.krishna@pioneer.com",
   "pankhuri.arora":"pankhuri.arora@pioneer.com",
   "sowmiya.govindaraj":"sowmiya.govindaraj@pioneer.com",
   "nikhil.anand":"nikhil.anand@pioneer.com",
   "mahesh.jadhav":"mahesh.jadhav@pioneer.com",
   "vandna.chawla":"vandna.chawla@pioneer.com",
   "vinay.kaikala":"vinay.kaikala@pioneer.com",
   "ming.yang":"ming.yang@pioneer.com",
   "vijay.narsapuram":"vijay.narsapuram@pioneer.com",
   "Adam.Johnson":"Adam.Johnson@pioneer.com",
   "AutoScaling":"AutoScaling",
   "AWSCloudFormation":"AWSCloudFormation",
   "CCSSession":"CCSSession",
   "duke.takle@pioneer.com":"duke.takle@pioneer.com",
   "mitali.nimkar":"mitali.nimkar@pioneer.com",
   "priyanka.bhole":"priyanka.bhole@pioneer.com"

}


REQUIRED_TAGS = {
  "Name": "*",
  "Project": "*",
  "ResourceOwner": "*",
  "DeployedBy": "*",
}


ENVIRONMENTS={
    'dsi-dev':'dv',
    'dsi-qa':'qa',
    'dsi-prod':'pd',
    'dsi-utility':'ut',
    'phigalileo':'ga',
    'dsi-sandbox':'sb',
    'ftiosandbox':'pg',
    'dsi-nonprod':'np'
}


def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno


def find_envcode(tags,profile):

    for t in tags:
        if t['Key'] == 'EnvCode':
            return t['Value']

    return ENVIRONMENTS[profile]

def find_projcode_by_resource_name(name):

    print('Trying to find projcode for '+str(name))

    if name:
        parts = name.replace('/', '-').split('-')

        for p in parts:
            print(p)


            if p in PROJCODES:
                print('found project code: '+str(PROJCODES[p]))
                return PROJCODES[p]

    return None


def find_project(name,tags, profile,clusterid):

    print('Trying to find project - instanceid: '+str(clusterid)+' - line: '+str(get_linenumber()))
    projcode = None
    name = None

    for t in tags:
        if t['Key'] == 'Project':
            print('Project tag is: '+str(t['Value']))
            return t['Value']
        elif t['Key'] == 'ProjCode':
            projcode = t['Value']
        elif t['Key'] == 'Name':
            name = t['Value']


    #  Try and find the project code based on the instance name
    if not projcode:
        projcode = find_projcode_by_resource_name(name)

    # Setting everything to datalake for phigalileo
    if not projcode and ENVIRONMENTS[profile] == 'phigalileo':
        projcode = 'dl'

    # Setting everything to proof of concept for ftiosandbox
    if not projcode and ENVIRONMENTS[profile] == 'ftiosandbox':
        projcode = 'poc'

    if projcode:

        # Setting the Project code
        if projcode in PROJCODES:

            try:

                print('### Trying to update tags to: '+str(PROJCODES[projcode])+' - line: '+str(get_linenumber()))
                print('clusterid: #'+str(clusterid)+'#')
                session = boto3.Session(region_name='us-east-1')
                client = session.client('emr')
                response = client.add_tags(
                    ResourceId=str(clusterid),
                    Tags=[
                        {
                            'Key': 'Project',
                            'Value': PROJCODES[projcode]
                        }
                    ]
                )

                return PROJCODES[projcode]

            except Exception as e:
                print('Function failed due to exception.')
                print(e)
                #traceback.print_exc()
                print("Could not add project tag for cluster "+str(clusterid)+' Error:'+str(e),tags)

        else:
            print('Project code of '+str(projcode)+' not in PROJCODES dict for config rule tag checker')
    else:
        print('Could not find projcode for clusterid: '+str(clusterid)+' line: '+str(get_linenumber()))


def find_violation(current_tags):
  if DEBUG:
      print('current_tags: '+str(current_tags)+' - line: '+str(get_linenumber()))


  violation = ""
  #for rtag, rvalues in REQUIRED_TAGS.iteritems():
  for rtag in REQUIRED_TAGS:
    print('rtag: '+str(rtag)+' - line: '+str(get_linenumber()))

    tag_present = False
    for tag in current_tags:
      print('tag key is: '+str(tag['Key'])+' - line: '+str(get_linenumber()))
      print('tag value is: '+str(tag['Value'])+' -line: '+str(get_linenumber()))
      if str(tag['Key']) == str(rtag):
        value_match = False

        tag_present = True
        for rvalue in REQUIRED_TAGS[rtag].split(","):
          if tag['Value'] == rvalue:
            value_match = True
          if tag['Key'] != "":
            if rvalue == "*":
              value_match = True

        if not value_match:
          violation = violation + "\n" + tag + ":" + tag['Key'] + " does not match value of " + str(
                      rtag) + ":" + REQUIRED_TAGS[rtag] + "."
    if not tag_present:
      violation = violation + "\n" + "Tag " + str(rtag) + " is not present."
  if violation == "":
    return None
  return violation


def get_emr_details(clusterid,tags,profile):

    project = None
    projcode = None
    deployedby = None
    owner = None
    keyname = None
    envcode = None
    name = None
    approver = None
    resourceowner = None
    deployedby = None




    if not envcode:
        print('Trying to find envcode - line: '+str(get_linenumber()))
        envcode = find_envcode(tags, profile)
        if envcode:
            print('found envcode of: '+str(envcode)+' - line: '+str(get_linenumber()))

    for t in tags:
        if t['Key'] == 'Name' or t['Key']=='name':
            name= t['Value']
            print('found name of: '+str(name)+' - line: '+str(get_linenumber()))

    if not project and name:
        print('There is not a Project tag')
        project = find_project(name,tags, profile,clusterid)
    else:
        print('Project code is: '+str(project))


    info = {}
    info['name'] = name
    info['envcode'] = envcode
    info['project'] = project
    info['projcode'] = projcode
    info['approver'] = approver
    info['deployedby'] = deployedby
    info['owner']= owner
    info['resourceowner']=resourceowner

    return info


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('emr')

    marker = None

    CLUSTERS = []

    while True:
        if marker:
            response_iterator = client.list_clusters(
                Marker=marker
            )
        else:
            response_iterator = client.list_clusters()

        for cluster in response_iterator['Clusters']:

            if str(cluster['Status']['State']) not in STATUS:
                print(pretty(cluster))
                print(cluster['Name'])
                print("\t" + "id: " + str(cluster['Id']))

                CLUSTERS.append(cluster['Id'])

        try:
            marker = response_iterator['Marker']
            print(marker)
        except KeyError:
            break

    print(CLUSTERS)


    for c in CLUSTERS:
        response = client.describe_cluster(
            ClusterId=str(c)
        )
        time.sleep(1)

        print(response)
        if 'Tags' in response['Cluster']:
            for t in response['Cluster']['Tags']:
                print('tag:'+str(t))

            violation = find_violation(response['Cluster']['Tags'])

            if violation:
                print(violation)
                time.sleep(5)
                #return {
                #    "compliance_type": "NON_COMPLIANT",
                #    "annotation": violation
                #}

                info = get_emr_details(c,response['Cluster']['Tags'],profile_name)



            else:
                print('cluster is compliant')
                time.sleep(1)





except (KeyboardInterrupt, SystemExit):
    sys.exit()
