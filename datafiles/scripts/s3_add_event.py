#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


def get_event():

    menu = {
        1:'s3:ReducedRedundancyLostObject',
        2:'s3:ObjectCreated:*',
        3:'s3:ObjectCreated:Put',
        4:'s3:ObjectCreated:Post',
        5:'s3:ObjectCreated:Copy',
        6:'s3:ObjectCreated:CompleteMultipartUpload',
        7:'s3:ObjectRemoved:*',
        8:'s3:ObjectRemoved:Delete',
        9:'s3:ObjectRemoved:DeleteMarkerCreated'
    }

    print "\n\n"
    print '#########################################'
    print '## Select Event                        ##'
    print '#########################################'
    for key in sorted(menu):
        print(str(key) + ":" + menu[key])

    pattern = r'^[0-9]+$'
    while True:

        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                return menu[int(ans)]



def get_lambda(session):

    client = session.client('lambda')


    response = client.list_functions()
    stacks = response.get('Functions')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            menu[counter] = s['FunctionName']

        print "\n\n"
        print '#########################################'
        print '## Select Function                     ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    function_name = menu[int(ans)]
                    break

        print('function name: '+str(function_name))
        response = client.get_function_configuration(
            FunctionName=function_name
        )

        print(pretty(response))

        return response['FunctionArn']

    else:
        print("\n")
        print("#########################")
        print('No Functions found')
        print("#########################")


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        names = []

        for s in stacks:
            name = s.get('Name')
            names.append(name)

        counter = 0
        for item in sorted(names):
            counter = counter + 1
            menu[counter] = item

        if len(menu) > 0:

            print "\n\n"
            print '#########################################'
            print '## S3 Buckets                          ##'
            print '#########################################'
            for key in sorted(menu):
                print(str(key) + ":" + menu[key])

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    bucket_name = menu[int(ans)]
                    break


        menu={1:'TopicConfiguration',2:'QueueConfiguration',3:'CloudFunctionConfiguration'}

        print("\n\n")
        print("#####################################")
        print("Select Configuration Type")
        print("#####################################")
        for key in sorted(menu):
            print(str(key) + ":" + menu[key])

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    answer = int(ans)
                    break


        if answer ==1:
            print('not configured')
        elif answer ==2:
            print('not configured')
        elif answer ==3:
            print('CloudFunctionConfiguration')

            event = get_event()
            print('event is: '+str(event))

            lambda_arn = get_lambda(session)
            print(lambda_arn)
            response = client.put_bucket_notification(
                Bucket=str(bucket_name),
                NotificationConfiguration={
                    'CloudFunctionConfiguration': {
                        #'Id': 'string',
                        'Event':str(event),
                        #'Events': [
                        #    's3:ReducedRedundancyLostObject'|'s3:ObjectCreated:*'|'s3:ObjectCreated:Put'|'s3:ObjectCreated:Post'|'s3:ObjectCreated:Copy'|'s3:ObjectCreated:CompleteMultipartUpload'|'s3:ObjectRemoved:*'|'s3:ObjectRemoved:Delete'|'s3:ObjectRemoved:DeleteMarkerCreated',
                        #],
                        'CloudFunction': str(lambda_arn)
                        #'InvocationRole': 'string'
                    }
                }
            )

            print(pretty(response))

        #response = client.get_bucket_notification(
        #    Bucket=str(bucket_name)
        #)

        #print(pretty(response))

    else:
        print("\n")
        print("##################")
        print('No S3 Buckets')
        print("##################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()
