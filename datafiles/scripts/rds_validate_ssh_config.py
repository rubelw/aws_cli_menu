#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re
import subprocess
from os.path import expanduser
import os
import collections
import shutil
import copy

LOCALFORWARD_PORTS = []
LOCALFORWARD_URLS = []

try:

    # Parse the ssh config file


    home = expanduser("~")
    user_config_file = home + '/.ssh/config'

    ssh_rds_instances = {}

    d = collections.OrderedDict()

    if os.path.exists(user_config_file):

        hostname = None
        for line in open(user_config_file):
            new_line=line.decode("utf-8").rstrip().split(" ", 1)
            print(new_line)


            if new_line[0]  in ['LocalForward','Host','HostName','StrictHostKeyChecking','User','UserKnownHostsFile','IdentityFile','IdentitiesOnly','ProxyCommand'] or new_line[0] =='' or new_line[0]=='UserKnownHostsFile=/dev/null':

                print(new_line)

                if new_line[0] == 'Host':
                    d[new_line[1]]= {}
                    hostname = str(new_line[1])
                    print('found hostname: '+str(hostname))
                elif len(new_line)>1 and new_line[0]=='':
                    list= new_line[1].strip().split()
                    print('list: '+str(list))
                    if len(list)>1 and list[0] != '':
                        if list[0] == 'LocalForward':
                            if 'LocalForward' in d[str(hostname)]:
                                lf = {}
                                lf[str(list[2])]=str(list[1])
                                LOCALFORWARD_PORTS.append(list[1])
                                LOCALFORWARD_URLS.append(str(list[2]))
                                d[str(hostname)]['LocalForward'].append(lf)
                            else:
                                d[str(hostname)]['LocalForward']=[]
                                lf = {}
                                lf[str(list[2])]=str(list[1])
                                LOCALFORWARD_PORTS.append(list[1])
                                LOCALFORWARD_URLS.append(str(list[2]))
                                d[str(hostname)]['LocalForward'].append(lf)
                        elif list[0] == 'ProxyCommand':
                                new_list = copy.deepcopy(list)
                                # Remove name
                                new_list.pop(0)
                                d[str(hostname)][str(list[0])]=str(" ".join(new_list))
                        else:
                            d[str(hostname)][str(list[0])]=str(list[1])
                    if len(list)>0 and list[0] != '':
                        if list[0] == 'UserKnownHostsFile=/dev/null':
                            d[str(hostname)]['UserKnownHostsFile'] = str('=/dev/null')
                            print('found it')
                            raw_input('press enter to continue')




    print(LOCALFORWARD_URLS)
    raw_input("Press Enter to continue...")

    for key in d:
        print(key)

        if key == 'dsi-utility-jump':
            print(d[key])


    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('rds')

    response = client.describe_db_instances()

    if 'DBInstances' in response:
        stacks = response.get('DBInstances')

        if len(stacks) > 0:
            menu = {}
            counter=0
            for s in stacks:
                endpoint = str(s['Endpoint']['Address']) + ':' + str(s['Endpoint']['Port'])
                if endpoint not in LOCALFORWARD_URLS:
                    flag = -1

                    if 'dsi-utility-jump' in d:
                        if 'LocalForward' in d['dsi-utility-jump']:
                            for item in d['dsi-utility-jump']['LocalForward']:
                                if str(endpoint) in item:
                                    print('found '+str(endpoint)+' in config')
                                    flag =1

                    if flag<0:
                        counter+=1
                        my_list = []
                        my_list.append(s['DBName'])
                        my_list.append(s['DBInstanceIdentifier'])
                        my_list.append(s['Endpoint']['Address']+':'+str(s['Endpoint']['Port']))

                        menu[counter]=my_list



            print('#######################')
            print('Select Which Database To Add To SSH Config')
            print('#######################')
            for key in sorted(menu):
                print str(key)+": " + str(menu[key][2])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        endpoint = menu[int(ans)][2]
                        break

            if len(LOCALFORWARD_PORTS)>0:
                local_port=int(max(LOCALFORWARD_PORTS))+1
                print('new_port: '+str(local_port))
                lf = {}
                lf[str(endpoint)] = str(local_port)
                d['dsi-utility-jump']['LocalForward'].append(lf)

            else:
                local_port = raw_input("Enter local port number to utilize: [ENTER](Cntrl-C to exit)")

                lf = {}
                lf[str(endpoint)] = str(local_port)
                d['dsi-utility-jump']['LocalForward'].append(lf)

            shutil.copy(user_config_file,user_config_file+'.bak')

            config_file = open(user_config_file, 'w')
            for k,v in d.items():
                config_file.write('Host '+str(k)+'\n')
                for k1,v1 in v.items():
                    if str(k1) == 'LocalForward':
                        for item in v1:
                            print('item: '+str(item))
                            config_file.write("    "+'LocalForward '+str(item[item.keys()[0]])+' '+str(item.keys()[0])+'\n')
                    elif str(k1) == 'UserKnownHostsFile':
                        config_file.write("    "+str(k1)+str(v1).strip()+'\n')
                    else:
                        config_file.write("    "+str(k1)+' '+str(v1)+'\n')
                config_file.write("\n")

            config_file.close()

        else:
            print("\n")
            print("####################")
            print('No RDS Instances')
            print("####################")
    else:
        print("\n")
        print("#####################")
        print('No RDS Instances')
        print("#####################")



except (KeyboardInterrupt, SystemExit):
    sys.exit()