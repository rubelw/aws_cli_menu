#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('efs')

    response = client.describe_file_systems(
    )

    if 'FileSystems' in response:
        if len(response['FileSystems']) >0:
            for f in response['FileSystems']:
                print(pretty(f))


        fs = response.get('FileSystems')
        menu = {}
        counter=0

        for f in fs:
            counter += 1
            my_list = []
            my_list.append(f['Name'])
            my_list.append(f['FileSystemId'])
            my_list.append(f)
            menu[counter] = my_list


        if len(menu) > 0:
            print "\n"
            print '#########################################'
            print '## Select File System                  ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]+" "+menu[key][1]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][2]
                        break


            print(pretty(info))

            response = client.describe_mount_targets(
                MaxItems=99,
                FileSystemId=info['FileSystemId'],
            )

            print(pretty(response))



    else:
        print('No file systems')





except (KeyboardInterrupt, SystemExit):
    sys.exit()
