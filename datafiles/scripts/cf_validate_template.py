#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('cloudformation')

    url = raw_input("Enter template url: [ENTER](Cntrl-C to exit)")
    response = client.validate_template(
        TemplateURL=str(url)
    )

    print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
