#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
from os.path import expanduser
import re
from dateutil.parser import *
import datetime
from collections import defaultdict

DEBUG =0

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


def get_all_account_profiles():
        home = expanduser("~")
        cred_file = home+'/.aws/credentials'

        lines = [line.rstrip('\n') for line in open(cred_file)]

        profiles = []
        for line in lines:
                matchObj = re.match( r'^\s*\[(.*)\]\s*$', line, re.M|re.I)
                if matchObj:

                    if matchObj.group(1) <> 'default':
                        profiles.append(matchObj.group(1))

        return profiles

try:

    profile_names = get_all_account_profiles()



    if DEBUG:
        print('profile names: '+str(profile_names))


    users = []

    if len(profile_names) >0:
            for p in profile_names:

                print('Getting information for profile name: '+str(p))

                try:
                    session = boto3.session.Session(profile_name=p)

                    client = session.client('cloudtrail')

                    marker = None
                    while True:
                        if marker:
                            response = client.lookup_events(
                                LookupAttributes=[
                                    {
                                        'AttributeKey': 'EventName',
                                        'AttributeValue': 'StartInstances'
                                    },
                                ],
                                StartTime=datetime.datetime(2018, 2, 20),
                                EndTime=datetime.datetime(2018, 2, 21),
                                MaxResults=50,
                                NextToken=marker
                            )
                        else:
                            response_iterator = client.lookup_events(
                                LookupAttributes=[
                                    {
                                        'AttributeKey': 'EventName',
                                        'AttributeValue': 'StartInstances'
                                    },
                                ],
                                StartTime=datetime.datetime(2018, 2, 20),
                                EndTime=datetime.datetime(2018, 2, 21),
                                MaxResults=50                          )
                        print(response_iterator)
                        for events in response_iterator['Events']:
                            # print(pretty(log_group))
                            # raw_input("Press Enter to continue...")

                            print(events['Username'])
                            users.append(events['Username'])

                        try:
                            marker = response_iterator['NextToken']
                            print(marker)
                        except KeyError:
                            break


                except Exception as e:
		            print(e)




    else:
        print('There are not any profiles')
        sys.exit()


    print(pretty(users))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
