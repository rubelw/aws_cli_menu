#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64
from Crypto.Cipher import AES

pad = lambda s: s + (32 - len(s) % 32) * ' '

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('kms')

    response = client.list_aliases(
        Limit=99
    )

    #print(pretty(response))
    new_aliases = {}
    if 'Aliases' in response:
        aliases = response.get('Aliases')

        for a in aliases:
            #print(pretty(a))
            if 'AliasName' in a:
                #print('There is an alias name in dict')
                new_aliases[str(a['TargetKeyId'])] = []
                new_aliases[str(a['TargetKeyId'])].append(a['AliasName'])
                new_aliases[str(a['TargetKeyId'])].append(a['AliasArn'])



    #print(pretty(new_aliases))
    response = client.list_keys(
        Limit=99,
    )

    #print(pretty(response))


    if 'Keys' in response:
        keys = response.get('Keys')
        menu = {}
        counter=0

        for k in keys:
            counter += 1
            my_list = []
            my_list.append(k['KeyArn'])
            my_list.append(k['KeyId'])
            if str(k['KeyId']) in new_aliases:
                my_list.append(new_aliases[str(k['KeyId'])][0])
                my_list.append(new_aliases[str(k['KeyId'])][1])
            else:
                my_list.append('None')
                my_list.append('None')
            my_list.append(k)
            menu[counter] = my_list


        if len(menu) > 0:
            print "\n"
            print '#########################################'
            print '## Select KeyArn                       ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][2]+" "+menu[key][1]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][4]
                        break


        #print(pretty(info))


        response = client.generate_data_key(
            KeyId=info['KeyId'],
            KeySpec='AES_256'
        )

        print(pretty(response))

        ciphertext_blob = response.get('CiphertextBlob')
        plaintext_key = response.get('Plaintext')

        #print('ciphertext_blob: '+str(ciphertext_blob))
        #print('plaintext_key: '+str(plaintext_key))

        crypter = AES.new(plaintext_key)

        data = raw_input("Enter text to encrypt: [ENTER](Cntrl-C to exit)")
        encrypted_data = base64.b64encode(crypter.encrypt(pad(str(data))))


        print('encrypted data is: '+str(encrypted_data))
        print('ciphertext_blob: '+str(base64.b64encode(ciphertext_blob)))

        print('To decrypt:')
        print('response = client.generate_data_key(')
        print('    KeyId=info[\'KeyId\'],')
        print('    KeySpec=\'AES_256\'')
        print(')')
        print('ciphertext_blob = response.get(\'CiphertextBlob\')')
        print('response2 = client.decrypt(')
        print('    CiphertextBlob=ciphertext_blob')
        print(')')
        print('crypter = AES.new(response2.get(\'Plaintext\'))')
        print('print(crypter.decrypt(base64.b64decode(encrypted_data)).rstrip())')



except (KeyboardInterrupt, SystemExit):
    sys.exit()
