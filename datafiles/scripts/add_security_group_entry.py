#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('ec2')
    response = client.describe_security_groups()
    stacks = response.get('SecurityGroups')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            my_list = []
            my_list.append(s['GroupName'])
            my_list.append(s['Description'])
            my_list.append(s['GroupId'])
            my_list.append(s)
            menu[counter] = my_list

        print "\n"
        print '#########################################'
        print '## Select Security Group               ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0] + '-' + str(menu[key][1])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    groupname = menu[int(ans)][0]
                    groupid = menu[int(ans)][2]
                    perms = menu[int(ans)][3]
                    break




        print("\n")
        print("######################################")
        print("Detailed Security Group Information")
        print("######################################")


        print(pretty(perms['IpPermissions']))

        cidr = raw_input("Enter CIDR block: [ENTER](Cntrl-C to exit)")
        desc = raw_input("Enter CIDR block description: [ENTER](Cntrl-C to exit)")
        from_port = raw_input("Enter from ingress port: [ENTER](Cntrl-C to exit)")
        to_port = raw_input("Enter to ingress port: [ENTER](Cntrl-C to exit)")
        protocol = multiple_choice('Select Protocol', ['tcp', 'udp','icmp'])

        new_entry = {}
        new_entry['PrefixListIds']=[]
        new_entry['FromPort']= int(from_port)
        new_entry['IpRanges'] = []
        details = {}
        details['Description']=str(desc)
        details['CidrIp']=str(cidr)
        new_entry['IpRanges'].append(details)
        new_entry['ToPort']= int(to_port)
        new_entry['IpProtocol']= str(protocol)
        new_entry['UserIdGroupPairs']=[]
        new_entry['Ipv6Ranges']=[]

        perms['IpPermissions'].append(new_entry)

        print(pretty(perms['IpPermissions']))

        response = client.update_security_group_rule_descriptions_ingress(
            DryRun=False,
            GroupId=str(groupid),
            IpPermissions=perms['IpPermissions']
        )

        print(response)





    else:
        print("\n")
        print("##############################")
        print('No security groups found')
        print("##############################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
