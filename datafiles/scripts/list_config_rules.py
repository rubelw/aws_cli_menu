#!/usr/bin/env python


import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name, region_name='us-east-1')

    client = session.client('config')

    response = client.describe_config_rules()


    print(pretty(response))
except (KeyboardInterrupt, SystemExit):
    sys.exit()
