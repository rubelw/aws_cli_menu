#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('cloudformation')
    response = client.list_stacks(
        StackStatusFilter=[
            'CREATE_IN_PROGRESS',
            'CREATE_FAILED',
            'CREATE_COMPLETE',
            'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
            'UPDATE_COMPLETE',
            'UPDATE_IN_PROGRESS'])
    stacks = response.get('StackSummaries')

    if len(stacks) > 0:
        dict = {}

        for s in stacks:
            list = []
            list.append(s.get('StackName'))
            list.append(s.get('StackStatus'))
            dict[s.get('StackName')] = list

        print("\n\n")
        print('##################################')
        print('## Select Stacks             ##')
        print('##################################')

        menu = {}
        counter=0

        for key, value in sorted(dict.iteritems()):


            counter+=1
            my_list = []
            my_list.append(key)
            my_list.append(value)

            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select Method'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    stack_name = menu[int(ans)][0]
                    break

        response = client.describe_stacks(
            StackName=str(stack_name),
        )

        print(pretty(response))

        print(pretty(response['Stacks'][0]['Outputs']))

    else:
        print("\n")
        print("#############################")
        print('No undeleted stacks found')
        print("#############################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
