#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


try:


    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('iam')

    print("\n")
    policy_name = raw_input("Enter policy name: [ENTER]")

    policy_description = raw_input("Enter policy description: [ENTER]")

    policy_document = raw_input("Enter policy document (dictionary): [ENTER]")



    client = session.client('iam')
    response = client.create_policy(
        PolicyName=policy_name,
        Path='/',
        PolicyDocument=str(policy_document),
        Description='string')

    print(pretty(response))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
