#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys



try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('apigateway')
    response = client.get_rest_apis()

    #print(pretty(response))
    stacks = response.get('items')


    if len(stacks)>0:

        menu = {}
        counter=0

        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['name'])
            if 'description' in s:
                my_list.append(s['description'])
            else:
                my_list.append('No description')
            my_list.append(s)
            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select API'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][2]
                    break

        api_id = info['id']

        response = client.get_stages(
            restApiId=str(api_id),
        )

        print(pretty(response))

        stacks = response.get('item')


        if len(stacks)>0:

            menu = {}
            counter=0

            for s in stacks:
                counter+=1
                my_list = []
                my_list.append(s['stageName'])
                my_list.append(s['deploymentId'])
                my_list.append(s)
                menu[counter]=my_list

            print "\n"
            print '#############################'
            print '## Select Stage'
            print '#############################'
            for key in sorted(menu):
                print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

            pattern = r'^[0-9]+$'
            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        stage_name = menu[int(ans)][0]
                        break



        response = client.get_export(
            restApiId=str(api_id),
            stageName=str(stage_name),
            exportType='swagger',
            parameters={
                'extensions': 'integrations'
            },
            accepts='application/json'
        )

        print(pretty(response))
        huge_body = response['body'].read()


        print(pretty(huge_body))




    else:
        print("\n")
        print("##########################")
        print('No APIs Found')
        print("##########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
