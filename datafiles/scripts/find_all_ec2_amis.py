#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
from os.path import expanduser
import re
from dateutil.parser import *
import datetime
from collections import defaultdict

DEBUG =1

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


def get_all_account_profiles():
        home = expanduser("~")
        cred_file = home+'/.aws/credentials'

        lines = [line.rstrip('\n') for line in open(cred_file)]

        profiles = []
        for line in lines:
                matchObj = re.match( r'^\s*\[(.*)\]\s*$', line, re.M|re.I)
                if matchObj:

                    if matchObj.group(1) <> 'default':
                        profiles.append(matchObj.group(1))

        return profiles

try:

    profile_names = get_all_account_profiles()

    if DEBUG:
        print('profile names: '+str(profile_names))

    ec2info = defaultdict()


    if len(profile_names) >0:
            for p in profile_names:

                print('Getting information for profile name: '+str(p))

                try:
                    session = boto3.session.Session(profile_name=p)

                    client = session.resource('ec2')

                    if DEBUG:
                        print('Created session client')


                    # Get information for all running instances
                    running_instances = client.instances.filter(Filters=[{
                        'Name': 'instance-state-name',
                        'Values': ['running']}])



                    if DEBUG:
                        print('Have the information for all running instances:')
                        print(running_instances)


                    for instance in running_instances:

                        for tag in instance.tags:
                            if 'Name'in tag['Key']:
                                name = tag['Value']

                        if len(name)<1:
                            name = 'no_name'

                        # Add instance info to a dictionary
                        ec2info[instance.id] = {
                              'Environment':p,
                              'Name': name,
                             'Type': instance.instance_type,
                             'AMI': instance.image_id
                        }

                except Exception as e:
		            print(e)




    else:
        print('There are not any profiles')
        sys.exit()

    amis = {}
    print(pretty(ec2info))

    for item in ec2info:
        amis[ec2info[item]['AMI']]= ec2info[item]['Environment']

    print(pretty(amis))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
