#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import stackility
import sys
import subprocess

try:

    profile= get_profile_name()
    print("\n\n")

    region = select_region()

    ini_data = {}
    environment = {}
    environment['region'] = region
    environment['profile'] = profile
    ini_data['environment'] = environment

    stack_driver = stackility.CloudStackUtility(ini_data)

    print(stack_driver.list())


except (KeyboardInterrupt, SystemExit):
    sys.exit()