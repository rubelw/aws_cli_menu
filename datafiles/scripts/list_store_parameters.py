#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('ssm')
    response = client.describe_parameters()

    #print(pretty(response))
    stacks = response.get('Parameters')


    if len(stacks)>0:

        menu = {}
        counter=0

        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            if 'Description' in s:
                my_list.append(s['Description'])
            else:
                my_list.append('No description')
            my_list.append(s)
            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select Parameter'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][2]
                    break


        key_id = info['KeyId']
        name = info['Name']
        if info['Type'] == 'SecureString':
            encryption = True
        else:
            encryption = False

        response = client.get_parameter(
            Name=str(name),
            WithDecryption=encryption
        )
        print(pretty(response))


    else:
        print("\n")
        print("##########################")
        print('No parameters Found')
        print("##########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
