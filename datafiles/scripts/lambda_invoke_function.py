#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('lambda')

    function_name = raw_input("Enter function name: [ENTER](Cntrl-C to exit)")
    payload = raw_input("Enter payload: [ENTER](Cntrl-C to exit)")

    response = client.invoke(
        FunctionName=str(function_name),
        #InvocationType='Event',
        #InvocationType='Event'|'RequestResponse'|'DryRun',
        #LogType='None'|'Tail',
        #ClientContext='string',
        #Payload='{"body-json" : {},"params" : {"path" : {},"querystring" : {},"header" : {}},"stage-variables" : {},"context" : { "http-method" : "GET","stage" : "test-invoke-stage","user-agent" : "Apache-HttpClient/4.5.x (Java/1.8.0_102)", "request-id":"test-invoke-request", "resource-path" : "/version"}}'
        Payload='{"body-json" : {},"params" : {"path" : {},"querystring" : {},"header" : {}},"stage-variables" : {},"context" : { "http-method" : "GET","resource-path" : "/version"}}'
        #Qualifier='string'

    )

    print(response)
    for i in response:
        print(i)

    print('###')
    print(response['Payload'])
    r = response['Payload']
    s = r.read()
    print('body: '+str(s))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
