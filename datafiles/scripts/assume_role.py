#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json




try:


    #creds = get_rds_profile_name()
    #print(pretty(creds))

    profiles = get_rds_profiles()
    print(pretty(profiles))

    creds = profiles[1]['phidataprod']

    need_to_add_to_prod = {}
    dev = {}
    test = {}
    acc = {}
    supp= {}
    prod={}

    #conn_string = "dbname='"+str(creds['database'])+"' user='"+str(creds['user'])+"' host='localhost' port="+int(creds['port'])+" password='"+creds['password']+"'"
    conn_string = 'dbname=\''+str(creds['database'])+'\' user=\''+str(creds['user'])+'\' host=\'localhost\' port=\''+str(creds['port'])+'\' password=\''+str(creds['password'])+'\''

    print('conn_string is:'+str(conn_string))

    try:
        conn=psycopg2.connect(conn_string)
        print('Connected to database')

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()


        for row in rows:
            print "   ", row[0],row[3],row[4],row[5],row[6],row[7]
            prod[str(row[0])] = row

        cur.close()
        conn.close()

    except:
        print('Could not connect to database')

    # Fixme
    creds = profiles[1]['']

    conn_string = 'dbname=\''+str(creds['database'])+'\' user=\''+str(creds['user'])+'\' host=\'localhost\' port=\''+str(creds['port'])+'\' password=\''+str(creds['password'])+'\''

    print('conn_string is:'+str(conn_string))

    try:
        conn=psycopg2.connect(conn_string)
        print('Connected to database')

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()


        for row in rows:
            print "   ", row[0],row[3],row[4],row[5],row[6],row[7]
            supp[str(row[0])]=row
            if row[0] in prod:
                print('support role exists in production')
            else:
                print('support role: '+str(row[0]+' does not exists in production'))
                need_to_add_to_prod[str(row[0])] = 1


        cur.close()
        conn.close()


        print(pretty(need_to_add_to_prod))

    except:
        print('Could not connect to database')


    print('### Production ###')
    print(pretty(prod))
    print('### Support ###')
    print(pretty(supp))





except (KeyboardInterrupt, SystemExit):
    sys.exit()