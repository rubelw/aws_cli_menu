#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('apigateway')



    response = client.get_rest_apis()
    print(pretty(response))
    stacks = response.get('items')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:

            my_list = []

            my_list.append(s['id'])
            my_list.append(s['name'])
            counter += 1
            menu[counter] = my_list

        print "\n\n"
        print '#########################################'
        print '## Select API                          ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]+' - '+str(menu[key][1])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    api_id = menu[int(ans)][0]
                    break

        print('id: '+str(api_id))
        response = client.get_stages(
            restApiId=str(api_id)
            #deploymentId='string'
        )
        print(pretty(response))
        stacks = response.get('item')

        if len(stacks) > 0:

            menu = {}
            counter = 0
            for s in stacks:

                print(pretty(s))
                my_list = []

                my_list.append(s['stageName'])
                my_list.append(s)
                counter += 1
                menu[counter] = my_list

            print "\n\n"
            print '#########################################'
            print '## Select Stage                    ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        stage_name = menu[int(ans)][0]

                        break

            print('stage_name: '+str(stage_name))

            response = client.get_stage(
                restApiId=str(api_id),
                stageName=str(stage_name)
            )

            print(pretty(response))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
