#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
import datetime

DEBUG =0

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


now_utc = datetime.datetime.utcnow()
now_utc = now_utc.replace(microsecond=0)
valid_until = now_utc + datetime.timedelta(hours=1)


try:
    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('emr')

    menu = {}
    clusters = {}
    marker = None

    while True:
        if marker:
            response_iterator = client.list_clusters(
                Marker=marker
            )
        else:
            response_iterator = client.list_clusters()

        for cluster in response_iterator['Clusters']:
            list = []
            list.append(cluster['Name'])
            list.append(cluster['Id'])
            print('appending: '+str(cluster['Name']))
            clusters[cluster['Id']]=list
        try:
            marker = response_iterator['Marker']
            print(marker)
        except KeyError:
            break



    counter = 0
    for item in sorted(clusters):
        print(item)
        counter = counter +1
        menu[counter] = clusters[item]


    if len(menu) > 0:

        print("\n")
        print("######################")
        print("Select Cluster")
        print("######################")
        for key in sorted(menu):
            print(str(key)+":" + menu[key][0]+ ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    cluster_id = menu[int(ans)][1]
                    break

        print('cluster id: '+str(cluster_id))

        client = session.client('cloudtrail')
        marker = None
        while True:
            if marker:
                response = client.lookup_events(
                    LookupAttributes=[
                        {
                            'AttributeKey': 'ResourceName',
                            'AttributeValue': cluster_id
                        },
                    ],
                    StartTime=datetime.datetime(2018, 2, 20),
                    EndTime=valid_until ,
                    MaxResults=50,
                    NextToken=marker
                )
            else:
                response_iterator = client.lookup_events(
                    LookupAttributes=[
                        {
                            'AttributeKey': 'EventName',
                            'AttributeValue': 'StartInstances'
                        },
                    ],
                    StartTime=datetime.datetime(2018, 2, 20),
                    EndTime=valid_until ,
                    MaxResults=50)
            print(response_iterator)


        #    for events in response_iterator['Events']:
        #        # print(pretty(log_group))
        #        # raw_input("Press Enter to continue...")

        #        #print(events['Username'])
        #        #users.append(events['Username'])
        #        print(pretty(events))

        #        if 'Username' in events:
        #            print('Username is: '+str(events['Username']))
        #            sys.exit(0)


        #    try:
        #        marker = response_iterator['NextToken']
        #        print(marker)
        #    except KeyError:
        #        break


except (KeyboardInterrupt, SystemExit):
    sys.exit()


