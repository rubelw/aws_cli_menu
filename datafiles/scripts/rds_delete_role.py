#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re


DEBUG =1

try:


    creds = get_rds_profile_name()
    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    master_user = str(creds[profile][0]['user'])
    if (DEBUG):
        print('master user is: '+str(master_user))

    try:
        conn=psycopg2.connect(conn_string)

    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)


    cur = conn.cursor()
    cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

    rows = cur.fetchall()

    users = {}


    for row in rows:
        users[row[0]]=row

    cur.close()
    conn.close()

    menu = {}

    counter = 0

    for i in sorted(users):
        counter +=1
        menu[counter]=users[i]

    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select User")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")

            if (DEBUG):
                print('answer is: '+str(ans))

            if re.match(pattern, ans) is not None:
                if (DEBUG):
                    print('Found a match: '+str(ans))

                if int(ans) in menu:
                    if (DEBUG):
                        print('Selected: '+str(ans))

                    user_name = menu[int(ans)][0]
                    break

        if (DEBUG):
            print ('user_name: '+str(user_name))

        conn=psycopg2.connect(conn_string)

        if (DEBUG):
            print('connection string: '+str(conn_string))

        cur = conn.cursor()


        try:
            conn.commit()
        except  psycopg2.OperationalError as e:
            print('Error: \n{0}').format(e)
            sys.exit(1)

        # Need to check for a user
        try:
            cur.execute("DROP ROLE "+str(user_name))

        except psycopg2.OperationalError as e:
            print('Error: \n{0}').format(e)
            print ('May need to revoke permissions on tables in schema and schema usage')
            print('revoke all privileges on all tables in schema xxx from '+str(user_name))
            print('revoek usage on schema xxx from '+str(user_name))
            sys.exit(1)

        try:
            conn.commit()
        except psycopg2.OperationalError as e:
            print('Error: \n{0}').format(e)
            sys.exit(1)


        print("\n")
        print("##############################")
        print("User has been deleted")
        print("##############################")


    else:
        print("\n")
        print("####################")
        print('No users found')
        print("####################")




except (KeyboardInterrupt, SystemExit):
    sys.exit()