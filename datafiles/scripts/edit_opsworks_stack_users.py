#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('opsworks')
    response = client.describe_stacks()
    stacks = response.get('Stacks')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#############################'
        print '## Select Stack            ##'
        print '#############################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        print("\n")
        print("########################")
        print("Detailed Stack Info")
        print("########################")
        print(pretty(info))

        response = client.describe_user_profiles()
        #print(response)
        names={}
        users = response.get('UserProfiles')
        if len(users)>0:
            for u in users:
                #print(u['Name'])
                names[u['Name']]=u

        menu = {}
        temp = {}
        for i in names:
            my_list = []
            my_list.append(i)
            my_list.append(names[i])
            temp[i] = my_list

        counter = 0
        for i in sorted(temp):
            counter +=1
            menu[counter]=temp[i]

        print(pretty(menu))
        print("\n")
        print('#######################')
        print('Select User')
        print('#######################')
        for key in sorted(menu):
            print(str(key)+" : "+str(menu[key][0]))

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        print("\n")

        print(info)

        if info['AllowSelfManagement'] == True:
            ans = yes_or_no('Turn off self management?')
            if ans == 'Yes':
                response = client.update_user_profile(
                    IamUserArn=info['IamUserArn'],
                    AllowSelfManagement=False
                )
                print(response)

        else:
            ans = yes_or_no('Turn on self management?')
            if ans == 'Yes':
                response = client.update_user_profile(
                    IamUserArn=info['IamUserArn'],
                    AllowSelfManagement=True
                )
                print(response)

    else:
        print("\n")
        print("#######################")
        print('No stacks found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
