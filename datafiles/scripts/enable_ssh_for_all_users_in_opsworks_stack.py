#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('opsworks')
    response = client.describe_stacks()
    stacks = response.get('Stacks')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#############################'
        print '## Select Stack            ##'
        print '#############################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        print("\n")
        print("########################")
        print("Detailed Stack Info")
        print("########################")
        print(pretty(info))
        stack_id=info['StackId']

        response = client.describe_user_profiles()


        names={}
        users = response.get('UserProfiles')
        if len(users)>0:
            for u in users:
                #print(u['Name'])
                names[u['Name']]=u


        for key in sorted(names):
            print(names[key]['IamUserArn'])
            response = client.describe_permissions(IamUserArn=names[key]['IamUserArn'],StackId=str(stack_id))

            print("\t"+str(response['Permissions'][0]['AllowSsh']))

            if response['Permissions'][0]['AllowSsh']:
                print('already allowed to ssh')
            else:

                response = client.set_permission(
                    StackId=str(stack_id),
                    IamUserArn=str(names[key]['IamUserArn']),
                    AllowSsh=True,
                    AllowSudo=False,
                    Level='iam_only'
                )

                print(response)
    else:
        print("\n")
        print("#######################")
        print('No stacks found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
