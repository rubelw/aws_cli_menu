#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
from os.path import expanduser
import re
from dateutil.parser import *
import datetime
from collections import defaultdict

DEBUG =1

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


def get_all_account_profiles():
        home = expanduser("~")

        if (DEBUG):
            print('home: '+str(home))

        cred_file = home+'/.aws/credentials'

        lines = [line.rstrip('\n') for line in open(cred_file)]

        profiles = []
        for line in lines:
                matchObj = re.match( r'^\s*\[(.*)\]\s*$', line, re.M|re.I)
                if matchObj:

                    if matchObj.group(1) <> 'default':
                        profiles.append(matchObj.group(1))

        return profiles

try:

    profile_names = get_all_account_profiles()

    if DEBUG:
        print('profile names: '+str(profile_names))

    ec2info = defaultdict()


    unique_blocks = {}
    cidr_blocks = {}
    for p in profile_names:
        cidr_blocks[str(p)]=[]

    print(pretty(cidr_blocks))

    if len(profile_names) >0:
            for p in profile_names:

                print('Getting information for profile name: '+str(p))

                try:
                    session = boto3.session.Session(profile_name=p)


                    client = session.client('ec2')
                    response = client.describe_vpcs()


                    if 'Vpcs' in response:

                        stacks = response.get('Vpcs')

                        if len(stacks) > 0:


                            for s in stacks:

                                cidr_blocks[str(p)].append(str(s.get('CidrBlock')))
                                unique_blocks[str(s.get('CidrBlock'))]=str(p)


                except Exception as e:
		            print(e)

    else:
        print('There are not any profiles')
        sys.exit()

    print(pretty(cidr_blocks))

    for key in sorted(unique_blocks):
        print "%s: %s" % (key, unique_blocks[key])


    #print(pretty(unique_blocks))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
