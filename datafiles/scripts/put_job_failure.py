#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    job_id = raw_input("Enter Job ID: [ENTER](Cntrl-C to exit)")

    client = session.client('codepipeline')

    response = response = client.put_job_failure_result(
        jobId=str(job_id),
        failureDetails={
            'type': 'JobFailed',
            'message': 'Job Failed'
        }
    )

    print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
