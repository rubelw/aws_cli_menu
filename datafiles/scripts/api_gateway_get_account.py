#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('apigateway')
    response = client.get_account()
    response.pop('ResponseMetadata',None)
    print(pretty(response))



except (KeyboardInterrupt, SystemExit):
    sys.exit()
