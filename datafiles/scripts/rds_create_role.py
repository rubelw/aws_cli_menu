#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import subprocess
import re

DEBUG =0


try:


    creds = get_rds_profile_name()


    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''



    #profile = creds.keys()[0]
    #database = creds[profile][0]['database']
    #hostname = creds[profile][0]['hostname']
    #port = creds[profile][0]['port']

    user_name = raw_input("Enter name or user or role: [ENTER](Cntrl-C to exit)")
    cmd = 'openssl rand -base64 32'
    new_pwd = subprocess.check_output(cmd,shell=True)
    new_pwd = new_pwd.rstrip()



    #conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''


    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")


        rows = cur.fetchall()


        users = {}


        for row in rows:
            users[row[0]]=row

        cur.close()
        conn.close()

    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)


    menu = {}

    counter = 0

    for i in sorted(users):

        searchObj = re.search( r'_r[ow].*', i, re.M|re.I)

        if searchObj:
           counter+=1
           menu[counter] = users[i]


    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select Role")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    role_name = menu[int(ans)][0]
                    break



        print('############ SQL COMMANDS ################')
        print('create role '+str(user_name)+';')


        print('alter role '+str(user_name)+' with login;')
        print('alter role '+str(user_name)+' with password \''+str(new_pwd)+'\';')
        print('grant '+str(role_name)+' to '+str(user_name)+';')


        try:

            conn=psycopg2.connect(conn_string)
            print('Connected to database')

            cur = conn.cursor()

        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)


            # Need to check for a user

        try:
            cur.execute("CREATE ROLE "+str(user_name))
            conn.commit()
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)


        try:
            cur.execute('alter role '+str(user_name)+' with login')
            conn.commit()
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)

        try:
            cur.execute('alter role '+str(user_name)+' with password \''+str(new_pwd)+'\'')
            conn.commit()

        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)

        try:
            cur.execute('grant '+str(role_name)+' to '+str(user_name))
            conn.commit()

        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)


        cur.close()
        conn.close()




    else:
        print("\n")
        print("####################")
        print('No roles found')
        print("####################")



except (KeyboardInterrupt, SystemExit):
    sys.exit()