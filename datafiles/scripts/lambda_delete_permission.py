#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import json


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('lambda')


    response = client.list_functions()
    stacks = response.get('Functions')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            menu[counter] = s['FunctionName']

        print "\n\n"
        print '#########################################'
        print '## Select Function                     ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    function_name = menu[int(ans)]
                    break

        print('function name: '+str(function_name))
        response = client.get_policy(
            FunctionName=function_name
        )

        permissions = response['Policy']
        permissions = json.loads(permissions)
        permissions = permissions['Statement']


        menu = {}
        counter = 0

        for permission in permissions:

            counter += 1
            list=[]
            list.append(permission['Sid'])
            list.append(permission['Resource'])
            menu[counter] = list

        print "\n\n"
        print '#########################################'
        print '## Select Function                     ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    function_name = menu[int(ans)][0]
                    arn = menu[int(ans)][1]
                    break

        print('sid: '+str(function_name))
        print('arn: '+str(arn))

        response = client.remove_permission(
            FunctionName=str(arn),
            StatementId=str(function_name)
            #Qualifier='string'
        )

        print(pretty(response))

    else:
        print("\n")
        print("#########################")
        print('No Functions found')
        print("#########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
