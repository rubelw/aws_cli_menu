#!/usr/bin/env python

#from __future__ import print_function
import boto3.session
import math
from aws_cli_menu_helper import *
import sys
import os

new_menu = dict()
tmp_menu = dict()
confirmEachConnection = True
vpcPair = []
DEBUG =1

def my_sorter(x, y):
    global new_menu
    xs = str(new_menu[x][0])
    ys = str(new_menu[y][0])
    xs1 = xs[0:1]
    ys1 = ys[0:1]

    if xs1 == ' ' and ys1 == ' ':
        return cmp(xs, ys)
    if xs1 == ' ':
        return -1
    if ys1 == ' ':
        return  1
    if xs1.isupper() == True and ys1.isupper() == True:
        return cmp(xs, ys)
    elif xs1.isupper() == True and ys1.isupper() == False:
        return -1
    elif xs1.isupper() == False and ys1.isupper() == True:
        return 1
    else: # both lower
        return cmp(xs, ys)

def process_save(afile):
    global confirmEachConnection
    global vpcPair
    global DEBUG
    global new_menu
    global tmp_menu
    if DEBUG > 0:
        print 'enter: process_save({0})'.format(afile)

    n0 = 0
    n1 = 0
    n2 = 0
    for key in new_menu:
        if len(str(new_menu[key][0])) > n0:
            n0 = len(str(new_menu[key][0]))
        if len(str(new_menu[key][1])) > n1:
            n1 = len(str(new_menu[key][1]))
        if len(str(new_menu[key][2])) > n2:
            n2 = len(str(new_menu[key][2]))

    # get number of digits needed to pretty print the numbers on the left side
    n = len(new_menu)
    n = int(math.log10(n)) + 1
    fmt = "{:"+str(n)+"d} {:"+str(n0)+"s} | {:"+str(n1)+"s} | {:"+str(n2)+"s}"
    fmtH = re.sub(r'd', r's', fmt) # change the first "d" to an "s"

    tmp_menu = { }
    ndx = 1
    for key in sorted(new_menu, cmp=my_sorter):
       tmp_menu[ndx] = new_menu[key]
       ndx+=1

    if afile == '-':
        print fmtH.format(' ' , "NAME", "VPC", "CIDR")     # print out header
    else:
        try:
            out = open(afile, 'w')
        except Exception, e:
            print 'failed to open file: "{0}"\n'.format(afile)
            print 'reason: "{0}"\n'.format(str(e))
            print 'current working directory: {0}\n'.format(os.getcwd())
            sys.exit(1)

    for key in tmp_menu:                       # print the list
        outStr = fmt.format(key, str(tmp_menu[key][0]), str(tmp_menu[key][1]), str(tmp_menu[key][2]))
        if afile == '-':
            print '{0}'.format(outStr)
        else:
            outStr+= "\n"
            out.write(outStr)

    return True

def process_export(afile):
    global confirmEachConnection
    global vpcPair
    global DEBUG
    if DEBUG > 0:
        print 'enter: process_export({0})'.format(afile)
    return True

def process_import(afile):
    global confirmEachConnection
    global vpcPair
    global DEBUG
    if DEBUG > 0:
        print 'enter: process_import({0})'.format(afile)
    return True

def process_vpcpair():
    global confirmEachConnection
    global vpcPair
    global DEBUG
    if DEBUG > 0:
        print 'enter: process_vpcPair()'
    return True

def notnull(who, afile):
    if len(afile):
        return True;
    print 'The file for the "{0}" operation cannot be null, you entered: "{1}"'.format(who, afile)
    return False

try:

    profile_name = get_profile_name()
    print("\n")

    session = boto3.session.Session(profile_name=profile_name)

    print("\n")
    print("** Please wait 5-10 seconds while we collect the information. **")

    menu = {
        1:['VPCs',[0]],
    }

    #1 Get VPCs
    client = session.client('ec2')
    response = client.describe_vpcs()

    sub_menu = {}
    if 'Vpcs' in response:

        stacks = response.get('Vpcs')

        if len(stacks) > 0:
            counter = 0
            for s in stacks:   # s is a dictionary, stacks is a list of dictionaries
                name = ''
#               print 'CLEAR name "{0}"'.format(name)
                counter+=1
                my_list = []
#               print 's is: {0}'.format(pretty(s))
                for k,l in s.items(): # k is a key, l is a value (possible a list), s is a dictionary
                    if k == 'Tags':
                        for d in l:   # d is a dictonary, l is a value, and in this case a list of dictionaries
                            namevalue = ''
#                           print 'NULL  namevalue "{0}"'.format(namevalue)
#                           print 'd is: {0}'.format(pretty(d))
                            for dk,dv in d.items():    # dk is a key, dv a value, d is a dictionary
#                               print 'Next pair'
#                               print '    dk is: {0}'.format(pretty(dk))
#                               print '    dv is: {0}'.format(pretty(dv))
                                if dk == 'Value':
                                    namevalue = dv
#                                   print 'SET   namevalue "{0}"'.format(namevalue)
                                elif dk == 'Key' and dv == 'Name':
                                    name = namevalue
#                                   print 'name = namevalue "{0}"'.format(name)
                                    break
                            if len(name) > 0:
                                break          # for d in l
                        if len(name) == 0:
#                           name = 'TAG: '+str(s['Tags'][0]['Value'])
                            name = '            ** NONE **'
#                           print 'TAG   name      "{0}"'.format(name)

                # final check
                if len(name) == 0:
#                   name = 'NONE: - ' + str(s.get('VpcId'))
                    name = '          *** NONE ***'
#                   print 'NONE  name      "{0}"'.format(name)

                vpcid = s.get('VpcId')
                cidr_block = s.get('CidrBlock')

                my_list.append(name)
                my_list.append(vpcid)
                my_list.append(cidr_block)
                my_list.append(s)
                sub_menu[counter] = my_list


    if len(sub_menu)>0:
        menu[1][1][0] = int(len(sub_menu))
        menu[1][1].append(sub_menu)

    ans = 1
    results = menu[int(ans)]

    menu_title = results[0]
    count = results[1][0]
    new_menu = results[1][1]


    # Get VPCs
    print "\n"
    print '#########################################'
    print '## Select Two (2) VPCs                 ##'
    print '##  Enter the first one                ##'
    print '##  then some whitespace or a comma    ##'
    print '##  then the second one, e.g.: 3,5     ##'
    print '##  Enter 0 (zero) to exit             ##'
    print '#########################################'

    process_save('-')

    # match a single 0, or num<whitespace>num, or num,num with any
    # any amount of useless whitespace which will be thrown away
    pattern = r'^(\s*0\s*|\s*[1-9][0-9]*\s*,\s*[0-9][0-9]*\s*$)|\s*[0-9][0-9]*\s+[0-9][0-9]*\s*$'
    while True:
        vpcPair = []
        afile = ''
        ans = raw_input('Make A Choice, enter help, or enter a Command: [ENTER](Cntrl-C to exit)')
        lans = ans.strip().lower()
        if re.match(r'^h.*', lans) is not None:
            print 'Commands: '
            print '   help        - see this'
            print '   confirm     - for each peering connection to be created prompt for conformation'
            print '   automatic   - for each peering connection to be created DO NOT prompt for conformation'
            print '   list N      - for VPC number N show its peering connections'
            print '   save file   - write the list of VPCs to "file"'
            print '   export file - "file" is written with proper syntax for creating an import file'
            print '   import file - "file" is read and acted upon - the peering connections requested are made'
            print '   0, exit or quit'
        elif re.match(r'^c.*', lans) is not None:
            confirmEachConnection = True
            if DEBUG > 0:
                print 'confirm confirmEachConnection  ={0}   "{1}"'.format(confirmEachConnection, ans)
        elif re.match(r'^a.*', lans) is not None:
            confirmEachConnection = False
            if DEBUG > 0:
                print 'automatic confirmEachConnection  ={0}   "{1}"'.format(confirmEachConnection, ans)
        elif re.match(r'^s.*', lans) is not None:
            afile = re.sub(r'^[A-Za-z]+\s*', r'', ans).rstrip()
            if notnull("save", afile) is True:
                if DEBUG > 0:
                    print 'save {0}   "{1}"'.format(afile, ans)
                process_save(afile)
        elif re.match(r'^e.*', lans) is not None:
            afile = re.sub(r'^[A-Za-z]+\s+', r'', ans)
            if notnull("export", afile) is True:
                if DEBUG > 0:
                    print 'export {0}   "{1}"'.format(afile, ans)
                process_export(afile)
        elif re.match(r'^i.*', lans) is not None:
            if notnull("import", afile) is True:
                afile = re.sub(r'^[A-Za-z]+\s+', r'', ans)
                if DEBUG > 0:
                    print 'import {0}   "{1}"'.format(afile, ans)
                process_import(afile)
        elif re.match(r'^0+$', lans) is not None:
            sys.exit(0)
        elif re.match(r'^exit$', lans) is not None:
            sys.exit(0)
        elif re.match(r'^quit$', lans) is not None:
            sys.exit(0)
        elif re.match(pattern, ans) is not None:
            lans = re.sub(r',', r' ', lans)
            lans = re.sub(r'\s+', r' ', lans)
            if re.match(r'^[1-9][0-9]* [1-9][0-9]*$', lans) is not None:
                info = ''
                tl = 0
                for choice in lans.split(' '):
                    if int(choice) in tmp_menu:
                        info = str(tmp_menu[int(choice)][0]) # [len('Tag: '):]
                        if len(info) > tl:
                            tl = len(info)
                        vpcPair.append(info)
                        info = str(tmp_menu[int(choice)][1])
                        vpcPair.append(info)
                        info = str(tmp_menu[int(choice)][2])
                        vpcPair.append(info)
                    else:
                        print "selection {0} is invalid".format(choice)
                        break
                if len(vpcPair) == 6:
                    print "\n"
                    print '#########################################'
                    print '## Peering Connection Details          ##'
                    print '#########################################'
                    fmt = '    FROM: {0:'
                    fmt+= str(tl)
                    fmt+= 's}'
                    fmt+= '        '
                    fmt+= '{1}'
                    fmt+= '        '
                    fmt+= '{2}'
                    if DEBUG > 0:
                        print 'process pair       "{0}"'.format(ans)
                    print fmt.format(vpcPair[0], vpcPair[1], vpcPair[2])
                    fmt = re.sub(r'FROM:', r'TO:  ', fmt)
                    print fmt.format(vpcPair[3], vpcPair[4], vpcPair[5])
                    process_vpcpair()
            else:
                print 'answer: "{0}" is not recognized'.format(ans)
        else:
            print 'answer: "{0}" is not recognized'.format(ans)

except (KeyboardInterrupt, SystemExit):
    sys.exit()

