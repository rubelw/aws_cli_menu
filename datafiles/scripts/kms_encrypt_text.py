#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('kms')

    response = client.list_aliases(
        Limit=99
    )

    #print(pretty(response))
    new_aliases = {}
    if 'Aliases' in response:
        aliases = response.get('Aliases')

        for a in aliases:
            #print(pretty(a))
            if 'AliasName' in a:
                #print('There is an alias name in dict')
                new_aliases[str(a['TargetKeyId'])] = []
                new_aliases[str(a['TargetKeyId'])].append(a['AliasName'])
                new_aliases[str(a['TargetKeyId'])].append(a['AliasArn'])



    #print(pretty(new_aliases))
    response = client.list_keys(
        Limit=99,
    )

    #print(pretty(response))


    if 'Keys' in response:
        keys = response.get('Keys')
        menu = {}
        counter=0

        for k in keys:
            counter += 1
            my_list = []
            my_list.append(k['KeyArn'])
            my_list.append(k['KeyId'])
            if str(k['KeyId']) in new_aliases:
                my_list.append(new_aliases[str(k['KeyId'])][0])
                my_list.append(new_aliases[str(k['KeyId'])][1])
            else:
                my_list.append('None')
                my_list.append('None')
            my_list.append(k)
            menu[counter] = my_list

        if len(menu) > 0:
            print "\n"
            print '#########################################'
            print '## Select KeyArn                       ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][2]+" "+menu[key][1]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][4]
                        break


        #print(pretty(info))

        text = raw_input("Enter text to encrypt: [ENTER](Cntrl-C to exit)")

        response = client.encrypt(
            KeyId=info['KeyId'],
            Plaintext=bytearray(text)
        )

        print(pretty(response))


        print('ciphertext_blob: '+str(base64.b64encode(response['CiphertextBlob'])))





except (KeyboardInterrupt, SystemExit):
    sys.exit()
