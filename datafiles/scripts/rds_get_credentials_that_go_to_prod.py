#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json


need_to_add_to_prod = {}
dev = {}
test = {}
acc = {}
supp= {}
prod={}
profiles = {}

databases = {'phidatadev':'dev', 'phidatatest':'test','phidatasupp':'supp','phidatacc':'acc'}


def get_data():

    for db in databases:
        creds = profiles[str(db)][0]

        conn_string = 'dbname=\''+str(creds['database'])+'\' user=\''+str(creds['user'])+'\' host=\'localhost\' port=\''+str(creds['port'])+'\' password=\''+str(creds['password'])+'\''

        print('conn_string is:'+str(conn_string))

        try:
            conn=psycopg2.connect(conn_string)
            print('Connected to database')

            cur = conn.cursor()
            cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

            rows = cur.fetchall()

            for row in rows:
                #print "   ", row[0],row[3],row[4],row[5],row[6],row[7]

		if databases[db] == 'supp':
                    supp[str(row[0])]=row
		elif databases[db] == 'test':
                    test[str(row[0])]=row
                elif databases[db] == 'dev':
                    dev[str(row[0])]=row
                elif databases[db] == 'acc':
                    acc[str(row[0])]=row

                if row[0] not in prod:
               
                    print('##########################')
                    print(str(databases[db])+' role: '+str(row[0]+' does not exists in production'))
                    print('##########################')
                    need_to_add_to_prod[str(databases[db])+' '+str(row[0])] = 1


            cur.close()
            conn.close()


            print(pretty(need_to_add_to_prod))

        except:
            print('Could not connect to database')




try:

    old_profiles = get_rds_profiles()

    for itm in range(len(old_profiles)):

        temp_profile = old_profiles[int(itm)+1]
        key = temp_profile.keys()[0]

        data = []
        data.append(temp_profile[key][0])

	profiles[key]=data

    creds = profiles['phidataprod'][0]

    conn_string = 'dbname=\''+str(creds['database'])+'\' user=\''+str(creds['user'])+'\' host=\'localhost\' port=\''+str(creds['port'])+'\' password=\''+str(creds['password'])+'\''

    print('conn_string is:'+str(conn_string))


    try:
        conn=psycopg2.connect(conn_string)
        print('Connected to database')

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()


        for row in rows:
            print "   ", row[0],row[3],row[4],row[5],row[6],row[7]
            prod[str(row[0])] = row

        cur.close()
        conn.close()

    except:
        print('Could not connect to database')

    get_data()

    print('## Need to add to production ##')
    print(pretty(need_to_add_to_prod))


    #print('### Production ###')
    #print(pretty(prod))
    #print('### Support ###')
    #print(pretty(supp))
    #print('### Test ###')
    #print(pretty(test))
    #print('### Acc ###')
    #print(pretty(acc))
    #print('### Dev ###')
    #print(pretty(dev))




except (KeyboardInterrupt, SystemExit):
    sys.exit()
