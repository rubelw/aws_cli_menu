#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import re
import datetime
import decimal


EC2_TYPES = [
    't1.micro','t2.nano','t2.micro','t2.small','t2.medium','t2.large',
    'm1.small','m1.medium','m1.large','m1.xlarge','m3.medium','m3.large',
    'm3.xlarge','m3.2xlarge','m4.large','m4.xlarge','m4.2xlarge','m4.4xlarge',
    'm4.10xlarge','m4.16xlarge','m2.xlarge','m2.2xlarge','m2.4xlarge','cr1.8xlarge',
    'r3.large','r3.xlarge','r3.2xlarge','r3.4xlarge','r3.8xlarge','x1.16xlarge','x1.32xlarge',
    'i2.xlarge','i2.2xlarge','i2.4xlarge','i2.8xlarge','hi1.4xlarge','hs1.8xlarge','c1.medium',
    'c1.xlarge','c3.large','c3.xlarge','c3.2xlarge','c3.4xlarge','c3.8xlarge',
    'c4.large','c4.xlarge','c4.2xlarge','c4.4xlarge','c4.8xlarge','cc1.4xlarge','cc2.8xlarge','g2.2xlarge',
    'g2.8xlarge','cg1.4xlarge','p2.xlarge','p2.8xlarge','p2.16xlarge','d2.xlarge','d2.2xlarge','d2.4xlarge','d2.8xlarge'
]


def lowest_price(some_dict):
    positions = [] # output variable
    min_value = float("inf")
    for k, v in some_dict.items():
        if v == min_value:
            positions.append(k)
        if v < min_value:
            min_value = v
            positions = [] # output variable
            positions.append(k)

    return positions


try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('ec2')


    menu = {}
    counter =0
    for item in sorted(EC2_TYPES):
            counter += 1
            my_list = []
            my_list.append(str(item))
            menu[counter] = my_list

    print(pretty(menu))
    print "\n"
    print '#########################################'
    print '## Select Instance Type                ##'
    print '#########################################'
    for key in sorted(menu):
        print str(key) + ":" + str(menu[key][0])

    pattern = r'^[0-9]+$'
    while True:
        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                answer = menu[int(ans)][0]
                break

    start_time = datetime.datetime.now()-datetime.timedelta(minutes=1)
    end_time = datetime.datetime.now()

    response = client.describe_spot_price_history(
    InstanceTypes=[
        str(answer)
    ],
        DryRun=False,
        StartTime=start_time,
        EndTime=end_time
    )

    if 'SpotPriceHistory' in response:
        stacks = response.get('SpotPriceHistory')

        if len(stacks) > 0:

            price = {}
            for s in stacks:

                if s['AvailabilityZone'] in price:
                    if decimal.Decimal(s['SpotPrice']) < decimal.Decimal(price[s['AvailabilityZone']]):
                        price[s['AvailabilityZone']] = decimal.Decimal(s['SpotPrice'])
                else:
                    price[s['AvailabilityZone']]= decimal.Decimal(s['SpotPrice'])

            print '#########################################'
            print '## Spot History #'
            print '#########################################'
            print(pretty(price))


            print('lowest price is in: '+lowest_price(price))

        else:
            print("\n")
            print("##########################")
            print('No Spot Price History')
            print("#########################")
    else:
        print("\n")
        print("###########################")
        print('No Spot Price History')
        print("###########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
