#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import collections
import json
import time
import os

try:

    profile_name = get_profile_name()
    print("\n")

    file_path = raw_input("Enter file path (including file name): [ENTER](Cntrl-C to exit)")
    file_path = '/home/rubelw/projects/dsi-ecs/new/ecs-host/cloudformation/deploy.py'
    file_name = os.path.basename(file_path)

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('s3')

    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1

            menu[counter] = s['Name']

        print "\n\n"
        print '#########################################'
        print '## Select Bucket                       ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    bucket_name = menu[int(ans)]
                    break

        
        response = client.upload_file(file_path, bucket_name,file_name)
        print(response)




except (KeyboardInterrupt, SystemExit):
    sys.exit()

