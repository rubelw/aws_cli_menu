#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


DEBUG=0

if (DEBUG):
    boto3.set_stream_logger(name='botocore')


try:

    profile_name = get_profile_name()
    print('profile_name: '+profile_name)

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('apigateway')

    response = client.get_usage_plans(
        limit=99
    )
    print(pretty(response))
    stacks = response.get('items')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:

            stages = s['apiStages']

            if len(stages)<1:
                print(pretty(s))
                print("lenght: "+str(len(stages)))

                print('id: '+str(s['id']))
                response = client.delete_usage_plan(
                    usagePlanId=str(s['id'])
                )

                print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
