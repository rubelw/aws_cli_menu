#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    instance_name = raw_input("Enter instance name to find: [ENTER]")





    client = session.client('ec2')

    response = client.describe_instances(

        DryRun=False,
        Filters= [
            {
                'Name': 'tag-key',
                'Values': ['Name']
            },
            {
                'Name':'tag-value',
                'Values':[str(instance_name)]
            }
        ]
    )

    print("\n")
    print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
