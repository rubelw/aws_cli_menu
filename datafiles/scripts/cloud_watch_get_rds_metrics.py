#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
from datetime import datetime, timedelta

try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('rds')

    response = client.describe_db_instances()

    if 'DBInstances' in response:
        stacks = response.get('DBInstances')

        if len(stacks) > 0:
            menu = {}
            counter=0
            for s in stacks:
                print(pretty(s))
                counter+=1
                my_list = []
                my_list.append(s['Endpoint']['Address'])
                my_list.append(s['DBInstanceIdentifier'])
                my_list.append(s)
                menu[counter]=my_list


            print('#######################')
            print('Select Database')
            print('#######################')
            for key in sorted(menu):
                print str(key)+": " + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][2]
                        break

            print("\n")
            print("###########################")
            print("Detailed Database Info")
            print("###########################")
            print(pretty(info))

            db_identifier = info['DBInstanceIdentifier']

            print('db identifier: '+str(db_identifier))
            cw_client = session.client('cloudwatch')

            # today + 1 because we want all of today
            #today = datetime.now() + timedelta(days=1)
            today = datetime.now()
            print('today: '+str(today))

            two_weeks = timedelta(days=1)
            #start_date = today - two_weeks
            start_date = datetime.now() - timedelta(seconds=120)

            response = cw_client.get_metric_statistics(
                Namespace='AWS/RDS',
                MetricName='CPUUtilization',
                Dimensions=[
                    {
                        'Name': 'DBInstanceIdentifier',
                        'Value': str(db_identifier)
                    },
                ],
                StartTime=start_date,
                EndTime=today,
                Period=60,
                Statistics=['Average']

            )

            print(pretty(response))
        else:
            print("\n")
            print("####################")
            print('No RDS Instances')
            print("####################")
    else:
        print("\n")
        print("#####################")
        print('No RDS Instances')
        print("#####################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()
