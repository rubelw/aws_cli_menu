#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


import sys

try:

    prefixes = [
        '/aws/billing',
        '/aws/ebs',
        '/aws/ec2',
        '/aws/elb',
        '/aws/rds',
        '/aws/s3',
        '/aws/sqs',
        '/aws/lambda/',
        '/aws/apigateway/'
    ]



    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('logs')

    menu = {}
    counter = 0
    for i in sorted(prefixes):
        counter +=1
        menu[counter] = i

    print "\n"
    print '#######################'
    print '## Select Category   ##'
    print '#######################'
    for key in sorted(menu):
        print str(key)+": " + str(menu[key])

    pattern = r'^[0-9]+$'
    while True:
        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                namespace = menu[int(ans)]
                break

    print(client.can_paginate('describe_log_groups'))


    menu = {}
    counter = 0

    paginator = client.get_paginator('describe_log_groups')
    page_iterator = paginator.paginate(logGroupNamePrefix=str(namespace))
    for page in page_iterator:
        #print(page['logGroups'])
        if len(page['logGroups'])>0:
            for g in page['logGroups']:
                counter+=1
                #print(g['logGroupName'])
                #log_groups.append(g['logGroupName'])
                menu[counter]=g['logGroupName']

    print "\n"
    print '#######################'
    print '## Select Log Group   ##'
    print '#######################'
    for key in sorted(menu):
        print str(key)+": " + str(menu[key])


    pattern = r'^[0-9]+$'
    while True:
        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                log_group_name = menu[int(ans)]
                break

    response = client.describe_log_groups(
        logGroupNamePrefix=str(log_group_name),
        #nextToken='string',
        #limit=123
    )

    print(response)

    response = client.delete_log_group(
        logGroupName=str(log_group_name)
    )

    print(response)

except (KeyboardInterrupt, SystemExit):
    sys.exit()
