#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re
import subprocess


try:


    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''


    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)

    rows = cur.fetchall()

    users = {}


    for row in rows:
        users[row[0]]=row

    cur.close()
    conn.close()

    menu = {}

    counter = 0

    for i in sorted(users):
        counter +=1
        menu[counter]=users[i]

    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select User")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    user_name = menu[int(ans)][0]
                    break


        cmd = 'openssl rand -base64 32'
        new_pwd = subprocess.check_output(cmd,shell=True)


        conn=psycopg2.connect(conn_string)
        #print('Connected to database')

        cur = conn.cursor()


        try:
            cur.execute('alter role '+str(user_name)+' with password \''+str(new_pwd)+'\';')

        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)


        conn.commit()

        print("\n")
        print("##############################")
        print("User new password is: "+str(new_pwd))
        print("##############################")


    else:
        print("\n")
        print("####################")
        print('No users found')
        print("####################")









except (KeyboardInterrupt, SystemExit):
    sys.exit()