#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('codepipeline')

    response = client.list_pipelines()
    print(pretty(response))
    stacks = response.get('pipelines')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['version'])
            my_list.append(s['name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#############################'
        print '## Select Pipeline         ##'
        print '#############################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][1]+' version: '+str(menu[key][0]))

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    name = menu[int(ans)][1]
                    break

        response = client.get_pipeline(
            name= str(name)
        )


        pipelines = response.get('pipeline')

        if len(pipelines) > 0:

            stages = pipelines['stages']

            if len(stages) > 0:

                menu = {}
                counter = 0
                for s in stages:
                    counter += 1
                    my_list = []
                    my_list.append(s['name'])
                    my_list.append(s)
                    menu[counter] = my_list

                print "\n\n"
                print '#############################'
                print '## Select Stage         ##'
                print '#############################'
                for key in sorted(menu):
                    print str(key) + ": " + str(menu[key][0])

                pattern = r'^[0-9]+$'
                while True:
                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            info = menu[int(ans)][1]
                            break

                print(pretty(info))


                category = info['actions'][0]['actionTypeId']['category']
                owner = info['actions'][0]['actionTypeId']['owner']
                version = info['actions'][0]['actionTypeId']['version']
                provider= info['actions'][0]['actionTypeId']['provider']

                print('category: '+str(category))
                print('owner: '+str(owner))
                print('version: '+str(version))
                print('provider: '+str(provider))

                response = client.poll_for_jobs(
                    actionTypeId={
                        'category': str(category),
                        'owner': 'ThirdParty',
                        'provider': str(provider),
                        'version': str(version)
                    },
                    maxBatchSize=75,
                    #queryParam={
                    #    'string': 'string'
                    #}
                )

                print(pretty(response))

                print("\n")
                print("########################")
                print("Detailed Pipeline Info")
                print("########################")
                print(pretty(name))


            else:
                print("\n")
                print("#######################")
                print('No Pipeline sTAGES found')
                print("#######################")


    else:
        print("\n")
        print("#######################")
        print('No Pipelines found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
