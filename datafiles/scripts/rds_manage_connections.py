#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re

try:
    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''


    connections = {}

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("select * FROM pg_stat_activity;")


        column_names = []
        data_rows = []

        #rows = cur.fetchall()
        column_names = [desc[0] for desc in cur.description]
        for row in cur:
            data_rows.append(row)




        main_counter = 0
        for item2 in data_rows:

            counter=0
            for item in item2:


                if main_counter not in connections.keys():
                    print(str(main_counter)+'not in connections keys')
                    connections[main_counter]={}
                    connections[main_counter][column_names[counter]]=item
                    counter+=1
                else:
                    print(str(main_counter)+' in connections keys')
                    connections[main_counter][column_names[counter]]=item
                    counter+=1

            main_counter+=1


        print(pretty(connections))


    except psycopg2.OperationalError as e:
        print('Error: \n{0}').format(e)
        sys.exit(1)


except (KeyboardInterrupt, SystemExit):
    sys.exit()