#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('autoscaling')

    response = client.describe_auto_scaling_groups()

    if 'AutoScalingGroups' in response:
        stacks = response.get('AutoScalingGroups')

        if len(stacks) > 0:


            menu = {}
            counter = 0

            for item in stacks:
                counter += 1
                list = []
                list.append(item['AutoScalingGroupName'])
                list.append(item)
                menu[counter] = list

            print("\n")
            print('#######################')
            print('Tables')
            print('#######################')
            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        asg = menu[int(ans)][1]
                        break

            print"\n"
            print(pretty(asg))


        else:
            print("\n")
            print("#########################")
            print('No AutoScalingGroups')
            print("#########################")
    else:
        print("\n")
        print("##########################")
        print('No AutoScalingGroups')
        print("##########################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()
