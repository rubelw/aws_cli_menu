#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('lambda')


    response = client.list_functions()
    stacks = response.get('Functions')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            counter += 1
            menu[counter] = s['FunctionName']

        print "\n\n"
        print '#########################################'
        print '## Select Function                     ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    function_name = menu[int(ans)]
                    break

        print('function name: '+str(function_name))
        response = client.list_aliases(
            FunctionName=function_name
        )
        if 'Aliases' in response:
            aliases = response['Aliases']
            if len(aliases)>0:
                for a in aliases:
                    print(pretty(a))
            else:
                print('######################################')
                print('There are no aliases for this function')
                print('######################################')



    else:
        print("\n")
        print("#########################")
        print('No Functions found')
        print("#########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
