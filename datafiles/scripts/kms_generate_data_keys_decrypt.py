#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64
from Crypto.Cipher import AES

pad = lambda s: s + (32 - len(s) % 32) * ' '

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('kms')


    data = raw_input("Enter text/data to decrypt: [ENTER](Cntrl-C to exit)")
    cipher = raw_input("Enter ciphertext blob: [ENTER](Cntrl-C to exit)")
    print(pretty(cipher))
    response2 = client.decrypt(
        CiphertextBlob=base64.b64decode(cipher)
    )
    crypter = AES.new(response2.get('Plaintext'))
    print(crypter.decrypt(base64.b64decode(data)).rstrip())



except (KeyboardInterrupt, SystemExit):
    sys.exit()
