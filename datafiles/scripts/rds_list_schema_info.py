#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re

try:
    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("select datname from pg_database where datistemplate = false;")

        rows = cur.fetchall()
    except psycopg2.OperationalError as e:
        print('Error: \n{0}').format(e)
        sys.exit(1)

    schemas = {}


    for row in rows:
        print(row)
        schemas[row[0]]=row

    cur.close()
    conn.close()

    menu = {}

    counter = 0

    for i in sorted(schemas):
        counter +=1
        menu[counter]=schemas[i]


    print(pretty(menu))
    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select Database")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    database_name = menu[int(ans)][0]
                    break

        print('database name: '+str(database_name))

        conn_string = 'dbname=\'' + str(database_name) + '\' user=\'' + str(
            creds[profile][0]['user']) + '\' host=\'localhost\' port=\'' + str(
            creds[profile][0]['port']) + '\' password=\'' + str(creds[profile][0]['password']) + '\''


        try:
            conn=psycopg2.connect(conn_string)

            cur = conn.cursor()
            cur.execute("select schema_name from information_schema.schemata;")

            rows = cur.fetchall()
        except psycopg2.OperationalError as e:
            print('Error: \n{0}').format(e)
            sys.exit(1)

        schemas = {}

        for row in rows:
            schemas[row[0]] = row

        cur.close()
        conn.close()

        menu = {}

        counter = 0

        for i in sorted(schemas):
            counter += 1
            menu[counter] = schemas[i]

        print(pretty(menu))
        if len(menu) > 0:

            print("\n")
            print("#################################")
            print("Select Schema")
            print("#################################")

            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        schema_name = menu[int(ans)][0]
                        break

            # print('schema name: '+str(schema_name))


            try:
                conn = psycopg2.connect(conn_string)

                cur = conn.cursor()
                cur.execute(
                    "SELECT n.nspname AS \"Name\", pg_catalog.pg_get_userbyid(n.nspowner) AS \"Owner\", pg_catalog.array_to_string(n.nspacl, E'\n') AS \"Access privileges\", pg_catalog.obj_description(n.oid, 'pg_namespace') AS \"Description\" FROM pg_catalog.pg_namespace n WHERE n.nspname !~ '^pg_' AND n.nspname <> 'information_schema' and n.nspname = \'" + str(
                        schema_name) + "\' ORDER BY 1")

                rows = cur.fetchall()

                permissions = rows[0][2].splitlines()

                print(pretty(permissions))




            except psycopg2.OperationalError as e:
                print('Error: \n{0}').format(e)
                sys.exit(1)

            cur.close()
            conn.close()

        else:
            print("\n")
            print("####################")
            print('No are no schemas')
            print("####################")


except (KeyboardInterrupt, SystemExit):
    sys.exit()