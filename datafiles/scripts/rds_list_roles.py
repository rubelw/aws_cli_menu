#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re

try:
    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit, r.rolcreaterole, r.rolcreatedb, r.rolcanlogin, r.rolconnlimit, r.rolvaliduntil, ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof, r.rolreplication, r.rolbypassrls FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()
    except psycopg2.OperationalError as e:
        print('Error: \n{0}').format(e)
        sys.exit(1)

    schemas = {}


    for row in rows:
        print(row)
        schemas[row[0]]=row

    cur.close()
    conn.close()

    menu = {}

    counter = 0

    for i in sorted(schemas):
        counter +=1
        menu[counter]=schemas[i]


    print(pretty(menu))
    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Roles")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]


    else:
        print('There are no databases')





except (KeyboardInterrupt, SystemExit):
    sys.exit()