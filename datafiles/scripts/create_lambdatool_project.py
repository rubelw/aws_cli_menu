#!/usr/bin/env python

import sys
import subprocess
import boto3.session
from aws_cli_menu_helper import *
import re

ACCOUNNTS = {
    "dsi-nonprod" : "np",
    "dsi-prod":"pd",
    "dsi-sandbox":"sb",
    "dsi-utility":"ut",
    "phigalileo":"ga"
}

try:
    STACKILITY_DIR = get_stackility_project_directory()
    #print STACKILITY_DIR

    DIRS = []
    for child in os.listdir(STACKILITY_DIR):
        path = os.path.join(STACKILITY_DIR, child)
        if os.path.isdir(path):
            DIRS.append(path)



    #print DIRS

    MENU = {}
    UNSORTED_MENU = {}

    COUNTER = 0

    for d in DIRS:
        list = []


        list.append(d)

        matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

        if matchObj:
            list.append(matchObj.group(1))
            UNSORTED_MENU[matchObj.group(1)]= list


    for key in sorted(UNSORTED_MENU):
        COUNTER+=1

        MENU[COUNTER] = UNSORTED_MENU[key]

    print "\n"
    print "####################################"
    print "Select Project"
    print "####################################"

    for key in sorted(MENU):
        print str(key) + ": " + MENU[key][1]

    while True:

        USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
        if int(USER_RESPONSE) in MENU:
            PROJECT = MENU[int(USER_RESPONSE)][0]
            break


    PROJECT_DIRS = []
    PROJECT_DIRECTORY = os.path.join(STACKILITY_DIR,PROJECT)
    for child in os.listdir(PROJECT_DIRECTORY):
        path = os.path.join(PROJECT_DIRECTORY, child)
        if os.path.isdir(path):
            PROJECT_DIRS.append(path)

    MENU = {}
    UNSORTED_MENU = {}

    COUNTER = 0

    steps_flag = -1

    for d in PROJECT_DIRS:
        list = []

        list.append(d)

        matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

        if matchObj:
            list.append(matchObj.group(1))

            if 'step' in matchObj.group(1):
                print('flag turned on')
                steps_flag = 1

            UNSORTED_MENU[matchObj.group(1)] = list



    if steps_flag <0:
        for key in sorted(UNSORTED_MENU):
            COUNTER += 1

            MENU[COUNTER]= UNSORTED_MENU[key]
    else:
        COUNTER=40
        for key in sorted(UNSORTED_MENU):
            print(key)
            matchObj = re.match(r'.*step([0-9]+).*', key, re.M | re.I)

            if matchObj:
                print('match')
                print(matchObj.group(1))
                MENU[int(matchObj.group(1))] = UNSORTED_MENU[key]
            else:
                MENU[COUNTER] = UNSORTED_MENU[key]
                COUNTER += 1


    print "\n"
    print "####################################"
    print "Select Stack"
    print "####################################"

    for key in sorted(MENU):
        print str(key) + ": " + MENU[key][1]

    while True:

        USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
        if int(USER_RESPONSE) in MENU:
            STACK = MENU[int(USER_RESPONSE)][0]
            break



    if os.path.exists(STACK+'/config'):

        INI_FILES = []
        for file in os.listdir(STACK+'/config'):
            if file.endswith(".ini"):
                ifile = os.path.join(STACK, 'config/'+file)
                #print ifile
                INI_FILES.append(os.path.join(ifile))

        MENU = {}

        COUNTER = 0

        for f in INI_FILES:
            #print(f)
            list = []
            COUNTER += 1
            list.append(f)

            matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_\.]+)$', f, re.M | re.I)

            if matchObj:
                #print('found match')
                list.append(matchObj.group(1))

            MENU[COUNTER] = list

        print "\n"
        print "####################################"
        print "Select Ini File"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                INI_FILE = MENU[int(USER_RESPONSE)][0]
                break

    # Has subprojects
    else:

        SUBPROJECT_DIRS = []

        for child in os.listdir(STACK):
            path = os.path.join(STACK, child)
            if os.path.isdir(path):
                SUBPROJECT_DIRS.append(path)

        MENU = {}
        UNSORTED_MENU = {}

        COUNTER = 0

        for d in SUBPROJECT_DIRS:
            list = []

            list.append(d)

            matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

            if matchObj:
                list.append(matchObj.group(1))

                UNSORTED_MENU[matchObj.group(1)] = list

        for key in sorted(UNSORTED_MENU):
            COUNTER += 1

            MENU[COUNTER] = UNSORTED_MENU[key]




        print "\n"
        print "####################################"
        print "Select Stack"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                STACK = MENU[int(USER_RESPONSE)][0]
                break

        INI_FILES = []
        for file in os.listdir(STACK + '/config'):
            if file.endswith(".ini"):
                ifile = os.path.join(STACK, 'config/' + file)
                # print ifile
                INI_FILES.append(os.path.join(ifile))

        MENU = {}

        COUNTER = 0

        for f in INI_FILES:
            # print(f)
            list = []
            COUNTER += 1
            list.append(f)

            matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_\.]+)$', f, re.M | re.I)

            if matchObj:
                # print('found match')
                list.append(matchObj.group(1))

            MENU[COUNTER] = list

        print "\n"
        print "####################################"
        print "Select Ini File"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                INI_FILE = MENU[int(USER_RESPONSE)][0]
                break




    #print 'ini file: ' + str(INI_FILE)

    #PROFILE_NAME = get_profile_name()
    #print "\n\n"

    #REGION = select_region()

    #cmd = "stackility upsert --ini "+str(ini_file)+" --region "+str(region)+" --profile "+str(profile_name)

    if determine_if_opsworks_stack(INI_FILE, STACK):


        DIRS = []
        for child in os.listdir(STACK):
            path = os.path.join(STACK, child)
            if os.path.isdir(path):
                DIRS.append(path)

        # print DIRS

        MENU = {}
        UNSORTED_MENU = {}

        COUNTER = 0

        for d in DIRS:
            list = []

            list.append(d)

            matchObj = re.match(r'.*[!/]([a-zA-Z0-9-_]+)$', d, re.M | re.I)

            if matchObj:
                list.append(matchObj.group(1))
                UNSORTED_MENU[matchObj.group(1)] = list

        for key in sorted(UNSORTED_MENU):
            COUNTER += 1

            MENU[COUNTER] = UNSORTED_MENU[key]

        print "\n"
        print "####################################"
        print "Select Recipe Directory"
        print "####################################"

        for key in sorted(MENU):
            print str(key) + ": " + MENU[key][1]

        while True:

            USER_RESPONSE = raw_input("Make a selection [ENTER] (Cntrl-C to exit):")
            if int(USER_RESPONSE) in MENU:
                recipe_dir = MENU[int(USER_RESPONSE)][0]
                break

        recipe_param = get_package_url_from_ini(INI_FILE)

        if yes_or_no('Is this a dryrun?'):
            CMD = "stackility upsert -d --recipe_dir "+str(recipe_dir)+ " --recipe_url_parameter "+str(recipe_param)+ " --output_yaml True --project_dir " + str(STACK) + "/ --ini " + str(INI_FILE)
        else:
            CMD = "stackility upsert --recipe_dir "+str(recipe_dir)+ " --recipe_url_parameter "+str(recipe_param)+ " --output_yaml True --project_dir " + str(STACK) + "/ --ini " + str(INI_FILE)


    else:
        if yes_or_no('Is this a dryrun?'):
            CMD = "stackility upsert -d --output_yaml True --project_dir " + str(STACK) + "/ --ini " + str(INI_FILE)
        else:
            CMD = "stackility upsert --output_yaml True --project_dir " + str(STACK) + "/ --ini " + str(INI_FILE)

    print CMD

    ## run it ##

    STACKS = []
    PROCESS = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        out = PROCESS.stdout.read(1)
        if out == '' and PROCESS.poll() != None:
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()



except (KeyboardInterrupt, SystemExit):
    sys.exit()
