#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import datetime




try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    menu = get_iam_users(session)

    if len(menu) > 0:

        print("\n")
        print("#################################")
        print("Select User")
        print("#################################")

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    user_name = menu[int(ans)][0]
                    break

        client = session.client('iam')
        response = client.get_user(UserName=str(user_name))
        user_info = response.get('User')

        print "\n\n"
        print '#############################'
        print '## User Info               ##'
        print '#############################'

        print(pretty(user_info))

        username = user_info['UserName']


        response = client.list_access_keys(
            UserName=str(username),
            MaxItems=25
        )

        print(pretty(response))

        if 'AccessKeyMetadata' in response:
            keys = response['AccessKeyMetadata']

            for k in keys:
                print(k['CreateDate'])

                key_id = k['AccessKeyId']

                lt_datetime = datetime.datetime.strptime(str(k['CreateDate']), '%Y-%m-%d %H:%M:%S+00:00')

                lt_delta = datetime.datetime.utcnow() - lt_datetime

                print(lt_delta.days)

                if  int(lt_delta.days) > 82:


                    raw_input("Press Enter to create new key...")

                    response = client.create_access_key(
                        UserName=str(username)
                    )

                    print(pretty(response))

                    raw_input("Press Enter to delete old key...")

                    response = client.delete_access_key(
                        UserName=str(username),
                        AccessKeyId=str(key_id)
                    )

                    print(pretty(response))


        else:
            print("####################")
            print('No keys found')
            print("####################")



    else:
        print("\n")
        print("####################")
        print('No users found')
        print("####################")



except (KeyboardInterrupt, SystemExit):
    sys.exit()