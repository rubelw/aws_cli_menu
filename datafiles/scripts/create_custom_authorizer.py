#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('apigateway')
    response = client.get_rest_apis()

    #print(pretty(response))
    stacks = response.get('items')


    if len(stacks)>0:

        menu = {}
        counter=0

        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['name'])
            if 'description' in s:
                my_list.append(s['description'])
            else:
                my_list.append('No description')
            my_list.append(s)
            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select API'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][2]
                    break

        print(pretty(info))

        if 'id' in info:

            api_id = info.get('id')

            authorizer_name = raw_input("Enter authorizer name: [ENTER](Cntrl-C to exit)")

            #print('api id: '+str(api_id))


            # Get the authorizers arn
            lambda_client = session.client('lambda')


            response = lambda_client.list_functions()
            stacks = response.get('Functions')

            if len(stacks) > 0:

                menu = {}
                counter = 0
                for s in stacks:
                    counter += 1
                    my_list = []
                    my_list.append(s['FunctionName'])
                    my_list.append(s)
                    #menu[counter] = s['FunctionName']
                    menu[counter] = my_list

                print "\n\n"
                print '#########################################'
                print '## Select Function                     ##'
                print '#########################################'
                for key in sorted(menu):
                    #print str(key) + ":" + menu[key]
                    print str(key)+":" + str(menu[key][0])

                #pattern = r'^[0-9]+$'
                #while True:
                #    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                #    if re.match(pattern, ans) is not None:
                #        if int(ans) in menu:
                #            function_name = menu[int(ans)[0]]
                #            info = menu[int(ans)][1]
                #            break
                pattern = r'^[0-9]+$'
                while True:

                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            info = menu[int(ans)][1]
                            break

                print(pretty(info))
                lambda_arn = info['FunctionArn']
                function_name = info['FunctionName']
                print('lambda arn: '+str(lambda_arn))
                print('function_name: '+str(function_name))


                matchObj = re.match( r'^.*:(\d+):function.*$', lambda_arn, re.M|re.I)



                if matchObj:
                   account_id =  str(matchObj.group(1))


                   response = client.create_authorizer(
                        restApiId=str(api_id),
                        name=str(authorizer_name),
                        type='TOKEN',
                        #providerARNs=[
                        #    str(lambda_arn)
                        #],
                        authType='CUSTOM',
                        authorizerUri = 'arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:'+str(account_id)+':function:'+str(function_name)+'/invocations',
                        authorizerCredentials='arn:aws:iam::434834166239:role/compass-jenkins',
                        identitySource='method.request.header.Authorization',
                        #identityValidationExpression='string',
                        authorizerResultTtlInSeconds=60
                   )


                   print(pretty(response))

                else:
                    print('could not find the account number')

        else:
            print(pretty(info))
            print('id does not exists')
            sys.exit(-1)



    else:
        print("\n")
        print("##########################")
        print('No APIs Found')
        print("##########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
