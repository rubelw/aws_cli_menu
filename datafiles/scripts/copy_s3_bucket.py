#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import subprocess
import os

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy From   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break


        from_bucket_name = info['Name']


    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:

            counter+=1
            my_list = []
            my_list.append(s['Name'])
            my_list.append(s)
            menu[counter]=my_list

        print "\n\n"
        print '#####################################'
        print '## Select S3 Bucket To Copy To   ##'
        print '#####################################'
        for key in sorted(menu):
            print str(key)+": " + str(menu[key][0])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][1]
                    break

        to_bucket_name = info['Name']


        print('from bucket: '+str(from_bucket_name))
        print('to bucket: '+str(to_bucket_name))

        os.environ["AWS_DEFAULT_PROFILE"]=str(profile_name)
        #cmd = 'aws s3 cp s3://'+str(from_bucket_name)+' s3://'+str(to_bucket_name)+' --recursive'
        #push=subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE)
        #print push.returncode

        CMD = 'aws s3 cp s3://'+str(from_bucket_name)+' s3://'+str(to_bucket_name)+' --recursive'

        print CMD

        ## run it ##

        STACKS = []
        PROCESS = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        while True:
            out = PROCESS.stdout.read(1)
            if out == '' and PROCESS.poll() != None:
                break
            if out != '':
                sys.stdout.write(out)
                sys.stdout.flush()

    else:
        print("\n")
        print("#######################")
        print('No S3 buckets found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
