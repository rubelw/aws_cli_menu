#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('opsworks')
    response = client.describe_stacks()
    stacks = response.get('Stacks')

    if len(stacks) > 0:

        menu = {}
        counter=0
        for s in stacks:

            response = client.describe_instances(
                StackId=str(s['StackId'])
            )

            print(pretty(response))



    else:
        print("\n")
        print("#######################")
        print('No stacks found')
        print("#######################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
