#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import re
import time


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('dynamodb')

    response = client.list_tables()
    stacks = response.get('TableNames')

    if len(stacks) > 0:

        menu = {}

        counter = 0
        for item in stacks:
            counter += 1
            menu[counter] = item

        print("\n")
        print('#######################')
        print('Tables')
        print('#######################')
        for key in sorted(menu):
            print str(key) + ":" + menu[key]

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    table_name = menu[int(ans)]
                    break

        print"\n\n"

        read_units = raw_input("Enter read units: [ENTER](Cntrl-C to exit)")
        write_units = raw_input("Enter write units: [ENTER](Cntrl-C to exit)")


        response = client.update_table(
            #AttributeDefinitions=[
            #    {
            #        'AttributeName': 'string',
            #        'AttributeType': 'S' | 'N' | 'B'
            #    },
            #],
            TableName=str(table_name),
            ProvisionedThroughput={
                'ReadCapacityUnits': int(read_units),
                'WriteCapacityUnits': int(write_units)
            }
            #GlobalSecondaryIndexUpdates=[
            #    {
            #        'Update': {
            #            'IndexName': 'string',
            #            'ProvisionedThroughput': {
            #                'ReadCapacityUnits': 123,
            #                'WriteCapacityUnits': 123
            #            }
            #        },
            #        'Create': {
            #            'IndexName': 'string',
            #            'KeySchema': [
            #                {
            #                    'AttributeName': 'string',
            #                    'KeyType': 'HASH' | 'RANGE'
            #                },
            #            ],
            #            'Projection': {
            #                'ProjectionType': 'ALL' | 'KEYS_ONLY' | 'INCLUDE',
            #                'NonKeyAttributes': [
            #                    'string',
            #                ]
            #            },
            #            'ProvisionedThroughput': {
            #                'ReadCapacityUnits': 123,
            #                'WriteCapacityUnits': 123
            #            }
            #        },
            #        'Delete': {
            #            'IndexName': 'string'
            #        }
            #    },
            #],
            #StreamSpecification={
            #    'StreamEnabled': True | False,
            #    'StreamViewType': 'NEW_IMAGE' | 'OLD_IMAGE' | 'NEW_AND_OLD_IMAGES' | 'KEYS_ONLY'
            #}
        )

        print(pretty(response))

        while (1):
            print('Waiting for table to update...')
            time.sleep(15)

            response = client.describe_table(
                TableName=str(table_name)
            )

            if response['Table']['TableStatus'] == 'ACTIVE':
                break

        print("Read and write units have been changed")

    else:
        print('No tables')

except (KeyboardInterrupt, SystemExit):
    sys.exit()
