#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
from os.path import expanduser
import gzip
import botocore
import subprocess
import time
from datetime import datetime


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('s3')
    response = client.list_buckets()
    stacks = response.get('Buckets')

    unsorted = {}
    buckets = []
    if len(stacks) > 0:

        menu = {}
        counter = 0
        s3 = session.resource('s3')

        for s in stacks:
            counter += 1
            menu[counter] = s['Name']
            buckets.append(s['Name'])

        s3 = session.resource('s3')

        for bucket in buckets:
            my_bucket = s3.Bucket(bucket)

            for file in my_bucket.objects.filter():
                print(file)

                #obj = s3.Object(bucket_name=my_bucket, key=file)
                my_date = str(file.last_modified)
                new_date = str(my_date[0:19])
                print(new_date)
                modified = datetime.strptime(new_date, "%Y-%m-%d %H:%M:%S")
                print(modified)
                # convert to datetime
                #dt = datetime.fromtimestamp(mktime(modified))

                if str(file.key).endswith("Manifest.json"):
                    print('skipping manifest file')
                else:
                    unsorted[file]=modified

        my_sorted = sorted(unsorted.items(), key=lambda p: p[1], reverse=True)

        print(pretty(my_sorted))


        my_sorted_keys = [item[0] for item in sorted(unsorted.items(), key=lambda val: val[1])]

        print(pretty(my_sorted_keys))

        latest_file = my_sorted_keys[-1]

        print(latest_file.key)
        print(latest_file.bucket_name)



        print(latest_file)

        try:
            home = expanduser("~")
            s3.Bucket(str(latest_file.bucket_name)).download_file(str(latest_file.key), str(home)+'/Downloads/billings.csv.gz')

            print('uncompress file')
            ##p = subprocess.Popen('cd '+str(home)+'/Downloads && ls', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            p = subprocess.Popen('cd '+str(home)+'/Downloads && gunzip -f billings.csv.gz', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            for line in p.stdout.readlines():
                print line,
            retval = p.wait()


            print('scp file')
            p = subprocess.Popen("scp "+str(home)+'/Downloads/billings.csv logstash:/tmp/test.csv', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for line in p.stdout.readlines():
                print line,
            retval = p.wait()


            print('scp file')
            p = subprocess.Popen("cd "+str(home)+'/Downloads && rm billings.csv', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for line in p.stdout.readlines():
                print line,
            retval = p.wait()


        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

    else:
        print("\n")
        print("#########################")
        print('No S3 buckets found')
        print("#########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
