#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('iam')

    user_name = raw_input("Enter user name: [ENTER]")

    response = client.create_user(
        UserName=user_name
    )

    user_arn = response['User']['Arn']

    response = client.list_policies(
        Scope='Local'
    )

    stacks = response.get('Policies')

    if len(stacks) > 0:
        menu = {}
        fields = {}

        for s in stacks:
            list = []
            name = str(s.get('PolicyName'))
            arn = str(s.get('Arn'))
            list.append(name)
            list.append(arn)
            fields[name] = list

        counter = 0
        for item in sorted(fields):
            counter = counter + 1
            menu[counter] = fields[item]

        print("\n")
        print('#######################')
        print('Policies')
        print('#######################')

        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    arn = menu[int(ans)][1]
                    break

        print"\n\n"

        response = client.attach_user_policy(
            UserName=user_name,
            PolicyArn=arn
        )

        #print(pretty(response))

        client = session.client('opsworks')
        response = client.create_user_profile(
            IamUserArn=str(user_arn),
            AllowSelfManagement=True
        )


        response = client.describe_stacks()
        stacks = response.get('Stacks')

        if len(stacks) > 0:

            menu = {}
            counter = 0
            for s in stacks:
                counter += 1
                my_list = []
                my_list.append(s['Name'])
                my_list.append(s)
                menu[counter] = my_list

            print "\n\n"
            print '#############################'
            print '## Select Stack            ##'
            print '#############################'
            for key in sorted(menu):
                print str(key) + ": " + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][1]
                        break

            print("\n")
            print("########################")
            print("Detailed Stack Info")
            print("########################")
            #print(pretty(info))

            stack_id = info['StackId']


        print(response)

        response = client.set_permission(
            StackId=str(stack_id),
            IamUserArn=str(user_arn),
            AllowSsh=True,
            AllowSudo=False,
            Level='iam_only'

        )

        print(response)

        response = client.describe_permissions(
            IamUserArn=str(user_arn),
            StackId=str(stack_id)
        )

        print(response)


    else:
        print("\n")
        print("#########################")
        print('There are no policies')
        print("#########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
