#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import subprocess

try:

    cmd = "stackility --version"

    ## run it ##
    p = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE)

    ## But do not wait till netstat finish, start displaying output immediately ##
    while True:
        out = p.stderr.read(1)
        if out == '' and p.poll() != None:
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()

except (KeyboardInterrupt, SystemExit):
    sys.exit()
