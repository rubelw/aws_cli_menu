#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import datetime
from datetime import timedelta
from subprocess import Popen, PIPE, STDOUT

import re


try:

    profile_name = get_profile_name()
    print("\n\n")

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('logs')

    log_groups = []

    marker = None
    while True:
        if marker:
            response_iterator = client.describe_log_groups(
                nextToken=marker,
                limit=50
            )
        else:
            response_iterator = client.describe_log_groups(
                limit=10
            )

        for log_group in response_iterator['logGroups']:
            print(log_group['logGroupName'])
            log_groups.append(log_group['logGroupName'])

        try:
            marker = response_iterator['nextToken']
            print(marker)
        except KeyError:
            break

    #print(pretty(log_groups))
    menu = {}
    counter=0

    for lg in log_groups:
        print('lg: '+str(lg))
        counter+=1
        my_list=[]
        my_list.append(lg)
        menu[counter]=my_list

    if len(menu) > 0:
        print "\n"
        print '#########################################'
        print '## Select Log Group'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    log_group_name = menu[int(ans)][0]
                    break

        print("\n")
        #print(pretty(info))

    log_streams = []

    marker = None
    while True:
        if marker:
            response_iterator = client.describe_log_streams(
                logGroupName=log_group_name,
                nextToken=marker,
                limit=50
            )
        else:
            response_iterator = client.describe_log_streams(
                logGroupName=log_group_name,
                limit=10
            )
        print(pretty(response_iterator))
        for log_stream in response_iterator['logStreams']:
            print(log_stream)
            print(log_stream['logStreamName'])
            log_streams.append(log_stream['logStreamName'])

        try:
            marker = response_iterator['nextToken']
            print(marker)
        except KeyError:
            break

    #print(pretty(log_groups))
    menu = {}
    counter=0

    for ls in log_streams:
        print('ls: '+str(ls))
        counter+=1
        my_list=[]
        my_list.append(ls)
        menu[counter]=my_list

    if len(menu) > 0:
        print "\n"
        print '#########################################'
        print '## Select Log Stream'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    log_stream_name = menu[int(ans)][0]
                    break

        print("\n")
        #print(pretty(info))


    cmd = 'awslogs get '+str(log_group_name)+' '+str(log_stream_name[:10])+'.* --start=2h'
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    print output



except (KeyboardInterrupt, SystemExit):
    sys.exit()
