#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import os

try:


    stackility_dir = get_stackility_project_directory()
    print(stackility_dir)

    dirs = []
    for child in os.listdir(stackility_dir):
        path = os.path.join(stackility_dir, child)
        if os.path.isdir(path):
            dirs.append(path)

    print(dirs)

except (KeyboardInterrupt, SystemExit):
    sys.exit()
