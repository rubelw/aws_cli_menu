#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
from os.path import expanduser
import re
from dateutil.parser import *
import datetime
from collections import defaultdict

DEBUG =0

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


def get_all_account_profiles():
        home = expanduser("~")
        cred_file = home+'/.aws/credentials'

        lines = [line.rstrip('\n') for line in open(cred_file)]

        profiles = []
        for line in lines:
                matchObj = re.match( r'^\s*\[(.*)\]\s*$', line, re.M|re.I)
                if matchObj:

                    if matchObj.group(1) <> 'default':
                        profiles.append(matchObj.group(1))

        return profiles

try:

    profile_names = get_all_account_profiles()

    if DEBUG:
        print('profile names: '+str(profile_names))

    ec2info = defaultdict()


    unique_zones = {}
    hosted_zones = {}
    for p in profile_names:
        hosted_zones[str(p)]=[]

    print(pretty(hosted_zones))

    if len(profile_names) >0:
            for p in profile_names:

                print('Getting information for profile name: '+str(p))

                try:
                    session = boto3.session.Session(profile_name=p)


                    client = session.client('route53')


                    response = client.list_hosted_zones()
                    print(pretty(response))

                    if 'HostedZones' in response:

                        stacks = response.get('HostedZones')

                        if len(stacks) > 0:


                            for s in stacks:

                                hosted_zones[str(p)].append(str(s.get('Name')))
                                unique_zones[str(s.get('Name'))]=str(p)


                except Exception as e:
		            print(e)

    else:
        print('There are not any profiles')
        sys.exit()

    print(pretty(hosted_zones))

    for key in sorted(unique_zones):
        print "%s: %s" % (key, unique_zones[key])


    #print(pretty(unique_blocks))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
