#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re




try:
    creds = get_rds_profile_name()

    profile = creds.keys()[0]

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    try:
        conn=psycopg2.connect(conn_string)

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()

        users = {}


        for row in rows:
            users[row[0]]=row

        cur.close()
        conn.close()

        menu = {}

        counter = 0

        for i in sorted(users):
            counter +=1
            menu[counter]=users[i]



        if len(menu) > 0:

            print("\n")
            print("#################################")
            print("Select User")
            print("#################################")

            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        user_name = menu[int(ans)][0]
                        break


            menu2 = { 1: ['Add Role'], 2:['Delete Role']}


            print("\n")
            print("#################################")
            print("Select Modification Type")
            print("#################################")

            for key in sorted(menu2):
                print str(key) + ":" + menu2[key][0]

            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu2:
                        selection  = int(ans)
                        break


            if int(selection) == 1:
                roles_menu = {}

                counter = 0

                for i in sorted(users):
                    searchObj = re.search( r'_r[ow].*', i, re.M|re.I)

                    if searchObj:
                       counter+=1
                       roles_menu[counter] = users[i]


                if len(roles_menu) > 0:

                    print("\n")
                    print("#################################")
                    print("Select Role")
                    print("#################################")

                    for key in sorted(roles_menu):
                        print str(key) + ":" + roles_menu[key][0]


                    pattern = r'^[0-9]+$'
                    while True:
                        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in roles_menu:
                                role_name = roles_menu[int(ans)][0]
                                break

                    conn=psycopg2.connect(conn_string)

                    cur = conn.cursor()

                    # Need to check for a user
                    cur.execute("GRANT "+str(role_name)+' to '+str(user_name))
                    conn.commit()

                    print("\n")
                    print("##############################")
                    print("Role has been modified")
                    print("##############################")

            elif int(selection) ==2:
                users_roles =  users[str(user_name)][7]

                roles_menu = {}

                counter = 0

                for i in sorted(users_roles):
                   counter+=1
                   roles_menu[counter] = i


                print("\n")
                print("#################################")
                print("Select Role To Delete")
                print("#################################")

                for key in sorted(roles_menu):
                    print str(key) + ":" + roles_menu[key]


                pattern = r'^[0-9]+$'
                while True:
                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in roles_menu:
                            role_name = roles_menu[int(ans)]
                            break

                conn=psycopg2.connect(conn_string)

                cur = conn.cursor()

                # Need to check for a user
                cur.execute("REVOKE "+str(role_name)+' FROM '+str(user_name))
                conn.commit()

                print("\n")
                print("##############################")
                print("Role has been modified")
                print("##############################")

            else:
                print('I am not sure what we are doing')
                sys.exit(-1)

        else:
            print("\n")
            print("####################")
            print('No users/roles found')
            print("####################")


    except:
        print('Could not connect to database')


except (KeyboardInterrupt, SystemExit):
    sys.exit()