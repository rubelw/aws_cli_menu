#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('emr')

    marker = None

    while True:
        if marker:
            response_iterator = client.list_clusters(
                Marker=marker
            )
        else:
            response_iterator = client.list_clusters()
        for cluster in response_iterator['Clusters']:
            print(cluster['Name'])

        try:
            marker = response_iterator['Marker']
            print(marker)
        except KeyError:
            sys.exit()



except (KeyboardInterrupt, SystemExit):
    sys.exit()
