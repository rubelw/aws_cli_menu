#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml
import base64


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('efs')

    response = client.describe_file_systems(
    )

    if 'FileSystems' in response:
        if len(response['FileSystems']) >0:
            for f in response['FileSystems']:
                print(pretty(f))

    else:
        print('No file systems')



except (KeyboardInterrupt, SystemExit):
    sys.exit()
