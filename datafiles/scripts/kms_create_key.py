#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('kms')

    policy= {
  "Version": "2012-10-17",
  "Id": "DefaultKmsPolicy",
  "Statement": [
    {
      "Sid": "Enable IAM User Permissions",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::434834166239:root"
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": "AIDAI3HUN7OLEKY77YJMI"
      },
      "Action": [
        "kms:Create*",
        "kms:Describe*",
        "kms:Enable*",
        "kms:List*",
        "kms:Put*",
        "kms:Update*",
        "kms:Revoke*",
        "kms:Disable*",
        "kms:Get*",
        "kms:Delete*",
        "kms:ScheduleKeyDeletion",
        "kms:CancelKeyDeletion"
      ],
      "Resource": "*"
    },
    {
      "Sid": "Allow use of the key",
      "Effect": "Allow",
      "Principal": {
        "AWS": "AIDAJAC5JB5FA7LS273LQ"
      },
      "Action": [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ],
      "Resource": "*"
    },
    {
      "Sid": "Allow attachment of persistent resources",
      "Effect": "Allow",
      "Principal": {
        "AWS": "AIDAJAC5JB5FA7LS273LQ"
      },
      "Action": [
        "kms:CreateGrant",
        "kms:ListGrants",
        "kms:RevokeGrant"
      ],
      "Resource": "*",
      "Condition": {
        "Bool": {
          "kms:GrantIsForAWSResource": "true"
        }
      }
    },
    {
      "Sid": "Allow decrypting of any value encrypted under this key.",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "AROAJTHA7W63OIIHBAI4G",
          "AROAJU3WGXSZF25LYZMQ6"
        ]
      },
      "Action": "kms:Decrypt",
      "Resource": "*"
    },
    {
      "Sid": "Allow encrypting under this key.",
      "Effect": "Allow",
      "Principal": {
        "AWS": "AROAJRE7V2F7RRCYUKOFG"
      },
      "Action": [
        "kms:Encrypt",
        "kms:GenerateDataKey"
      ],
      "Resource": "*"
    }
  ]
}

    response = client.create_key(
        Policy='string',
        Description='string',
        KeyUsage='ENCRYPT_DECRYPT',
        Origin='AWS_KMS' | 'EXTERNAL',
        BypassPolicyLockoutSafetyCheck=True | False,
        Tags=[
            {
                'TagKey': 'string',
                'TagValue': 'string'
            },
        ]
    )

    #print(pretty(response))
    new_aliases = {}
    if 'Aliases' in response:
        aliases = response.get('Aliases')

        for a in aliases:
            #print(pretty(a))
            if 'AliasName' in a:
                #print('There is an alias name in dict')
                new_aliases[str(a['TargetKeyId'])] = []
                new_aliases[str(a['TargetKeyId'])].append(a['AliasName'])
                new_aliases[str(a['TargetKeyId'])].append(a['AliasArn'])



    #print(pretty(new_aliases))
    response = client.list_keys(
        Limit=99,
    )

    #print(pretty(response))


    if 'Keys' in response:
        keys = response.get('Keys')
        menu = {}
        counter=0

        for k in keys:
            counter += 1
            my_list = []
            my_list.append(k['KeyArn'])
            my_list.append(k['KeyId'])
            if str(k['KeyId']) in new_aliases:
                my_list.append(new_aliases[str(k['KeyId'])][0])
                my_list.append(new_aliases[str(k['KeyId'])][1])
            else:
                my_list.append('None')
                my_list.append('None')
            my_list.append(k)
            menu[counter] = my_list

        if len(menu) > 0:
            print "\n"
            print '#########################################'
            print '## Select KeyArn                       ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][2]+" "+menu[key][1]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][4]
                        break


        #print(pretty(info))

        print("\n##################################")
        print('Alias Name: '+str(menu[int(ans)][2]))
        print('Alias ARN: '+str(menu[int(ans)][3]))
        print("##################################")
        response = client.describe_key(
            KeyId=info['KeyId'],
        )

        print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
