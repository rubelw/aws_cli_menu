#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('apigateway')



    response = client.get_rest_apis()
    print(pretty(response))
    stacks = response.get('items')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            my_list = []

            my_list.append(s['id'])
            my_list.append(s['name'])
            counter += 1
            menu[counter] = my_list

        print "\n\n"
        print '#########################################'
        print '## Select APIs                        ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]+' - '+str(menu[key][1])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    api_id = menu[int(ans)][0]
                    break

        print('id: '+str(api_id))
        response = client.get_resources(
            restApiId=str(api_id)
            #position='string',
            #limit=123
        )

        stacks = response.get('items')

        if len(stacks) > 0:

            menu = {}
            counter = 0
            for s in stacks:

                print(pretty(s))
                my_list = []

                my_list.append(s['id'])
                my_list.append(s['path'])
                my_list.append(s)
                counter += 1
                menu[counter] = my_list

            print "\n\n"
            print '#########################################'
            print '## Select Resource                     ##'
            print '#########################################'
            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]+' - '+str(menu[key][1])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        id = menu[int(ans)][0]

                        if 'resourceMethods' in menu[int(ans)][2]:
                            methods = menu[int(ans)][2]['resourceMethods']
                        break

            if len(methods)>0:
                menu = {}
                counter = 0
                for m in methods:
                    my_list=[]
                    my_list.append(m)
                    counter+=1
                    menu[counter]=my_list

                print "\n\n"
                print '#########################################'
                print '## Select HTTP Method                  ##'
                print '#########################################'
                for key in sorted(menu):
                    print str(key) + ":" + menu[key][0]

                pattern = r'^[0-9]+$'
                while True:
                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            method = menu[int(ans)][0]
                            break

                print('method: '+str(method))
                print('api_id: '+str(api_id))
                print('resource id: '+str(id))
                print('http method: '+str(method))


                response = client.get_method(
                    restApiId=str(api_id),
                    resourceId=str(id),
                    httpMethod=str(method)
                )

                print(pretty(response))

except (KeyboardInterrupt, SystemExit):
    sys.exit()
