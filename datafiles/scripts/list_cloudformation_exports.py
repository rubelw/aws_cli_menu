#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

def get_resources_from(compliance_details):
    results = compliance_details['EvaluationResults']
    resources = [result['EvaluationResultIdentifier']['EvaluationResultQualifier'] for result in results]
    next_token = compliance_details.get('NextToken', None)

    return resources, next_token


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('cloudformation')

    next_token = ''
    exports = []

    # Call the `get_compliance_details_by_config_rule` method in a loop
    # until we have all the results from the AWS Config service API.
    while next_token is not None:

        if len(next_token)<1:
            details = client.list_exports()
            print(details)
        else:
            details = client.list_exports(
                NextToken=next_token
                )

        if 'NextToken' in details:
            next_token = details['NextToken']
        else:
            next_token = None

        if 'Exports' in details:

            for export in details['Exports']:
                exports.append(export)


    for export in exports:

        print(export)

except (KeyboardInterrupt, SystemExit):
    sys.exit()
