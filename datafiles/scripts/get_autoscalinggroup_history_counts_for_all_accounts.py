#!/usr/bin/env python


import boto3.session
import sys
from aws_cli_menu_helper import *
from os.path import expanduser
import re
from dateutil.parser import *
import datetime
import dateutil
from pytz import timezone

from collections import defaultdict

DEBUG =0

def date2str(dt):
	return dt.strftime("%a, %d %b %Y %H:%M:%S GMT")


def get_all_account_profiles():
        home = expanduser("~")
        cred_file = home+'/.aws/credentials'

        lines = [line.rstrip('\n') for line in open(cred_file)]

        profiles = []
        for line in lines:
                matchObj = re.match( r'^\s*\[(.*)\]\s*$', line, re.M|re.I)
                if matchObj:

                    if matchObj.group(1) <> 'default':
                        profiles.append(matchObj.group(1))

        return profiles


try:

    profile_names = get_all_account_profiles()

    if DEBUG:
        print('profile names: '+str(profile_names))

    ec2info = defaultdict()


    unique_zones = {}
    hosted_zones = {}
    for p in profile_names:
        hosted_zones[str(p)]=[]

    if (DEBUG):
        print(pretty(hosted_zones))

    if len(profile_names) >0:
            for p in profile_names:

                if (DEBUG):
                    print("\n")
                    print("#################################################")
                    print('Getting information for profile name: '+str(p))
                    print("#################################################")

                try:
                    session = boto3.session.Session(profile_name=p)

                    client = session.client('autoscaling')

                    response = client.describe_auto_scaling_groups()

                    if 'AutoScalingGroups' in response:
                        stacks = response.get('AutoScalingGroups')

                        if (DEBUG):
                            print("\t"+'Total auto scaling groups: '+str(len(stacks))+"\n")

                        if len(stacks) > 0:
                            for s in stacks:
                                name = s.get('AutoScalingGroupName')
                                if (DEBUG):
                                    print("\t"+'AutoScalingGroup name: ' + str(name)+' account: '+str(p))


                                activities = client.describe_scaling_activities(
                                    AutoScalingGroupName=str(name)
                                )

                                activity_count = 0

                                if 'Activities' in activities:
                                    activities_stack = activities.get('Activities')

                                    if len(activities_stack)>0:
                                        if (DEBUG):
                                            print("\t\t"+'total activities: '+str(len(activities_stack)))

                                        todays_activities = []

                                        for item in activities_stack:
                                            date = item['StartTime']
                                            date = str(date)[:-13]

                                            if(DEBUG):
                                                print('date: '+str(date))

                                            activity_date = dateutil.parser.parse(str(date))

                                            activity_date = activity_date.replace(tzinfo=timezone('UTC'))
                                            tz_info = activity_date.tzinfo



                                            delta = datetime.datetime.now(tz_info)-activity_date;
                                            days = delta.days

                                            if (int(days)<1):
                                                activity_count+=1
                                                todays_activities.append(item)


                                        if (DEBUG):
                                            print("\t\t"+'todays activity count: '+str(activity_count))

                                        if (int(activity_count)>10):


                                            successful = 0
                                            not_successful = 0

                                            for item in todays_activities:

                                                if (DEBUG):
                                                    print(pretty(item))

                                                if item['StatusCode'] == 'Successful':
                                                    successful+=1
                                                else:
                                                    not_successful+=1

                                            if (DEBUG):
                                                print('Total successful instance creations: '+str(successful))
                                                print('Other than successful instance creations: '+str(not_successful))

                                            average = (successful + not_successful)/2

                                            if (average > (successful -10)) and (average < (successful+10)) and (average > (not_successful-10)) and (average < (not_successful+10)):
                                                print("\t\t"+'#############################')
                                                print("\t\t"+'AutoScalingGroup name: ' + str(name)+' account: '+str(p))
                                                print("\t\t"+'We have churning')
                                                print("\t\t"+'#############################')




                                    else:
                                        if (DEBUG):
                                            print("\t\t"+'ASG has no activities')


                                else:
                                    if (DEBUG):
                                        print("\t\t"+'ASG has no activities')

                        else:

                            if (DEBUG):
                                print("\n")
                                print("#########################")
                                print('No AutoScalingGroups')
                                print("#########################")
                    else:
                        if (DEBUG):
                            print("\n")
                            print("##########################")
                            print('No AutoScalingGroups')
                            print("##########################")

                except Exception as e:
		            print(e)

    else:
        print('There are not any profiles')
        sys.exit()




except (KeyboardInterrupt, SystemExit):
    sys.exit()
