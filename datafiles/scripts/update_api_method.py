#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys

try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    client = session.client('apigateway')
    response = client.get_rest_apis()

    #print(pretty(response))
    stacks = response.get('items')


    if len(stacks)>0:

        menu = {}
        counter=0

        for s in stacks:
            counter+=1
            my_list = []
            my_list.append(s['name'])
            if 'description' in s:
                my_list.append(s['description'])
            else:
                my_list.append('No description')
            my_list.append(s)
            menu[counter]=my_list

        print "\n"
        print '#############################'
        print '## Select API'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    info = menu[int(ans)][2]
                    break


        api_id = info['id']


        response = client.get_resources(
            restApiId=str(api_id),
        )


        print(pretty(response))

        items=response['items']

        if len(items)>0:

            menu = {}
            counter=0

            for s in items:
                counter+=1
                my_list = []
                my_list.append(s['path'])
                my_list.append(s['id'])
                my_list.append(s)
                menu[counter]=my_list

            print "\n"
            print '#############################'
            print '## Select Method'
            print '#############################'
            for key in sorted(menu):
                print str(key)+":" + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][2]
                        break




        parent_id = info['id']
        parent_path = info['path']

        menu = {}
        my_list=['GET','POST','OPTIONS']
        counter=0
        for i in my_list:
            counter+=1
            menu[counter]=i

        print(pretty(menu))

        print "\n"
        print '#############################'
        print '## Select Method Type'
        print '#############################'
        for key in sorted(menu):
            print str(key)+":" + str(menu[key])

        pattern = r'^[0-9]+$'
        while True:

            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    method_type = menu[int(ans)]
                    break


        want_custom_authorizer = yes_or_no('Do you want a custom authorizer?')
        want_api_key = yes_or_no('Do you want to use an api key?')

        if want_api_key:
            api_key_required = True
        else:
            api_key_required = False

        are_request_parameters = yes_or_no('Are there request parameters?')


        is_request_model = yes_or_no('Is there a request model?')
        if is_request_model == 'Yes':
            request_model = raw_input("Enter request model: [ENTER](Cntrl-C to exit)")

        response_status_code = raw_input("Enter response status code: [ENTER](Cntrl-C to exit)")

        are_response_parameters = yes_or_no('Are there response parameters?')
        if are_response_parameters  == 'Yes':
            response_parameters = raw_input("Enter response parameters: [ENTER](Cntrl-C to exit)")

        is_response_model = yes_or_no('Is there a response model?')
        if is_response_model == 'Yes':
            response_model = raw_input("Enter response model: [ENTER](Cntrl-C to exit)")


        want_to_add_integration = yes_or_no('Do you want to add integration?')
        if want_to_add_integration == 'Yes':
            menu = {}
            my_list=['HTTP','AWS','MOCK','HTTP_PROXY','AWS_PROXY']
            counter=0
            for i in my_list:
                counter+=1
                menu[counter]=i

            print(pretty(menu))

            print "\n"
            print '#############################'
            print '## Select Integration Type'
            print '#############################'
            for key in sorted(menu):
                print str(key)+":" + str(menu[key])

            pattern = r'^[0-9]+$'
            while True:

                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        integration_type = menu[int(ans)]
                        break


            integration_http_method = raw_input("Enter http method for integration: [ENTER](Cntrl-C to exit)")
            integration_credentials = raw_input("Enter arn of integration credentials: [ENTER](Cntrl-C to exit)")
            are_integration_request_parameters = yes_or_no('Are there integration request parameters?')
            if are_integration_request_parameters == 'Yes':
                integration_request_parameters = raw_input("Enter integration request parameters: [ENTER](Cntrl-C to exit)")


            is_integration_request_template = yes_or_no('Is there an integration request template?')
            if is_integration_request_template == 'Yes':
                integration_request_template = raw_input("Enter integration request template: [ENTER](Cntrl-C to exit)")

            is_integration_passthrough_behavior = yes_or_no('Is there an integration passthrough behavior?')
            if is_integration_passthrough_behavior == 'Yes':
                integration_passthrough_behavior = raw_input("Enter integration passthrough behavior: [ENTER](Cntrl-C to exit)")


            is_integration_cache_namespace = yes_or_no('Is there an integration cache namespace?')
            if is_integration_cache_namespace == 'Yes':
                integration_cache_namespace = raw_input("Enter integration cache namespace: [ENTER](Cntrl-C to exit)")

                integration_cache_parameters = raw_input("Enter integration cache parameters: [ENTER](Cntrl-C to exit)")


            integration_response = raw_input("Enter integration response: [ENTER](Cntrl-C to exit)")
            integration_response_status_code = raw_input("Enter integration response status code: [ENTER](Cntrl-C to exit)")
            integration_response_selection_pattern = raw_input("Enter integration response selection pattern: [ENTER](Cntrl-C to exit)")

            is_integration_response_parameters = yes_or_no('Are there integration response parameters?')
            if is_integration_response_parameters == 'Yes':
               integration_response_parameters = raw_input("Enter integration response parameters: [ENTER](Cntrl-C to exit)")


            is_integration_response_template = yes_or_no('Is there an integration response template?')
            if is_integration_response_template == 'Yes':
               integration_response_template = raw_input("Enter integration response template: [ENTER](Cntrl-C to exit)")


        if want_custom_authorizer == 'Yes':

            response = client.get_authorizers(
                restApiId=str(api_id)
            )

            stacks = response.get('items')


            if len(stacks)>0:


                menu = {}
                counter=0

                for s in stacks:
                    counter+=1
                    my_list = []
                    my_list.append(s['name'])
                    my_list.append('authType')

                    my_list.append(s)
                    menu[counter]=my_list

                print "\n"
                print '#############################'
                print '## Select Authorizer'
                print '#############################'
                for key in sorted(menu):
                    print str(key)+":" + str(menu[key][0] + ' - '+str(menu[key][1]))

                pattern = r'^[0-9]+$'
                while True:

                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            info = menu[int(ans)][2]
                            break


                print(pretty(info))
                authorizer_id = info['id']
                authorizer_type = info['authType']


                response = client.put_method(
                    restApiId=str(api_id),
                    resourceId=str(parent_id),
                    httpMethod=str(method_type),
                    #authorizationType=str(authorizer_type),
                    authorizerId=str(authorizer_id),
                    apiKeyRequired=False
                #    #requestParameters={
                #    #    'string': True|False
                #    #},
                #    #requestModels={
                #    #    'string': 'string'
                #    #}
                )

                print(pretty(response))

            else:
                print("\n"+'There are no authorizers')





                response = client.put_method(
                    restApiId=str(api_id),
                    resourceId=str(parent_id),
                    httpMethod=str(method_type),
                #    authorizationType='None',
                #    #authorizerId='string',
                    apiKeyRequired=api_key_required
                #    #requestParameters={
                #    #    'string': True|False
                #    #},
                #    #requestModels={
                #    #    'string': 'string'
                #    #}
                )

                print(pretty(response))



        # Does not want a custom authorizer



    else:
        print("\n")
        print("##########################")
        print('No APIs Found')
        print("##########################")

except (KeyboardInterrupt, SystemExit):
    sys.exit()
