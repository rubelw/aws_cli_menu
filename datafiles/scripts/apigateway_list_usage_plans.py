#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import yaml

boto3.set_stream_logger(name='botocore')


try:

    profile_name = get_profile_name()
    print('profile_name: '+profile_name)

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client('apigateway')

    response = client.get_usage_plans(
        limit=99
    )
    print(pretty(response))
    stacks = response.get('items')

    if len(stacks) > 0:

        menu = {}
        counter = 0
        for s in stacks:
            my_list = []

            my_list.append(s['id'])
            my_list.append(s['name'])
            counter += 1
            menu[counter] = my_list

        print "\n\n"
        print '#########################################'
        print '## Select Usage Plan                   ##'
        print '#########################################'
        for key in sorted(menu):
            print str(key) + ":" + menu[key][0]+' - '+str(menu[key][1])

        pattern = r'^[0-9]+$'
        while True:
            ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
            if re.match(pattern, ans) is not None:
                if int(ans) in menu:
                    id = menu[int(ans)][0]
                    break

        print('id: '+str(id))
        response = client.get_usage_plan(
            usagePlanId=str(id)

        )

        print(pretty(response))


except (KeyboardInterrupt, SystemExit):
    sys.exit()
