#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import inspect
import psycopg2
import json
import re
import os
import paramiko




try:


    # Get the .ssh/config file jump server host name
    jump_name = raw_input("Enter the Host name of the jump server in your .ssh config: [ENTER](Cntrl-C to exit)")

    ret_dict={}
    local_forwards = {}

    with open(os.path.expanduser('~/.ssh/config')) as f:
        cfg = paramiko.SSHConfig()
        cfg.parse(f)

        for d in cfg._config:
            _copy = dict(d)
            del _copy['host']
            for host in d['host']:
                ret_dict[host] = _copy['config']

    if jump_name in ret_dict:
        print(ret_dict[jump_name])
        if 'localforward' in ret_dict[jump_name]:
            for lf in ret_dict[jump_name]['localforward']:
                print(lf)
                port,db = lf.split(" ")
                print('port: '+str(port))
                print('db: '+str(db))

    creds = get_rds_profile_name()
    print(pretty(creds))


    profile = creds.keys()[0]

    print(pretty(profile))

    conn_string = 'dbname=\''+str(creds[profile][0]['database'])+'\' user=\''+str(creds[profile][0]['user'])+'\' host=\'localhost\' port=\''+str(creds[profile][0]['port'])+'\' password=\''+str(creds[profile][0]['password'])+'\''

    print('conn_string is:'+str(conn_string))



    try:
        conn=psycopg2.connect(conn_string)
        print('Connected to database')

        cur = conn.cursor()
        cur.execute("SELECT r.rolname, r.rolsuper, r.rolinherit,r.rolcreaterole, r.rolcreatedb, r.rolcanlogin,r.rolconnlimit,ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) as memberof FROM pg_catalog.pg_roles r ORDER BY 1;")

        rows = cur.fetchall()

        users = {}


        for row in rows:
            #print('test: '+str(row[0]))
            users[row[0]]=row
            #print(pretty(row))

        print(pretty(users))

        cur.close()
        conn.close()

        menu = {}

        counter = 0

        for i in sorted(users):
            counter +=1
            #print(pretty(i))
            menu[counter]=users[i]


        print(pretty(menu))
        if len(menu) > 0:

            print("\n")
            print("#################################")
            print("Select User")
            print("#################################")

            for key in sorted(menu):
                print str(key) + ":" + menu[key][0]

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        user_name = menu[int(ans)][0]
                        break

            print("\n")
            print("##############################")
            print("Credentials for "+str(user_name))
            print("##############################")
            print(pretty(users[str(user_name)][7]))

        else:
            print("\n")
            print("####################")
            print('No users found')
            print("####################")




    except:
        print('Could not connect to database')








except (KeyboardInterrupt, SystemExit):
    sys.exit()