#!/usr/bin/env python

import boto3.session
from aws_cli_menu_helper import *
import sys
import uuid


def my_random_string(string_length=10):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4()) # Convert UUID format to a Python string.
    random = random.upper() # Make all characters uppercase.
    random = random.replace("-","") # Remove the UUID '-'.
    return random[0:string_length] # Return the random string.


try:

    profile_name = get_profile_name()

    session = boto3.session.Session(profile_name=profile_name)

    domain_name = raw_input("Enter Domain Name: [ENTER](Cntrl-C to exit)")

    comment = raw_input("Enter Comment: [ENTER](Cntrl-C to exit)")

    menu = {1: ['Public Hosted Zone'],2:['Public Hosted Zone for Amazon VPC']}

    print "\n"
    print '#########################################'
    print '## Select Type                         ##'
    print '#########################################'
    for key in sorted(menu):
        print str(key) + ":" + menu[key][0]


    pattern = r'^[0-9]+$'
    while True:

        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                selected_type = menu[int(ans)][0]
                break

    client = session.client('ec2')
    response = client.describe_vpcs()


    if 'Vpcs' in response:

        stacks = response.get('Vpcs')

        if len(stacks) > 0:


            menu = {}
            counter = 0
            for s in stacks:
                counter+=1
                my_list = []

                if 'Tags' in s:
                    name = 'Tag: '+str(s['Tags'][0]['Value'])
                else:
                    name = 'Tag: None - ' + str(s.get('VpcId'))
                vpcid = s.get('VpcId')
                cidr_block = s.get('CidrBlock')

                my_list.append(name)
                my_list.append(vpcid)
                my_list.append(cidr_block)
                my_list.append(s)
                menu[counter] = my_list


            if len(menu) > 0:

                print "\n"
                print '#########################################'
                print '## Select VPC                          ##'
                print '#########################################'
                for key in sorted(menu):
                    print str(key) + ":" + menu[key][0] + ' - ' + menu[key][1] + ' - ' + menu[key][2]

                pattern = r'^[0-9]+$'
                while True:

                    ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            vpc_id = menu[int(ans)][1]
                            info = menu[int(ans)][3]
                            break
                        elif int(ans) ==0:
                            sys.exit(1)



    menu = {1 : ['us-east-1'], 2:['us-west-1'], 3: ['us-west-2'], 4: ['eu-west-1'], 5: ['eu-central-1'], 6:['ap-southeast-1'], 7:['ap-southeast-2'], 8: ['ap-northeast-1'], 9: ['ap-northeast-2'], 10: ['sa-east-1'], 11: ['cn-north-1']}


    print "\n"
    print '#########################################'
    print '## Select Region                       ##'
    print '#########################################'
    for key in sorted(menu):
        print str(key) + ":" + menu[key][0]


    pattern = r'^[0-9]+$'
    while True:

        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                region = menu[int(ans)][0]
                break


    menu = {1:['True'],2:['False']}

    print "\n"
    print '#########################################'
    print '## Select Whether Private Zone         ##'
    print '#########################################'
    for key in sorted(menu):
        print str(key) + ":" + menu[key][0]


    pattern = r'^[0-9]+$'
    while True:

        ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
        if re.match(pattern, ans) is not None:
            if int(ans) in menu:
                private_zone = menu[int(ans)][0]
                break



    client = session.client('route53')


    response = client.create_hosted_zone(
        Name=domain_name,
        VPC={
            'VPCRegion': str(region),
            'VPCId': str(vpc_id)
        },
        CallerReference=my_random_string(6),
        HostedZoneConfig={
            'Comment': str(comment),
            'PrivateZone': bool(private_zone)
        }
        #DelegationSetId='string'
    )


except (KeyboardInterrupt, SystemExit):
    sys.exit()