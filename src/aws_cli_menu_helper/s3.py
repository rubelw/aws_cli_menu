
import boto3.session
from aws_cli_menu_helper import *

def create_s3_bucket(profile=None,title=None):

    try:
        profile
    except NameError:
        profile = get_profile_name()

    try:

        session = boto3.session.Session(profile_name=profile)
        client = session.client('s3')

        try:
            title
        except NameError:
            title = 'Enter new bucket name: [ENTER](Cntrl-C to exit)'

        ans = raw_input(title)

        response = client.create_bucket(Bucket=str(ans))

        print(pretty(response))

        return ans



    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def select_s3_bucket(profile=None):

    try:
        profile
    except NameError:
        profile = get_profile_name()

    try:

        profile = get_profile_name()

        session = boto3.session.Session(profile_name=profile)
        client = session.client('s3')
        response = client.list_buckets()
        stacks = response.get('Buckets')

        if len(stacks) > 0:

            menu = {}
            counter = 0
            for s in stacks:
                counter += 1
                my_list = []
                my_list.append(s['Name'])
                my_list.append(s)
                menu[counter] = my_list

            print "\n\n"
            print '#############################'
            print '## Select S3 Buckets       ##'
            print '#############################'
            for key in sorted(menu):
                print str(key) + ": " + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][1]
                        break

            print("\n")
            print("########################")
            print("Detailed Bucket Info")
            print("########################")
            print(pretty(info))

            return info['Name']

        else:
            print("\n")
            print("#######################")
            print('No S3 buckets found')
            print("#######################")

    except (KeyboardInterrupt, SystemExit):
        sys.exit()



def delete_s3_bucket(profile=None):

    try:
        profile
    except NameError:
        profile = get_profile_name()

    try:

        profile = get_profile_name()

        session = boto3.session.Session(profile_name=profile)
        client = session.client('s3')
        response = client.list_buckets()
        stacks = response.get('Buckets')

        if len(stacks) > 0:

            menu = {}
            counter = 0
            for s in stacks:
                counter += 1
                my_list = []
                my_list.append(s['Name'])
                my_list.append(s)
                menu[counter] = my_list

            print "\n\n"
            print '#############################'
            print '## Select S3 Buckets       ##'
            print '#############################'
            for key in sorted(menu):
                print str(key) + ": " + str(menu[key][0])

            pattern = r'^[0-9]+$'
            while True:
                ans = raw_input("Make A Choice: [ENTER](Cntrl-C to exit)")
                if re.match(pattern, ans) is not None:
                    if int(ans) in menu:
                        info = menu[int(ans)][1]
                        break

            print("\n")
            print("########################")
            print("Detailed Bucket Info")
            print("########################")
            print(pretty(info))

            bucket_name = info['Name']

            response = client.delete_bucket(
                Bucket=str(bucket_name)
            )

            print(pretty(response))


        else:
            print("\n")
            print("#######################")
            print('No S3 buckets found')
            print("#######################")

    except (KeyboardInterrupt, SystemExit):
        sys.exit()
