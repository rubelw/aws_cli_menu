
import boto3.session
import re
from .pretty import *
import os
import sys
from os.path import expanduser
import ConfigParser
from .s3 import *
from Tkinter import Tk
from tkFileDialog import askdirectory
from inspect import currentframe

DEBUG=1

def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno



def create_stackility_directory_config_option():
    if (DEBUG):
        print('stackility - def create_stackility_directory_config_option')

    home = expanduser("~")
    PATH = home + '/aws-cli-menu/config.ini'

    if os.path.exists(PATH):
        # lets create that config file for next time...

        home = expanduser("~")

        PATH = home + '/aws-cli-menu/config.ini'

        # add the settings to the structure of the file, and lets write it out...
        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        config.read(PATH)
        cfgfile = open(PATH, 'w')

        if config.has_section('stackility'):

            if (DEBUG):
                print('config file has stackility section')

                Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
                print('#########################################')
                print('Select cloudformation projects directory')
                print('#########################################')

                directory = askdirectory(initialdir=str(home), title="Select cloudformation projects directory")

                if (DEBUG):
                    print(directory)

                config.set('stackility', 'directory', directory)
                config.write(cfgfile)
                cfgfile.close()

        else:
            print('could not find stackility section in config file')

            Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
            print('#########################################')
            print('Select cloudformation projects directory')
            print('#########################################')

            directory = askdirectory(initialdir=str(home), title="Select cloudformation projects directory")

            if (DEBUG):
                print(directory)

            config.add_section('stackility')
            config.set('stackility', 'directory', directory)
            config.write(cfgfile)
            cfgfile.close()


    else:
        print('Directory does not exist. Create the directory and try again')


def create_stackility_s3_config_option(profile=None):

    if (DEBUG):
        print('stackility - def create_stackility_s3_config_option')

    home = expanduser("~")
    PATH = home + '/aws-cli-menu/config.ini'

    if os.path.exists(PATH):
        # lets create that config file for next time...


        # add the settings to the structure of the file, and lets write it out...
        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        config.read(PATH)
        cfgfile = open(PATH, 'w')

        if config.has_section('stackility'):

            if (DEBUG):
                print('config file has stackility section')

            ans = yes_or_no('Would you like to use an existing bucket?')

            if ans:

                try:
                    profile
                except NameError:
                    profile = get_profile_name()

                bucket = select_s3_bucket(profile)

                if (DEBUG):
                    print(bucket)

                config.set('stackility', 'bucket', bucket)
                config.write(cfgfile)
                cfgfile.close()

        else:
            print('could not find stackility section in config file')
            create_stackility_stackility_section()
    else:
        print('Could not find aws-cli-config file')



def create_stackility_stackility_section():
    if (DEBUG):
        print('stackility - def create_stackility_stackility_section')

    home = expanduser("~")
    PATH = home + '/aws-cli-menu/config.ini'
    home = expanduser("~")
    PATH = home + '/aws-cli-menu/config.ini'

    if os.path.exists(PATH):
        # lets create that config file for next time...


        # add the settings to the structure of the file, and lets write it out...
        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        config.read(PATH)
        cfgfile = open(PATH, 'w')

        if config.has_section('stackility'):

            if (DEBUG):
                print('config file has stackility section')

        else:
            print('could not find stackility section in config file')
            config.add_section('stackility')
            config.write(cfgfile)
            cfgfile.close()

    else:
        print('Could not find aws-cli-config file')




def modify_stackility_project_directory():
    if (DEBUG):
        print('stackility - def modify_stackility_project_directory')

        create_stackility_directory_config_option()




def modify_stackility_s3_bucket():
    if (DEBUG):
        print('stackility - def modify_stackility_s3_bucket')
        create_stackility_s3_config_option()


def set_stackility_project_directory():

    if (DEBUG):
        print('stackility - def set_stackility_project_directory')

    try:
        home = expanduser("~")

        PATH = home + '/aws-cli-menu/config.ini'

        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
            print "aws-cli-menu config.ini file exists and is readable"

            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('stackility'):

                if (DEBUG):
                    print('config file has stackility section')

                try:
                    directory = config.get('stackility', 'directory')
                    print('stackility directory already set.  modify ~/aws-cli-menu/config.ini if needed')


                except ConfigParser.NoOptionError:
                    print('config file does not contain stackility directory option')
                    create_stackility_directory_config_option()


            else:
                print('could not find stackility section in config file')
                # Create stackility directory
                create_stackility_directory_config_option()


        else:
            print "Either file is missing or is not readable.  Let us create a file"

            create_stackility_directory_config_option()



    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def set_stackility_s3_bucket(bucket=None):

    if (DEBUG):
        print('stackility - def set_stackility_s3_bucket')
        print("\tbucket is: "+str(bucket))

    if bucket != None:

        try:
            home = expanduser("~")

            PATH = home + '/aws-cli-menu/config.ini'

            if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
                print "File exists and is readable"

                config = ConfigParser.RawConfigParser()
                config.optionxform = str
                config.read(PATH)

                if config.has_section('stackility'):

                    cfgfile = open(PATH, 'w')
                    config.set('stackility', 'bucket', bucket)
                    config.write(cfgfile)
                    cfgfile.close()

                else:
                    print('config file does not have stackility section')

            else:
                print "Either file is missing or is not readable.  Let us create a file"

        except (KeyboardInterrupt, SystemExit):
            sys.exit()

    else:
        bucket = create_stackility_s3_config_option()

        try:
            home = expanduser("~")

            PATH = home + '/aws-cli-menu/config.ini'

            if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
                print "File exists and is readable"

                config = ConfigParser.RawConfigParser()
                config.optionxform = str
                config.read(PATH)

                if config.has_section('stackility'):

                    try:
                        config.set('stackility', 'bucket', str(bucket))
                        cfgfile = open(PATH, 'w')
                        config.write(cfgfile)
                        cfgfile.close()
                        return

                    except ConfigParser.NoOptionError:
                        print('could not set bucket in stackility config')
                else:
                    print('config file does not have stackility section')

            else:
                print "Either file is missing or is not readable.  Let us create a file"

                create_stackility_directory_config_option()

        except (KeyboardInterrupt, SystemExit):
            sys.exit()


def select_stackility_project():

    if (DEBUG):
        print('stackility - def select_stackility_project')


    projects = list_stackility_projects()

def list_stackility_projects():

    if (DEBUG):
        print('stackility - def list_stackility_projects')

    proj_dir,core_dir = get_stackility_project_directory()

    projects = [os.path.join(proj_dir, o) for o in os.listdir(proj_dir)
                    if os.path.isdir(os.path.join(proj_dir,o))]

    core_projects = [os.path.join(proj_dir, o) for o in os.listdir(core_dir)
                    if os.path.isdir(os.path.join(core_dir,o))]

    ans = raw_input("Enter 1 for core projects or 2 for other projects: [ENTER]")

    if int(ans) == 1:

        if len(core_projects)>0:
            print(core_projects)
        else:
            print('no core_projects in core_projects directory')

            ans = yes_or_no('Would you like to create a new stackility core_project directory?')
            print(ans)
            if ans:
                create_actual_stackility_project_directory()
            else:
                print('You must create a stackility project directory in order to continue')
                sys.exit()
    elif int(ans) ==2:

        if len(projects) > 0:
            print(projects)
        else:
            print('no projects in projects directory')

            ans = yes_or_no('Would you like to create a new stackility project directory?')
            print(ans)
            if ans:
                create_actual_stackility_project_directory()
            else:
                print('You must create a stackility project directory in order to continue')
                sys.exit()


def create_actual_stackility_project_directory(directory):

    if (DEBUG):
        print('stackility - def create_actual_stackility_project_directory')


    print('creating stackility projects directory')
    proj_directory,core_directory = get_stackility_project_directory()


    ans = raw_input("Enter 1 for core projects or 2 for other projects: [ENTER]")

    if int(ans) ==1:

        ans = raw_input("Enter core_project name without spaces: [ENTER]")

        try:
            os.stat(core_directory+'/'+str(ans))
        except:
            os.mkdir(core_directory+'/'+str(ans))

        return ans
    elif int(ans) ==2:

        ans = raw_input("Enter project name without spaces: [ENTER]")

        try:
            os.stat(proj_directory + '/' + str(ans))
        except:
            os.mkdir(proj_directory + '/' + str(ans))

        return ans


def get_stackility_project_directory():

    if (DEBUG):
        print('stackility - def get_stackility_project_directory')

    try:
        home = expanduser("~")

        PATH = home + '/aws-cli-menu/config.ini'

        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):

            if (DEBUG):
                print "aws-cli-menu config.ini file exists and is readable"

            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('stackility'):

                if (DEBUG):
                    print('ini file has stackility section')

                if config.get('stackility', 'directory'):

                    if (DEBUG):
                        print('config file has stackility directory option')

                    return [str(config.get('stackility','directory')),str(config.get('stackility','core_directory'))]
                else:
                    print('However, there is no stackility project directory set.  Add directory to stackility in config file')
                    create_stackility_directory_config_option()

            else:
                print('could not find the stackility section in the config file')
                # Create stackility directory
                create_stackility_directory_config_option()

        else:
            print "Either file is missing or is not readable.  Let us create a file"

            create_stackility_directory_config_option()

    except (KeyboardInterrupt, SystemExit):
        sys.exit()



def get_stackility_s3_bucket():

    if (DEBUG):
        print('stackility - def get_stackility_s3_bucket')

    try:
        home = expanduser("~")

        PATH = home + '/aws-cli-menu/config.ini'

        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
            if (DEBUG):
                print "aws-cli-menu config file exists and is readable"

            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('stackility'):

                try:
                    directory = config.get('stackility', 'bucket')
                    print('stackility bucket already set.  modify ~/aws-cli-menu/config.ini if needed')
                    return str(config.get('stackility','bucket'))
                except ConfigParser.NoOptionError:
                    create_stackility_s3_config_option()
            else:
                # Create stackility directory
                create_stackility_s3_config_option()


        else:
            print "Either file is missing or is not readable.  Let us create a file"

            create_stackility_s3_config_option()



    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def create_stackility_project_directory(directory):

    if (DEBUG):
        print('stackility - def create_stackility_project_directory')
    if not os.path.exists(directory):
        os.makedirs(directory)


def create_stackility_project_template():

    if (DEBUG):
        print('stackility - def create_stackility_project_template')

    try:
        home = expanduser("~")

        PATH = home + '/aws-cli-menu/config.ini'

        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):

            if (DEBUG):
                print "aws-cli-menu config file exists and is readable"

            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('stackility'):

                if (DEBUG):
                    print('config file has stackility section')

                try:
                    directory = config.get('stackility','directory')

                    if (DEBUG):
                        print('directory: '+str(directory))

                    subfolders = os.listdir(directory)

                    menu = {}
                    my_list=[]
                    my_list.append('Create a new project')
                    menu[0] = my_list


                    counter = 0
                    for folder in subfolders:

                        counter += 1
                        my_list = []
                        my_list.append(folder)
                        menu[counter] = my_list

                    # If there are existing projects
                    if len(menu) > 0:
                        print "\n\n"
                        print '#########################################'
                        print '## Select Project                    ##'
                        print '#########################################'
                        for key in sorted(menu):
                            print str(key) + ":" + str(menu[key][0])

                        pattern = r'^[0-9]+$'
                        while True:

                            ans = raw_input("Make A Choice: [ENTER]")
                            if re.match(pattern, ans) is not None:
                                if int(ans) in menu:
                                    project_name = menu[int(ans)][0]
                                    break

                        if project_name == 'Create a new project':
                            project_name = raw_input("Enter new project name with no spaces: [ENTER]")
                            project_directory = os.path.join(directory, project_name)

                            if not os.path.exists(project_directory):
                                os.makedirs(project_directory)

                            stack_name = raw_input("Enter new stack with no spaces (example "+str(project_name)+"_iam: [ENTER]")
                            stack_directory = os.path.join(project_directory,stack_name)

                            print('stack directory: '+str(stack_directory))
                            if not os.path.exists(stack_directory):
                                config_directory = os.path.join(stack_directory,'config')
                                os.makedirs(stack_directory)
                                os.makedirs(config_directory)

                            create_stackility_ini_template_file( stack_name,stack_directory)

                            if not os.path.exists(str(stack_directory) + '/template.json'):
                                open(str(stack_directory) + '/template.json', 'w').close()


                        else:
                            print('A project already exists, you will have to manually update the template')

                    # If there are no existing projects
                    else:

                        project_name = raw_input("Enter new project name with no spaces: [ENTER]")
                        project_directory = os.path.join(directory, project_name)

                        if not os.path.exists(directory):
                            os.makedirs(str(directory)+'/'+str(project_name))
                            os.makedirs(str(directory)+'/'+str(project_name)+'/config')

                            stack_name = raw_input("Enter new stack with no spaces (example " + str(project_name) + "_iam: [ENTER]")
                            stack_directory = os.path.join(project_dir, stack_name)

                            if not os.path.exists(stack_directory):
                                os.makedirs(stack_directory)

                        create_stackility_ini_template_file(stack_name,stack_directory)

                        if not os.path.exists(str(stack_directory)+'/template.json'):
                            open(str(stack_directory)+'/template.json', 'w').close()

                except ConfigParser.NoOptionError:
                    print('could not find stackility directory option in config file')

            else:
                # Create stackility directory
                print('could not find stackility section in config file')
                create_stackility_directory_config_option()


        else:
            print "Either file is missing or is not readable.  Let us create a file"

            create_stackility_directory_config_option()

    except (KeyboardInterrupt, SystemExit):
        sys.exit()



def create_stackility_ini_template_file(project,path):

    if (DEBUG):
        print('stackility def create_stackiligy_ini_template_file')
        print("\tpath: "+str(path))

    bucket = get_stackility_s3_bucket()

    if (DEBUG):
        print('bucket: '+str(bucket))

    project_dir = path
    template_file = str(project_dir)+'/config/config.ini'
    template_dir = project_dir

    if not os.path.isdir(template_dir):
        print('new directry has been created')
        os.system('mkdir '+str(template_dir))


    if os.path.exists(template_file):
        if (DEBUG):
            print('template file exists')
    else:
        if (DEBUG):
            print('trying to create '+str(template_file))
        open(template_file, 'w').close()



    Config = ConfigParser.RawConfigParser()
    Config.optionxform = str
    # lets create that config file for next time...
    cfgfile = open(str(project_dir)+'/config/config.ini', 'w')

    # add the settings to the structure of the file, and lets write it out...
    Config.add_section('environment')
    Config.set('environment','template', 'template.json')
    Config.set('environment','bucket', str(bucket))
    Config.set('environment','stack_name',str(project))
    Config.set('environment','region','us-east-1')
    Config.add_section('tags')
    Config.set('tags','OWNER','fixme')
    Config.set('tags','PROJECT','fixme')
    Config.set('tags','Name',str(project))
    Config.add_section('parameters')
    Config.set('parameters','example','fixme')
    Config.write(cfgfile)
    cfgfile.close()


def get_package_url_from_ini(config_file):

    if (DEBUG):
        print('stackility - def get_package_url_from_iniy')

    try:
            PATH = config_file
            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('parameters'):

                parameters = list(config.items('parameters'))

                menu = {}

                counter = 0
                for p in parameters:
                    print(p)
                    counter += 1
                    my_list = []
                    my_list.append(p)
                    menu[counter] = my_list

                # If there are existing projects
                if len(menu) > 0:
                    print "\n\n"
                    print '#########################################'
                    print '## Select S3 Recipe Url Template Parameter ##'
                    print '#########################################'
                    for key in sorted(menu):
                        print str(key) + ":" + str(menu[key][0])

                    pattern = r'^[0-9]+$'
                    while True:

                        ans = raw_input("Make A Choice: [ENTER]")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in menu:
                                package_parameter = menu[int(ans)][0]
                                break


                    if (DEBUG):
                        print('package parameter is: '+str(package_parameter))

                    return package_parameter[0]

                else:
                    print('However, there is no stackility project directory set.  Add directory to stackility in config file')

            else:
                print('could not find the stackility section in the config file')




    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def determine_if_opsworks_stack(config_file,project_dir):

    if (DEBUG):
        print('stackility - determine_if_opsworks_stack')
        print('config_file: '+str(config_file))
        print('project dir: '+str(project_dir))

    try:
            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(config_file)

            for each_section in config.sections():
                print('section: '+str(each_section))
                for (each_key, each_val) in config.items(each_section):
                    print each_key
                    print each_val

            try:
                template_file = config.get('environment', 'template')
                path = os.path.join(project_dir, template_file)


                print(path)

                if 'AWS::OpsWorks::Stack' in open(path).read():
                    return True
                else:
                    return False

            except ConfigParser.NoOptionError:
                print('could not find the stackility section in the config file')
                return False



    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def determine_if_lambda_stack(config_file,project_dir):

    if (DEBUG):
        print("\n\n")
        print("##############################################")
        print('stackility - determine_if_lambda_stack')
        print('config_file: '+str(config_file))
        print('project dir: '+str(project_dir))
        print("##############################################")
        print("\n\n")


    try:
            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(config_file)

            for each_section in config.sections():
                print('section: '+str(each_section))
                for (each_key, each_val) in config.items(each_section):
                    print each_key
                    print each_val

            try:
                template_file = config.get('environment', 'template')
                path = os.path.join(project_dir, template_file)


                print(path)

                if 'AWS::Lambda::Function' in open(path).read():

                    return True
                else:
                    return False

            except ConfigParser.NoOptionError:
                print('could not find the stackility section in the config file')
                return False



    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def get_lambda_bucket_from_ini(config_file):

    if (DEBUG):
        print('stackility - def get_lambda_bucket_from_ini: '+str(get_linenumber()))

    try:
            PATH = config_file
            config = ConfigParser.RawConfigParser()
            config.optionxform = str
            config.read(PATH)

            if config.has_section('parameters'):

                parameters = list(config.items('parameters'))

                menu = {}

                counter = 0
                for p in parameters:
                    print(p)
                    counter += 1
                    my_list = []
                    my_list.append(p)

                    print('p: '+str(p))
                    if p[0] == 'S3BucketName':
                        print('found s3 bucket name ...returning')

                        return p[0]

                    menu[counter] = my_list


                # If there are existing projects
                if len(menu) > 0:
                    print "\n\n"
                    print '#########################################'
                    print '## Select Lambda Bucket Parameter                   ##'
                    print '#########################################'
                    for key in sorted(menu):
                        print str(key) + ":" + str(menu[key][0])

                    pattern = r'^[0-9]+$'
                    while True:

                        ans = raw_input("Make A Choice: [ENTER]")
                        if re.match(pattern, ans) is not None:
                            if int(ans) in menu:
                                package_parameter = menu[int(ans)][0]
                                break


                    if (DEBUG):
                        print('package parameter is: '+str(package_parameter))

                    return package_parameter[0]

                else:
                    print('However, there is no lambda bucket parameter set.  Add bucket to stackility in config file')

            else:
                print('could not find the stackility section in the config file')




    except (KeyboardInterrupt, SystemExit):
        sys.exit()


def get_lambda_bucket_key_from_ini(config_file):
    if (DEBUG):
        print('stackility - def get_lambda_bucket_key_from_ini')

    try:
        PATH = config_file
        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        config.read(PATH)

        if config.has_section('parameters'):

            parameters = list(config.items('parameters'))

            menu = {}

            counter = 0
            for p in parameters:
                print(p)
                counter += 1
                my_list = []
                my_list.append(p)

                if p[0] == 'S3Key':
                    return p[0]



                menu[counter] = my_list

            # If there are existing projects
            if len(menu) > 0:
                print "\n\n"
                print '#########################################'
                print '## Select Lambda Bucket Key Parameter                   ##'
                print '#########################################'
                for key in sorted(menu):
                    print str(key) + ":" + str(menu[key][0])

                pattern = r'^[0-9]+$'
                while True:

                    ans = raw_input("Make A Choice: [ENTER]")
                    if re.match(pattern, ans) is not None:
                        if int(ans) in menu:
                            package_parameter = menu[int(ans)][0]
                            break

                if (DEBUG):
                    print('package parameter is: ' + str(package_parameter))

                return package_parameter[0]

            else:
                print('However, there is no lambda bucket key parameter set.  Add bucket key to stackility in config file')

        else:
            print('could not find the stackility section in the config file')




    except (KeyboardInterrupt, SystemExit):
        sys.exit()


